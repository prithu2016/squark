package algorithm;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.StringTokenizer;
import java.util.Scanner;  

public class Landmarks {
	Graph graph;
	ArrayList<Integer> keywordIds = new ArrayList<>();
	int keywordNodesCount;
	HashMap<Integer, Integer> visited = new HashMap<Integer, Integer>();
	HashMap<Integer, ArrayList<Integer>> reverseVisited = new HashMap<>();
	int clusterId = 0;
	int distanceApart = 750;
	
	
	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		Landmarks lm = new Landmarks();
		lm.graph = new Graph ();
		//lm.buildInterReduced();
		//lm.medoidAnalysis();
		//lm.cluster(distanceApart);
		//lm.buildIndex();
		lm.init();
		//lm.buildIndex();
		//lm.buildInter();
	}

	public void buildInterReduced () throws Exception {
		graph.readCoordinates();
		graph.readKeywords();
		ArrayList<Integer> reducedKeywords = new ArrayList<Integer>();
		BufferedReader bf = new BufferedReader (new FileReader (Parameters.ROOT_DATA + Parameters.CITY + "/nodesWithKeywordsSerialReduced.txt"));
		while (true) {
			String line = bf.readLine();
			if (line == null) break;
			int id = Integer.parseInt(line.split(" ")[0]);
			reducedKeywords.add(id);	
		}
		ArrayList<Integer> oldKeynodes = new ArrayList<Integer>();
		for (int i = 0 ;i < graph.n; i++) {
			if (graph.nodes.get(i).keywords != null) {
				oldKeynodes.add(i);
			}
		}
		System.out.println(oldKeynodes.size() + " " + reducedKeywords.size());
		int index = 1;
		PrintWriter pow = new PrintWriter (new FileWriter (Parameters.ROOT_DATA + Parameters.CITY + "/interReduced_" + index + ".txt"));
		int readId = 0;
		ArrayList<Integer> presentIds = new ArrayList<Integer>();
		for (int i = 0; i < oldKeynodes.size(); i++) {
			int id = oldKeynodes.get(i);
			if (reducedKeywords.contains(id)) 
				presentIds.add(i);
		}
		while (true) {
			bf = new BufferedReader (new FileReader(Parameters.ROOT_DATA + Parameters.CITY + "/inter_" + index + ".txt"));
			pow = new PrintWriter (new FileWriter (Parameters.ROOT_DATA + Parameters.CITY + "/interReduced_" + index + ".txt"));
			while (true) {
				String line = bf.readLine();
				if (line == null) {
					break;
				}
				readId = Integer.parseInt(line);
				System.out.println(line);
				pow.println(line);
				for (int i = 0; i < oldKeynodes.size(); i++) {
					line = bf.readLine();
					if (presentIds.contains(i)) {
						pow.println(line);
					}
				}
				line = bf.readLine();
				System.out.println(line);
				pow.println(line);
			}
			if (readId == graph.n-1)
				break;
			index++;
		}
		pow.close();

	}

	public void buildInterNew () throws Exception {
		ArrayList<Integer> sizes = new ArrayList<Integer>();
		sizes.add(10000);
		sizes.add(25000);
		sizes.add(50000);
		Graph graph = new Graph();
		graph.readCoordinates();
		graph.readKeywords();
	
		ArrayList<Integer> oldKeywords = new ArrayList<Integer>();

		for (int i = 0; i < graph.n; i++) {
			if (graph.nodes.get(i).keywords != null)
				oldKeywords.add(i);
		}

		HashMap<Integer, Integer> newToOld = new HashMap<Integer, Integer>();
		HashMap<Integer, Integer> oldToNew = new HashMap<Integer, Integer>();

		for (int size : sizes) {	
			BufferedReader bf = new BufferedReader (new FileReader (Parameters.ROOT_OUTPUTS + size + "/serial_node_mapping.txt")); 
			String line = bf.readLine();
			while (true) {
				line = bf.readLine();
				if (line == null) break;
				int newId = Integer.parseInt(line.split(" -> ")[0]);
				int oldId = Integer.parseInt(line.split(" -> ")[1]);
				newToOld.put(newId, oldId);
				oldToNew.put(oldId, newId);
			}

			bf = new BufferedReader (new FileReader (Parameters.ROOT_OUTPUTS + size + "/nodesWithKeywordsSerial.txt"));
			ArrayList<Integer> newKeywords = new ArrayList<Integer>();
			while (true) {
				line = bf.readLine();
				if (line == null) break;
				newKeywords.add(Integer.parseInt(line.split(" ")[0]));
			}

			/*for (int i = 0 ; i < newKeywords.size(); i++) {
				for (int j = 0; j < i; j++) {
					if (newKeywords.get(j) > newKeywords.get(i))
						System.out.println("Fucked : " + newKeywords.get(j) + " " + newKeywords.get(i));
				}
			}*/
			Long start = System.currentTimeMillis();
			int index = 1;
			int count = 0;
			PrintWriter pow = new PrintWriter (new FileWriter (Parameters.ROOT_OUTPUTS + size + "/inter.txt", true));
			while (index <= 12) {		
				bf = new BufferedReader (new FileReader (Parameters.ROOT_DATA + Parameters.CITY + "/inter_" + index + ".txt"));
				HashMap<Integer, Integer> distanceMapping = new HashMap<Integer, Integer>();
				while (true) {
					line = bf.readLine();
					if (line == null) break;
					int id = Integer.parseInt(line.split(" ")[0]);
					if (oldToNew.containsKey(id)) {
						count++;
						pow.println(oldToNew.get(id));
						distanceMapping = new HashMap<Integer, Integer>();
					}
					for (int x = 0 ; x < oldKeywords.size(); x++) {
						line = bf.readLine();
						if (oldToNew.containsKey(id))
							distanceMapping.put(oldKeywords.get(x), Integer.parseInt(line));
					}
					if (oldToNew.containsKey(id)) {
						for (int x = 0; x < newKeywords.size(); x++)
							pow.println(distanceMapping.get(newToOld.get(newKeywords.get(x))));
						pow.println("****");
						System.out.println(size + " " + count);
					}
					line = bf.readLine();
				}
				index++;
			}
			pow.close();
			Long end = System.currentTimeMillis();
			System.out.println("Time taken : " + (end-start));
		}
	}
	
	public void temp () throws Exception {
		BufferedReader bf = new BufferedReader(new FileReader(Parameters.ROOT_DATA + Parameters.CITY + "/landmarkDistanceIndex.txt"));
		PrintWriter pow = new PrintWriter(new FileWriter(Parameters.ROOT_DATA + Parameters.CITY + "/landmarkDistanceIndexNew.txt"));
		while (true) {
			String line = bf.readLine();
			if (line == null) break;
			String[] splits = line.split(" ");
			if (splits.length == 1)  pow.println(line);
			else if (splits.length == 2) pow.println(splits[1]);
		}
		pow.close();
	}
	
	
	public void sortCluster() throws Exception {
		BufferedReader bf = new BufferedReader(new FileReader(Parameters.ROOT_DATA + Parameters.CITY + "/landmarkIndex.txt"));
		String line = "";
		int i = 4;
		while (i-- > 0) { line = bf.readLine();}
		ArrayList<Integer> roots = new ArrayList<Integer>();
		StringTokenizer stt = new StringTokenizer(line,  " ");
		String clusterId = stt.nextToken().toString();
		int total = 0;
		while (stt.hasMoreTokens()) {
			boolean flag  = true;
			int id = Integer.parseInt(stt.nextToken().toString());
			for (i = 0; i < roots.size(); i++) {
				if (getDist(graph.nodes.get(roots.get(i)), graph.nodes.get(id)) < distanceApart) {
					flag = false;
					break;
				}
			}
			if (flag)
				roots.add(id);
			total++;
		}
		System.out.println("roots : " + roots.size() + " total : " + total);
		
	}
	
	public void landmarkAnalysis () throws Exception {
		graph.readCoordinates();
		graph.readLandmarks();
		graph.readLabels();
		PrintWriter pow = new PrintWriter(new FileWriter(Parameters.ROOT_DATA + Parameters.CITY + "/LBUBLandmarkAnalysis.csv"));
		pow.println("Landmark no. , Count , UB Mean , UB Variance , LB Mean , LB Variance");
		
		Iterator it = graph.landmarks.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<Integer, Landmark> pair = (Entry<Integer, Landmark>) it.next();
			ArrayList<Integer> ids = pair.getValue().keynodes;
			int landmark = pair.getKey();
			
			if (landmark == 7) {
				System.out.println("here");
			}
			System.out.println("Landmark : " + landmark);
			ArrayList<Double> ratioLB = new ArrayList<>();
			ArrayList<Double> ratioUB = new ArrayList<>();
			
			for (int i = 0; i < 1000; i++) {
				if (ids.contains(i)) continue;
				if (graph.nodes.get(i).distanceToLandmarks.get(landmark) == Integer.MAX_VALUE) continue;
				for (int id : ids) {
					int dist = graph.getDistance(i, id);
					if (dist == Integer.MAX_VALUE) continue;
					if (graph.nodes.get(i).distanceToLandmarks.get(landmark) != Integer.MAX_VALUE)
						ratioLB.add(dist / (double)graph.nodes.get(i).distanceToLandmarks.get(landmark));
					//if (graph.nodes.get(i).maxDistanceToLandmarks.get(landmark) != Integer.MAX_VALUE)
						//ratioUB.add(dist / (double)graph.nodes.get(i).maxDistanceToLandmarks.get(landmark));
				}
				//if (ratioLB.size() > 100 && ratioUB.size() > 100) break;
			}
			pow.print(landmark + "," + ids.size() + ",");
			
			ArrayList<Double> ans = new ArrayList<Double>();
			analyseGaussian(ratioUB, ans);
			pow.print(ans.get(0) + "," + ans.get(1) + ",");
			
			ans = new ArrayList<Double>();
			analyseGaussian(ratioLB, ans);
			pow.println(ans.get(0) + "," + ans.get(1));
		}
		pow.close();
		
	}
	
	public void medoidAnalysis() throws Exception {
		graph.readCoordinates();
		graph.readLabels();
		graph.readLandmarks();
		
		PrintWriter pow = new PrintWriter(new FileWriter(Parameters.ROOT_DATA + Parameters.CITY + "/landmarkAnalysisMedoidMetis.csv"));
		
		pow.println("Landmark no. , Count , Medoid , Radius , UB Mean , UB Variance , LB Mean , LB Variance");
		
		for (int i = 0; i < graph.landmarks.size(); i++) {
			int landmark = i;
			
			ArrayList<Integer> ids = graph.landmarks.get(landmark).keynodes;
			ArrayList<Double> ans = new ArrayList<Double>();
			
			if (landmark == 2) {
				System.out.println("here");
			}
			
			getMedoid(ids, ans);
			
			double radius = ans.get(0);
			int medoid = ans.get(1).intValue();
			
			
			ArrayList<Double> ratioUB = new ArrayList<Double>();
			ArrayList<Double> ratioLB = new ArrayList<Double>();
			
			for (int j = 0; j < 100; j++) {
				if (ids.contains(j)) continue;
				
				int distToMedoid = graph.getDistance(j, medoid);
				if (distToMedoid == Double.MAX_VALUE || distToMedoid == Double.MIN_VALUE)
					continue;
				
				double UB = distToMedoid + radius;
				double LB = graph.nodes.get(j).distanceToLandmarks.get(landmark);
				if (LB <= 0) {
					continue; 
				}
				if (UB <= 0) {
					continue;
				}
				for (int id : ids) {
					double dist =/* getDist(graph.nodes.get(j), graph.nodes.get(id)); */graph.getDistance(j, id);
					if (dist == Double.MAX_VALUE || dist == Double.MIN_VALUE)
						continue;
					if (dist >= LB) ratioLB.add(dist / LB);
					else System.out.println("LB : " + dist + " " + LB);
					
					if (dist <= UB) ratioUB.add(dist / UB);
					else System.out.println("UB : " + dist + " " + UB);
					
				}
			}
			
			pow.print(landmark + "," + ids.size() + "," + medoid + "," + radius + ",");
			
			ArrayList<Double> gaussianAns = new ArrayList<Double>();
			analyseGaussian( ratioUB, gaussianAns);
			
			pow.print(gaussianAns.get(0) + "," + gaussianAns.get(1) + ",");
			
			gaussianAns = new ArrayList<Double>();
			analyseGaussian( ratioLB, gaussianAns);
			
			pow.println(gaussianAns.get(0) + "," + gaussianAns.get(1));
			
			System.out.println("Done landmark : " + landmark);
			
		}

		pow.close();
	}
	
	public void analyseGaussian(ArrayList<Double> ratio, ArrayList<Double> ans) {
		double total = 0;
		for (double val : ratio) total += val;
		double mean = total / ratio.size();
		total = 0;
		for (double val : ratio) total += Math.pow(val - mean, 2);
		double var = Math.sqrt( total / ratio.size() );
		
		ans.add(mean);
		ans.add(var);
		
	}
	
	public void getMedoid(ArrayList<Integer> ids, ArrayList<Double> ans) {
		
		if (ids.size() == 1) {
			ans.add(0.0);
			ans.add((double)ids.get(0));
			return ;
		}
		
		int min1 = 0;
		double radius = Integer.MAX_VALUE;
		
		for (int i = 0; i < ids.size(); i++) {
			double maxDist = Double.MIN_VALUE;
			boolean changed = false;
			for (int j = i+1; j < ids.size(); j++) {
				double dist = /*getDist(graph.nodes.get(ids.get(i)), graph.nodes.get(ids.get(j)));*/ graph.getDistance(ids.get(i), ids.get(j));
				if (dist > maxDist ) {
					if (dist < Double.MAX_VALUE && dist > Double.MIN_VALUE) {
						maxDist = dist;
						changed = true;
					}
					else {
						System.out.println("fucked 1");
					}
				}
			}
			if (maxDist < radius /*maxDist < minDist*/) {
				if (maxDist > Double.MIN_VALUE && changed == true) {
					min1 = i;
					radius = maxDist;
				}
				else if (changed == true) {
					System.out.println("fucked 2");
					
				}
			}
		}
		ans.add(radius);
		ans.add((double)min1);
	}
	
	public void buildIndex() throws Exception {
		//graph.buildLandmarkIndex(reverseVisited, clusterId);
		graph.landmarkIndexHandler();
	}
	
	public void init () throws Exception {
		/*ArrayList<Integer> sizes = new ArrayList<Integer>();
		sizes.add(5000);
		sizes.add(10000);
		sizes.add(15000);
		for (int size : sizes) {*/
			graph = new Graph();
			//graph.size = size;
			graph.metis = 1300 /*(int) (size * 0.055)*/;
			graph.readCoordinates();
			//graph.readNetwork();
			graph.readLabels();
			graph.readKeywords();
			buildIndex();
		//}
	}


	public void buildInter() throws Exception {
		//Scanner sc = new Scanner(System.in);
		//int size = Integer.parseInt(sc.nextLine());
		Graph graph = new Graph();
		//graph.size = size;
		graph.readCoordinates();
		graph.readLabels();
		graph.readKeywords();
		ArrayList<Integer> keys = new ArrayList<Integer>();
		for (int i = 0; i < graph.n; i++) {
			if (graph.nodes.get(i).keywords != null)
				keys.add(i);
		}
		System.out.println("Keys : " + keys.size());
		PrintWriter pow = new PrintWriter(new FileWriter(Parameters.ROOT_DATA + "/inter.txt", true));
		for (int i = 0 /*108454*/; i < graph.n; i++) {
			pow = new PrintWriter(new FileWriter(Parameters.ROOT_DATA +  "/inter.txt", true));
			pow.println(i);
			for (int j = 0; j < keys.size(); j++) {
				int dist = graph.getDistance(i, keys.get(j));
				pow.println(dist);
			}	
			pow.println("****");
			System.out.println(i);
			pow.close();
		}
	}
	
	public void cluster(int distance) throws Exception {
		keywordNodesCount = graph.keywords.keySet().size();
		
		for (int i = 0; i < keywordNodesCount; i++)
			visited.put(keywordIds.get(i), -1);
		
		while (true) {
			boolean flag = true;
			for (int i = 0; i < keywordNodesCount; i++) {
				if (visited.get(keywordIds.get(i)) == -1) {
					dfs(keywordIds.get(i), clusterId, distance);
					clusterId++;
					flag = false;
				}
			}
			if (flag)
				break;
		}
		System.out.println(clusterId);
	}
	
	public void dfs (int node, int clusterId, int distance) {
		visited.put(node, clusterId);
		if (!reverseVisited.containsKey(clusterId))
			reverseVisited.put(clusterId, new ArrayList<Integer>());
		reverseVisited.get(clusterId).add(node);
		for (int i = 0; i < keywordNodesCount; i++) {
			if (visited.get(keywordIds.get(i)) == -1) {
				if (getDist(graph.nodes.get(node), graph.nodes.get(keywordIds.get(i))) < distance) {
					dfs(keywordIds.get(i), clusterId, distance);
				}
			}
		}
	}
	
	public double getDist (Node n1, Node n2) {
		double earthRadius = 6371000; // metres
	    double dLat = Math.toRadians(n2.lat - n1.lat);
	    double dLng = Math.toRadians(n2.longi - n1.longi);
	    double sindLat = Math.sin(dLat / 2);
	    double sindLng = Math.sin(dLng / 2);
	    double a = Math.pow(sindLat, 2) + Math.pow(sindLng, 2)
	            * Math.cos(Math.toRadians(n1.lat)) * Math.cos(Math.toRadians(n2.lat));
	    double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
	    double dist = earthRadius * c;
	    return dist;
	}
}
