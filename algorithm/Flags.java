package algorithm;

public class Flags {
	public static final boolean RECURSION_STACK_STORAGE = false;
	public static final boolean GOAL_DIRECTED_SEARCH = true;
	public static final boolean LANDMARK_BASED_PRUNING = true;
	public static final boolean SHORTEST_DISTANCE_THROUGH_KEYWORD_STORAGE = true;
	public static final boolean SHORTEST_LANDMARK_DISTANCE_THROUGH_KEYWORD_STORAGE = true;
	public static final boolean DIRECTED = false;
	public static final boolean FIND_RANKING_OF_KEYNODE = false;
	public static final boolean READ_NEXTHOP = false;
}
