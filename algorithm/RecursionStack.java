package algorithm;

import java.util.ArrayList;
import java.util.HashMap;

public class RecursionStack {
	Skyline skyline;
	
	public RecursionStack (Skyline inputSkyline) {
		skyline = inputSkyline;
	}
	
	public boolean useRecursionStack(int source, int distTillNow) {
		StringBuilder vector = new StringBuilder("");
		for (String key : skyline.query.keywords) {
			if (skyline.query.unexploredKeywords.contains(key))
				vector.append("0");
			else
				vector.append("1");
		}
		if (skyline.graph.nodes.get(source).futureInfoDist.containsKey(vector)) {
			System.out.println("here");
			int exploredCount = skyline.query.exploredKeywords.size();
			for (int i = exploredCount+1; i <= skyline.query.keywords.size(); i++) {
				int index = i - exploredCount;
				if (!skyline.graph.nodes.get(source).futureInfoDist.get(vector).containsKey(index)) continue;
				int dist = skyline.graph.nodes.get(source).futureInfoDist.get(vector).get(index);
				if (skyline.dp.get(i) > distTillNow + dist) {
					skyline.dp.set(i, dist+distTillNow);
					/*ArrayList<Integer> path = new ArrayList<>();
					path.addAll(pathTillNow);
					path.addAll(graph.nodes.get(source).futureInfoPath.get(vector).get(index));
					paths.set(index, path);*/
				}
			}
			return true;
		}
		return false;
	}
	
	public void storeRecursionStack(int source, int distTillNow, ArrayList<Integer> oldDp, ArrayList<String> oldUnexplored) {
		StringBuilder vector = new StringBuilder("");
		for (String key : skyline.query.keywords) {
			if (oldUnexplored.contains(key))
				vector.append("0");
			else
				vector.append("1");
		}
		if (!skyline.graph.nodes.get(source).futureInfoDist.containsKey(vector))
			skyline.graph.nodes.get(source).futureInfoDist.put(vector, new HashMap<Integer, Integer>());
		
		int exploredCount = skyline.query.exploredKeywords.size();
		
		for (int c = 0; c < oldDp.size(); c++) {
			if (skyline.dp.get(c) == oldDp.get(c))
				continue;
			
			int index = c-(exploredCount-skyline.query.minKeywordCount);
			
			if (index < 0) continue;
			
			int dist = skyline.dp.get(c) - distTillNow;
			
			if (!skyline.graph.nodes.get(source).futureInfoDist.containsKey(vector))
				skyline.graph.nodes.get(source).futureInfoDist.put(vector, new HashMap<Integer, Integer>());
			if (!skyline.graph.nodes.get(source).futureInfoDist.get(vector).containsKey(index))
				skyline.graph.nodes.get(source).futureInfoDist.get(vector).put(index, Integer.MAX_VALUE);
			if (dist < skyline.graph.nodes.get(source).futureInfoDist.get(vector).get(index)) {
				System.out.println("Added " + source + " " + vector + " " + index);
				skyline.graph.nodes.get(source).futureInfoDist.get(vector).put(index, dist);
			}
		}
	}
}
