package algorithm;

import java.io.*; 
import java.util.ArrayList;
import java.util.Collections;
import java.util.*;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

public class SkylinePrune extends Skyline {
	
	public SkylinePrune (Graph inputGraph, Query inputQuery) {
		this.graph = inputGraph;
		this.query = inputQuery;
		
	}
	public SkylinePrune (Graph inputGraph) {
		this.graph = inputGraph;
	}
	
	public Long start() throws Exception {
		super.start();
		recur(query.source, 0);
		/*try {
			PrintWriter pow = new PrintWriter(new FileWriter(Parameters.ROOT_OUTPUTS + "Proportional/metis2000/LBs" + epsilon + ".csv", true));	
			pow.println("****,****" );
			pow.close();
		}
		catch (Exception e) {System.out.println(e);} */
		Long end = System.currentTimeMillis();
		int dist = graph.getDistance(query.source, query.destination);

		for (int i = 0; i < dp.size(); i++) {
			if (dp.get(i) == 0) {
                                dp.set(i, dist);
                        }
                }
		System.out.println(dp);
		for (ArrayList<Integer> path : paths) 
			System.out.println(path);
		
		uniquePaths = 0;
		int prev = -1;
		for (int i = 0; i <= query.maxKeywordCount-query.minKeywordCount; i++) {
			if (dp.get(i) != prev) {
				prev = dp.get(i);
				uniquePaths++;
			}
		}
		System.out.println("Unique paths : " + uniquePaths);
		return end;
	}
	
	/*public boolean prune (int distTillNow, ArrayList<Pair> minimumDistancesToKeywords) {
		int exploredKeywordsCount = query.exploredKeywords.size();
		for (int x = 0; x < minimumDistancesToKeywords.size(); x++) {
			int index = exploredKeywordsCount - query.minKeywordCount + x + 1;
			int minimumDistance = minimumDistancesToKeywords.get(x).first;
			if (minimumDistance + distTillNow < dp.get(index)) {
				return false;
				
			}
		}
		return true;
	}*/
	
	public void getLowerBoundThroughKeywords (int source, ArrayList<Pair> LBminimumDistancesToKeywordsUsingLandmarks) {
		if (Flags.SHORTEST_LANDMARK_DISTANCE_THROUGH_KEYWORD_STORAGE) {
			HashSet<String> unseenKeywords = new HashSet<String>();
			for (String keyword : query.unexploredKeywords) {
				if (!graph.nodes.get(source).landmarkDistanceToDestThroughKeyword.containsKey(keyword)) {
					//graph.LBgetDistanceThroughKeyword(source, query.destination, keyword);
					unseenKeywords.add(keyword);
				}
			}
			if (unseenKeywords.size() > 0)
				graph.LBgetDistancesThroughKeywords(source, query.destination, unseenKeywords);
			for (String keyword : query.unexploredKeywords) {
				if (!graph.nodes.get(source).landmarkDistanceToDestThroughKeyword.containsKey(keyword)) continue;
				if (graph.nodes.get(source).landmarkDistanceToDestThroughKeyword.get(keyword) != -1) {
					Pair p = new Pair();
					p.first = graph.nodes.get(source).landmarkDistanceToDestThroughKeyword.get(keyword);
					LBminimumDistancesToKeywordsUsingLandmarks.add(p);
				}
			}
		}
		else {
			HashMap<String, Pair> LBoccurences = graph.LBgetDistancesThroughKeywords(source, query.destination, query.unexploredKeywords);
			Iterator it = LBoccurences.entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry<String, Pair> pair = (Entry<String, Pair>) it.next();
				LBminimumDistancesToKeywordsUsingLandmarks.add(LBoccurences.get(pair.getKey()));
			}
		}
		Collections.sort(LBminimumDistancesToKeywordsUsingLandmarks, new PairSortingUsingFirstAndThird());
	}
	
	public void getDistanceThroughKeywords (int source, ArrayList<Pair> minimumDistancesThroughKeywords
			) throws Exception {
		for (String keyword : query.unexploredKeywords) {
			if (!graph.nodes.get(source).distanceToDestThroughKeyword.containsKey(keyword)) {
				graph.getDistanceThroughKeyword(source, query.destination, keyword);
			}
			if (graph.nodes.get(source).distanceToDestThroughKeyword.get(keyword) != -1) {
				Pair p = new Pair();
				p.first = graph.nodes.get(source).distanceToDestThroughKeyword.get(keyword);
				minimumDistancesThroughKeywords.add(p);
			}
		}
		Collections.sort(minimumDistancesThroughKeywords, new PairSortingUsingFirst());
	}
	
	public void storeNextHopAndDfs (int source, int distTillNow, ArrayList<Integer> pathTillNow, int distToDest) throws Exception {
		
		updateDp(source , distTillNow , pathTillNow, distToDest); 
		
		if (Flags.LANDMARK_BASED_PRUNING) {
			ArrayList<Pair> LBminimumDistancesToKeywordsUsingLandmarks = new ArrayList<Pair>();
			getLowerBoundThroughKeywords (source, LBminimumDistancesToKeywordsUsingLandmarks);
			if (prune (distTillNow, LBminimumDistancesToKeywordsUsingLandmarks)) {
				/*if (source == 147201) {
					System.out.println("Here");
					System.out.println(dp);
					for (Pair p : LBminimumDistancesToKeywordsUsingLandmarks)
					System.out.print(p.first + " ");
					System.out.println("****");
				}*/
				if (LBminimumDistancesToKeywordsUsingLandmarks.size() > 0)	{
					query.landmarkPruneCount++;
					//System.out.println("Landmark pruning : " + source);
				}
				return;
			}
		}
		
		ArrayList<NextHop> nextHops = new ArrayList<NextHop>();
		ArrayList<ArrayList<Pair>> keywordDistances = new ArrayList<ArrayList<Pair>>();
		ArrayList<Pair> minimumDistancesThroughKeywords = new ArrayList<Pair>();
		
		if (Flags.SHORTEST_DISTANCE_THROUGH_KEYWORD_STORAGE) {
			getDistanceThroughKeywords(source, minimumDistancesThroughKeywords);
		}
		else {
			//getNextHop (source, nextHops, minimumDistancesThroughKeywords);
			Collections.sort(minimumDistancesThroughKeywords, new PairSortingUsingFirst());
		}
		
		if (prune(distTillNow, minimumDistancesThroughKeywords)) {
                        /*for (String keyword : query.unexploredKeywords) {
                        	if (graph.nodes.get(source).distanceToDestThroughKeyword.get(keyword) < graph.nodes.get(source).landmarkDistanceToDestThroughKeyword.get(keyword))
				System.out.println("fucked " + graph.nodes.get(source).distanceToDestThroughKeyword.get(keyword) + " " + graph.nodes.get(source).landmarkDistanceToDestThroughKeyword.get(keyword) + " " + source + " " + query.destination + " " + keyword);
			}*/
			if (minimumDistancesThroughKeywords.size() > 0)	{
				query.pruneCount++;
				//System.out.println("Prune2 : " + source) ;
			}
			return;
		}
		if (Flags.SHORTEST_DISTANCE_THROUGH_KEYWORD_STORAGE) {
			getNextHop (source, keywordDistances);
			//getNextHop (source, nextHops);
		}
		query.pathsExplored++;
		/*System.out.println("****");
		System.out.println("Saved : " + source + " Path till now : " + pathTillNow);
		System.out.println("Dp : " + dp);
		System.out.println("Paths : " + paths) ;
		System.out.println("Dist till now : " + distTillNow);
		System.out.print("LB : [");
		for (Pair p : minimumDistancesThroughKeywords)
			System.out.print(p.first + ", ");
		System.out.println("]");
		System.out.println("Unexplored keywords : " + query.unexploredKeywords); 
		System.out.println("****");*/
		//dfs(source, nextHops, distTillNow, pathTillNow, minimumDistancesThroughKeywords);
		dfs(source, keywordDistances, distTillNow, pathTillNow, minimumDistancesThroughKeywords);
	}
	
}
