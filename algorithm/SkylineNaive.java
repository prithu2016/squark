package algorithm;

import java.util.ArrayList;

import org.omg.CORBA.ExceptionList;

public class SkylineNaive extends Skyline {
	
	public SkylineNaive (Graph inputGraph, Query inputQuery) {
		this.graph = inputGraph;
		this.query = inputQuery.deepCopy();
		
	}
	public SkylineNaive (Graph inputGraph) {
		this.graph = inputGraph;
	}
	
	public SkylineNaive () {
	}
	
	public void start() throws Exception {
		super.start();
		recur(query.source, 0);
		System.out.println(dp);
	}
	
	public void storeNextHopAndDfs (int source, int distTillNow, ArrayList<Integer> pathTillNow, int distToDest) throws Exception {
		if (query.unexploredKeywords.size() == 0) {
			//System.out.println("Here");
		}
		query.pathsExplored++;
		ArrayList<NextHop> nextHops = new ArrayList<NextHop>();
		ArrayList<Pair> minimumDistancesToKeywords = new ArrayList<Pair>();
		getNextHop (source , nextHops);
		if (nextHops.size() > 0)
			dfs(source, nextHops, distTillNow, pathTillNow);
	}

}
