package algorithm;

public class Edge {

	public int neighbour;
	public int weight;
	
	public Edge(int inputNeighbour, int inputWeight) {
		neighbour = inputNeighbour;
		weight = inputWeight;
	}
}
