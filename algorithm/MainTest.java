package algorithm;
import java.awt.Color;
import java.awt.Font;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.Random;
import java.util.Scanner;


import org.openstreetmap.gui.jmapviewer.Coordinate;
import org.openstreetmap.gui.jmapviewer.Demo;
import org.openstreetmap.gui.jmapviewer.MapMarkerDot;

public class MainTest {

	public static void main(String[] args) throws Exception {
		MainTest m = new MainTest();
		m.plotStuff();
		 
	}
	private  void plotStuff() throws Exception {
		Demo map = new Demo();
		map.setVisible(true);
		
		Graph graph = new Graph();
		graph.readCoordinates();
		graph.readKeywords();
		for (Node node : graph.nodes) {
			Coordinate c = new Coordinate(node.lat, node.longi);
			if (graph.nodes.get(node.id).keywords == null)
				mark(c, "" , map, Color.BLUE, 11);
			else {
				String ans = "";
				for (String key : graph.nodes.get(node.id).keywords)
					ans = ans + key + " ";
				mark(c, "", map, Color.RED, 13);
			}
		}
	}
	static void mark(Coordinate coord, String str, Demo map, Color color, int fontsize) {
		MapMarkerDot dot = new MapMarkerDot(coord);
		dot.setBackColor(color);
		dot.setColor(Color.BLACK);

		//MapMarkerDot.DOT_RADIUS = 200;
		dot.setFont(new Font("Courier", Font.BOLD, fontsize));
		dot.setName(str);
		map.treeMap.getViewer().addMapMarker(dot);

	}

}
