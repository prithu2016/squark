package algorithm;

import java.util.Comparator;
import java.util.HashMap;

class PairSortingUsingFirstAndThird implements Comparator<Pair> {
	public int compare (Pair p1, Pair p2) {
		if ((p1.first + p1.third) != (p2.first + p2.third))
			return (p1.first + p1.third) - ( p2.first + p2.third);
		else
			return (p1.first - p2.first);
	}
}

class FrequencyKeywordSorting implements Comparator<String> {
	HashMap<String, Keyword> keywords = new HashMap<>();
	public FrequencyKeywordSorting(HashMap<String, Keyword> input) {
		keywords = input;
	}
	public int compare (String s1, String s2) {
		return keywords.get(s2).keynodes.size() - keywords.get(s1).keynodes.size();
	}
}

class PairSortingUsingFirst implements Comparator<Pair> {
	public int compare (Pair p1, Pair p2) {
		return (p1.first) - (p2.first);
	}
}

class PairSortingUsingThird implements Comparator<Pair> {
	public int compare (Pair p1, Pair p2) {
		return (p1.third) - (p2.third);
	}
}


public class Pair {
	public int first = 0;
	public int second = 0;
	public int third = 0;
}
