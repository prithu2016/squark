package algorithm;

import java.io.*;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.*;
import java.util.Map.Entry;

import data.node;

public class Analyse {
	Graph graph = new Graph ();
	HashMap<Integer, HashMap<String, ArrayList<ArrayList<Integer>>>> fullPaths;
	HashMap<Integer, HashMap<String, ArrayList<ArrayList<Integer>>>> partialPaths;
	String city = "dublin";
	String root = city + "/";
	HashMap<Integer, Integer> clusterMapping = new HashMap<>();
	
	public static void main(String[] args) throws Exception {
		Analyse an = new Analyse();
		//an.average();
		//an.landmarkAnalysis();
		//an.landmarkAnalysis();
		an.testTime();
	}

	public Analyse() {
		
	}
	
	public Analyse (Graph g) {
		graph = g;
	}
	
	public void init () throws Exception {
		graph.readCoordinates();
		graph.readLabels();
		graph.readKeywords();
		System.out.println("Done reading");
	}
	
	public void average() throws Exception {
		buildClusterMapping();
		for (int j = 50; j <=2000; j = j + 50) {
			if (!clusterMapping.containsKey(j)) continue;
			
			BufferedReader bf1 = new BufferedReader(new FileReader("dublin/final/outputPruneLandmarks" + j + ".csv"));
			
			PrintWriter pow = new PrintWriter(new FileWriter("dublin/plots/timesLandmarks.csv", true));
			if (j == 50)
				pow.println("Max, Cluster, Time");
			
			String algo = "SQUARK with Node Pruning and Landmarks";
			HashMap<Integer, ArrayList<Integer>> times = new HashMap<>();
			int highest = Integer.MIN_VALUE;
			
			while (true) {
				String line = bf1.readLine();
				if (line == null) break;
				String[] splits = line.split(",");
				if (splits.length < 11) continue;
				if (splits[8].contains("2147483647")) continue;
				
				int max = Integer.parseInt(splits[1]);
				if (max > highest)
					highest = max;
				int time = Integer.parseInt(splits[4]);
				int landmarkPruning = Integer.parseInt(splits[5]);
				int nodePruning = Integer.parseInt(splits[6]);
				
				if (!times.containsKey(max)) {
					times.put(max, new ArrayList<Integer>());
				}
				times.get(max).add(time);
			}
			for (int i = 1; i <= highest; i++) {
				System.out.println("Keywords : " + i);
				System.out.println("avg time : " + avg(times.get(i)));
				pow.println(i + "," + clusterMapping.get(j) + "," + avg(times.get(i)));
			}
			pow.close();
			
		}
	}
	
	public void buildClusterMapping () throws Exception {
		HashMap<Integer, Integer> temp = new HashMap<>();
		HashMap<Integer, Integer> values = new HashMap<>();
		for (int i = 50; i <=2000; i=i+50) {
			BufferedReader bf = new BufferedReader(new FileReader("/Users/chendu/Desktop/DDP/New/Data/dublin/metis"+i+"/landmarkIndex.txt"));
			int count = 0;
			while (true) {
				String line = bf.readLine();
				if (line == null) break;
				count++;
			}
			temp.put(i, count);
			values.put(count, i);
		}
		int[] taken = new int[850];
		for (int center = 50; center <= 850; center=center+50) {
			int i = center;
			int j = center;
			while (true) {
				if (values.containsKey(i) && taken[i] == 0) {
					taken[i] = 1;
					clusterMapping.put(values.get(i), i);
					break;
				}
				if (values.containsKey(j) && taken[j] == 0) {
					taken[j] = 1;
					clusterMapping.put(values.get(j), i);
					break;
				}
				i++;
				j--;
			}
		}
	}
	
	public void landmarkAnalysis () throws Exception {
		PrintWriter pow = new PrintWriter(new FileWriter("dublin/plots/distances.csv"));
		pow.println("Max, Bucket, Runtime, Hops");
		graph = new Graph();
		graph.readCoordinates();
		graph.readLabels();
		BufferedReader bf = new BufferedReader(new FileReader("dublin/final/outputPruneLandmarksGoalDirected200.csv"));
		HashMap<Integer, HashMap<Integer, ArrayList<Integer>>> times = new HashMap<>();
		HashMap<Integer, HashMap<Integer, ArrayList<Integer>>> hops = new HashMap<>();
		while (true) {
			String line = bf.readLine();
			if (line == null) break;
			String[] splits = line.split(",");
			int max = Integer.parseInt(splits[1]);
			int source = Integer.parseInt(splits[0].split(" ")[0]);
			int dest = Integer.parseInt(splits[0].split(" ")[1]);
			int time = Integer.parseInt(splits[4]);
			int dist = graph.getDistance(source, dest);
			if (dist >= Integer.MAX_VALUE) {
				System.out.println("Found");
				continue;
			}
			dist = dist / 1000;
			if (!times.containsKey(max)) {
				times.put(max, new HashMap<Integer, ArrayList<Integer>>());
				hops.put(max, new HashMap<Integer, ArrayList<Integer>>());
			}
			int bucket = dist / 5;
			if (!times.get(max).containsKey(bucket)) {
				times.get(max).put(bucket, new ArrayList<Integer>());
				hops.get(max).put(bucket, new ArrayList<Integer>());
			}
			ArrayList<Integer> partialPath = new ArrayList<>();
			partialPath.add(source);
			partialPath.add(dist);
			int hop = getFullPath(partialPath).size();
			times.get(max).get(bucket).add(time);
			hops.get(max).get(bucket).add(hop);
		}
		for (int i = 1; i <=7; i++) {
			Iterator it = times.get(i).entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry<Integer, ArrayList<Integer>> pair = (Entry<Integer, ArrayList<Integer>>) it.next();
				int bucket = pair.getKey();
				int avg = avg(pair.getValue());
				pow.println(i + "," + bucket + "," + avg + "," + avg(hops.get(i).get(bucket)));
			}
		}
		pow.close();
	}
	
	public int avg (ArrayList<Integer> list) {
		int sum = 0;
		for (int id : list)
			sum += id;
		return sum/list.size();
	}
	
	public void testTime () throws Exception {
		BufferedReader bf1 = new BufferedReader(new FileReader("dublin/final/outputAllStoringShortestDistanceThroughKeyword500.csv"));
		BufferedReader bf2 = new BufferedReader(new FileReader("dublin/final/outputPruneLandmarksGoalDirected500.csv"));
		HashMap<Integer, HashMap<String, Integer>> times = new HashMap<>();
		HashMap<Integer, HashMap<String, Integer>> timeLandmarks = new HashMap<>();
		String line = bf1.readLine();
		int count = Integer.MIN_VALUE;
		while (true) {
			line = bf1.readLine();
			try {
			if (line == null) break;
			String[] splits = line.split(",");
			String sd = splits[0].split(" ")[0] + "_" + splits[0].split(" ")[1];
			int max = Integer.parseInt(splits[1]);
			int time = Integer.parseInt(splits[4]);
			if (timeLandmarks.get(max) == null)
				timeLandmarks.put(max, new HashMap<String, Integer>());
			timeLandmarks.get(max).put(sd, time);
			if (max > count)
				count = max;
			}
			catch (Exception e) {
				System.out.println(line);
			}
		}
		
		line = bf2.readLine();
		while (true) {
			line = bf2.readLine();
			if (line == null) break;
			String[] splits = line.split(",");
			String sd = splits[0].split(" ")[0] + "_" + splits[0].split(" ")[1];
			int max = Integer.parseInt(splits[1]);
			int time = Integer.parseInt(splits[4]);
			if (times.get(max) == null)
				times.put(max, new HashMap<String, Integer>());
			times.get(max).put(sd, time);
		}
		
		for (int i = 1; i <= count; i++) {
			double ratio = 0;
			double speedup = 0 ;
			int total = 0;
			Iterator it = timeLandmarks.get(i).entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry<String, Integer> pair = (Entry<String, Integer>) it.next();
				if (pair.getValue() < times.get(i).get(pair.getKey().toString())) {
					ratio += (double)pair.getValue()/ (double)times.get(i).get(pair.getKey().toString());
					speedup += ((double)times.get(i).get(pair.getKey().toString()) - (double)pair.getValue()) / (double)times.get(i).get(pair.getKey().toString());
					total++;
				}
				else {
					//System.out.println("fucked");
				}
			}
			System.out.println(i + /*" " + (ratio/total) +*/ " " + (speedup/total));
			//System.out.println(i + " " + timeLandmarks.get(i).entrySet().size());
		}
		
	}
	
	public void readPaths() throws Exception {
		BufferedReader bf = new BufferedReader(new FileReader(Parameters.ROOT_OUTPUTS + "/output.csv"));
		String line = bf.readLine();
		fullPaths = new HashMap<>(); 
		partialPaths = new HashMap<>();
		
		while (true) {
			line = bf.readLine();
			if(line==null)
				break;
			String[] splits = line.split(",");
			if (splits[10].equals("-"))
				continue;
			
			int max = Integer.parseInt(splits[1]);
			
			int source = Integer.parseInt(splits[0].split(" ")[0]);

			int dest = Integer.parseInt(splits[0].split(" ")[1]);
			
			String allPaths = splits[11];
			
			if (!fullPaths.containsKey(max)) fullPaths.put(max, new HashMap<String, ArrayList<ArrayList<Integer>>>());
			if (!partialPaths.containsKey(max)) partialPaths.put(max, new HashMap<String, ArrayList<ArrayList<Integer>>>());
			
			String key = source + " " + dest;
			partialPaths.get(max).put(key, new ArrayList<ArrayList<Integer>>());
			
			String[] splits1 = allPaths.split("\\[\\[|\\]\\s\\[|\\]\\]|\\s\\]\\[|\\]\\s\\]|\\]\\[");
			for (String s : splits1){
				if (s.equals(""))
					continue;
				String[] splits2 = s.split("\\s");
				ArrayList<Integer> path = new ArrayList<>();
				path.add(source);
				for (String t : splits2) {
					path.add(Integer.parseInt(t));
				}
				partialPaths.get(max).get(key).add(path);
				path.add(dest);
			}
			fullPaths.get(max).put(key, new ArrayList<ArrayList<Integer>>());
			
			for (ArrayList<Integer> path : partialPaths.get(max).get(key)) {
				ArrayList<Integer> fullPath = getFullPath(path);
				//fullPaths.add(fullPath);
				fullPaths.get(max).get(key).add(fullPath);
			}
			/*pow.print(source + " " + dest + ",");
			analyse();
			pow.print("," );
			pow.print("[");
			for (ArrayList<Integer> path : fullPaths) {
				pow.print(path.size() + " ");
			}
			pow.println("]");*/
			
		}
		//pow.close();
		System.out.println("Done reading paths");
	}
	
	public void analyse() {
		double maximumOverlap = Double.MIN_VALUE;
		double minimumOverlap = Double.MAX_VALUE;
		int maximumDistance = Integer.MIN_VALUE;
		int minimumDistance = Integer.MAX_VALUE;
		
		Iterator it = fullPaths.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<Integer, HashMap<String, ArrayList<ArrayList<Integer>>>> pair = (Entry<Integer, HashMap<String, ArrayList<ArrayList<Integer>>>>) it.next();
			Iterator it2 = pair.getValue().entrySet().iterator();
			Map.Entry<String, ArrayList<ArrayList<Integer>>> pair2 = (Entry<String, ArrayList<ArrayList<Integer>>>) it2.next();
			ArrayList<ArrayList<Integer>> allPaths = pair2.getValue();
			for (int i = 0; i < allPaths.size(); i++) {
				for (int j = i + 1; j < allPaths.size(); j++) {
					ArrayList<Integer> path1 = allPaths.get(i);
					ArrayList<Integer> path2 = allPaths.get(j);
					double overlap = getOverlap(path1, path2);
					if (overlap < minimumOverlap)
						minimumOverlap = overlap;
					if (overlap > maximumOverlap)
						maximumOverlap = overlap;
					int max = Integer.MIN_VALUE;
					int min = Integer.MAX_VALUE;
					for (int x = 0; x < path1.size(); x++) {
						int closest = Integer.MAX_VALUE;
						for (int y = 0; y < path2.size(); y++) {
							int dist = (int) getDistance(graph.nodes.get(path1.get(x)), graph.nodes.get(path2.get(y)));
							if (dist < min)
								min = dist;
							if (dist < closest)
								closest = dist;
						}
						if (closest > max)
							max = closest;
					}
					if (max > maximumDistance)
						maximumDistance = max;
					if (min < minimumDistance)
						minimumDistance = min;
				}
			}
		//pow.print(maximumOverlap + "," + minimumOverlap + "," + maximumDistance + "," + minimumDistance);
		}
	}
	
	public double getOverlap (ArrayList<Integer> path1, ArrayList<Integer> path2) {
		HashSet<Integer> h1 = new HashSet<>(path1);
		HashSet<Integer> h2 = new HashSet<>(path2);
		int ans = 0;
		Iterator it = h1.iterator();
		while (it.hasNext()) {
			int value = (int) it.next();
			if (h2.contains(value)) 
				ans ++;
		}
		int union = path1.size() + path2.size() - ans;
		return (double)ans/union;
	}
	
	public double getDistance (Node n1, Node n2) {
		double earthRadius = 6371000; // metres
	    double dLat = Math.toRadians(n2.lat - n1.lat);
	    double dLng = Math.toRadians(n2.longi - n1.longi);
	    double sindLat = Math.sin(dLat / 2);
	    double sindLng = Math.sin(dLng / 2);
	    double a = Math.pow(sindLat, 2) + Math.pow(sindLng, 2)
	            * Math.cos(Math.toRadians(n1.lat)) * Math.cos(Math.toRadians(n2.lat));
	    double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
	    double dist = earthRadius * c;
	    return dist;
	}
	
	public ArrayList<Integer> getFullPath (ArrayList<Integer> partialPath) {
		ArrayList<Integer> ans = new ArrayList<Integer>();
		int prev = -1;
		ans.add(partialPath.get(0));
		for (int node : partialPath) {
			if (prev == -1) {
				prev = node;
				continue;
			}
			try {
				ans.addAll(getPath(prev, node));
			}
			catch (Exception e) {
				System.out.println("Exception : " + partialPath + " curr : " + node);
			}
			ans.add(node);
			prev = node;
		}
		ans = removeDuplicates(ans);
		return ans;
	}
	
	public ArrayList<Integer> removeDuplicates (ArrayList<Integer> path) {
		ArrayList<Integer> ans = new ArrayList<Integer>();
		int prev = -1;
		for (int node : path) {
			if (node == prev)
				continue;
			ans.add(node);
			prev = node;
		}
		return ans;
	}
	
	public ArrayList<Integer> getPath (int source, int dest) {
		if (source == dest)
			return new ArrayList<Integer>();
		int ans = Integer.MAX_VALUE;
		int pivot = 0;
		Iterator it = graph.nodes.get(source).label.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<Integer, Integer> pair = (Entry<Integer, Integer>) it.next();
			if (graph.nodes.get(dest).label.containsKey(pair.getKey())) {
				int dist = graph.nodes.get(dest).label.get(pair.getKey()) + pair.getValue();
				if (dist < ans) {
					ans = dist;
					pivot = pair.getKey();
				}
			}
		}
		ArrayList<Integer> result = new ArrayList<>();
		if (pivot != source && pivot != dest) {
			int nexthop = graph.nodes.get(source).nextHop.get(pivot);
			result.add(nexthop);
			result.addAll(getPath(nexthop, pivot));
			result.add(pivot);
			result.addAll(getPath(pivot, dest));
			return result;
		}
		if (pivot == source) {
			int nexthop = graph.nodes.get(dest).nextHop.get(pivot);
			result.add(nexthop);
			result.addAll(getPath(nexthop, pivot));
			result.add(pivot);
			Collections.reverse(result);
			return result;
		}
		if (pivot == dest ){
			int nexthop = graph.nodes.get(source).nextHop.get(pivot);
			result.add(nexthop);
			result.addAll(getPath(nexthop, pivot));
			result.add(pivot);
			return result;
		}
		return result;
	}
}
