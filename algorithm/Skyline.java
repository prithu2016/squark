package algorithm;

import java.util.*;
import java.io.*;

public class Skyline{
	Graph graph;
	Query query;
	ArrayList<Integer> dp;
	ArrayList<ArrayList<Integer>> paths; //node ids along the path
	int uniquePaths;
	RecursionStack recursionStack;
	double epsilon;
	int metis = 0;
	PrintWriter pow;
	int size = 0;
	
	public void init () throws Exception {
		graph = new Graph ();
		graph.size = size;
		graph.metis = metis;
		graph.readCoordinates();
		graph.readLabels();
		//graph.readNetwork();
		graph.readKeywords();
		if (Flags.LANDMARK_BASED_PRUNING)
			graph.readLandmarks();
		if (Flags.RECURSION_STACK_STORAGE) 
			recursionStack = new RecursionStack(this);
	}

	public Skyline (Graph inputGraph, Query inputQuery) {
		this.graph = inputGraph;
		this.query = inputQuery;
	}
	
	public Skyline () {
		
	}
	
	public Long start () throws Exception {
		System.out.println("Epsilon : " + epsilon);
		//pow = new PrintWriter(new FileWriter(Parameters.ROOT_OUTPUTS + "Proportional/metis2000/LBs" + epsilon + ".csv", true));


		dp = new ArrayList<Integer>();
		paths = new ArrayList<>();
		for (int i = query.minKeywordCount; i <= query.maxKeywordCount; i++ ) {
			dp.add(Integer.MAX_VALUE);
			paths.add(new ArrayList<Integer>());
		}
		if (graph.nodes.get(query.source).keywords != null) {
			ArrayList<String> keywords = graph.nodes.get(query.source).keywords;
			for (int j = 0; j < keywords.size(); j++) {
				String keyword = keywords.get(j);
				if (query.unexploredKeywords.contains(keyword)) {
					query.unexploredKeywords.remove(keyword);
					query.exploredKeywords.add(keyword);
				}
			}
		}
		if (graph.nodes.get(query.destination).keywords != null) {
			ArrayList<String> keywords = graph.nodes.get(query.destination).keywords;
			for (int j = 0; j < keywords.size(); j++) {
				String keyword = keywords.get(j);
				if (query.unexploredKeywords.contains(keyword)) {
					query.unexploredKeywords.remove(keyword);
					query.exploredKeywords.add(keyword);
				}
			}
		}
		/*int covered = query.maxKeywordCount - query.unexploredKeywords.size();
		System.out.println("Covered : " + covered);
		int directDistance = graph.getDistance(query.source, query.destination);
		for (int i = 0; i < covered; i++) { 
			dp.set(i, directDistance);
		}
		if (covered > 0)
			System.out.println("Set dp : " + dp);*/
		return null;
	}
	
	public void getQuery () throws Exception {
		query = new Query (graph);
		query.start();
		query.print();
	}
	
	public void makeQuery (int start, int end, int minCount, int maxCount) throws Exception {
		query = new Query (graph);
		query.start(start, end, minCount, maxCount);
		query.print();
	}

	public void makeQuery (int start, int end, int minCount, int maxCount, int bucket) throws Exception {
		query = new Query (graph);
		query.start(start, end, minCount, maxCount, bucket);
		query.print();
	}
	
	public void makeQuery (int start, int end, int minCount, int maxCount, ArrayList<String> queryKeywords) throws Exception {
		query = new Query (graph);
		query.start(start, end, minCount, maxCount, queryKeywords);
		query.print();
	}

	public void recur (int source, int distTillNow) throws Exception {
		storeNextHopAndDfs(source, distTillNow, new ArrayList<Integer>(), 0);
		//pow.close();
	}
	
	public void storeNextHopAndDfs (int source, int distTillNow, ArrayList<Integer> pathTillNow, int distToDest) throws Exception {
	
	}
	
	public boolean prune (int distTillNow, ArrayList<Pair> minimumDistancesToKeywords) {
		int exploredKeywordsCount = query.exploredKeywords.size();
		try {	
			for (int x = 0; x < minimumDistancesToKeywords.size(); x++) {
				int index = exploredKeywordsCount - query.minKeywordCount + x + 1;
				int minimumDistance = minimumDistancesToKeywords.get(x).first;
				int LB = minimumDistance + distTillNow;
				//pow.println((x+1+exploredKeywordsCount) + "," + LB);
				int diff = dp.get(index) - (minimumDistance + distTillNow);
				if (dp.get(index) == Integer.MAX_VALUE) return false;
				//System.out.println("Print : " + epsilon);
				if (dp.get(index) > (1+epsilon)*LB) {
					return false;
				}
			}
			return true;
		}
		catch (Exception e) {System.out.println(e);}
		return true;
	}
	
		
	/*public void dfs(int source, ArrayList<NextHop> nextHops, int distTillNow, ArrayList<Integer> pathTillNow,
			ArrayList<Pair> minimumDistancesThroughKeywords) throws Exception {
		if (Flags.RECURSION_STACK_STORAGE) {
			if (recursionStack.useRecursionStack(source, distTillNow) == true) return;
		}
		
		ArrayList<String> oldUnexplored = new ArrayList<>(query.unexploredKeywords);
		ArrayList<String> oldExplored = new ArrayList<>(query.exploredKeywords);
		ArrayList<Integer> oldPathTillNow = new ArrayList<>(pathTillNow);
		ArrayList<Integer> oldDp = new ArrayList<>(dp);
		
		for (NextHop nx : nextHops) {
			if (graph.nodes.get(nx.nextHop).visited == true)
				continue;
			
			graph.nodes.get(nx.nextHop).visited = true;
			
			if (graph.nodes.get(nx.nextHop).keywords != null) {
				ArrayList<String> keywords = graph.nodes.get(nx.nextHop).keywords;
				for (int j = 0; j < keywords.size(); j++) {
					String keyword = keywords.get(j);
					if (query.unexploredKeywords.contains(keyword)) {
						query.unexploredKeywords.remove(keyword);
						query.exploredKeywords.add(keyword);
					}
				}
			}
			
			pathTillNow.add(nx.nextHop);
			
			storeNextHopAndDfs (nx.nextHop, distTillNow + nx.distanceToSource, pathTillNow, nx.distanceToDest);
			
			query.unexploredKeywords.clear();
			query.exploredKeywords.clear();
			pathTillNow.clear();
			
			query.unexploredKeywords.addAll(oldUnexplored);
			query.exploredKeywords.addAll(oldExplored);
			pathTillNow.addAll(oldPathTillNow);
			
			graph.nodes.get(nx.nextHop).visited = false;
			
			//see if i can prune source
			
			if (prune(distTillNow, minimumDistancesThroughKeywords)) {
				if (minimumDistancesThroughKeywords.size() > 0)	{
					query.pruneCount++;
				}
				break;
			}
		}
		
		if (Flags.RECURSION_STACK_STORAGE) {
			recursionStack.storeRecursionStack(source, distTillNow, oldDp, oldUnexplored);
		}
	}*/
	
	public void dfs(int source, ArrayList<ArrayList<Pair>> keywordDistances, int distTillNow, ArrayList<Integer> pathTillNow,
			ArrayList<Pair> minimumDistancesThroughKeywords) throws Exception {
		if (Flags.RECURSION_STACK_STORAGE) {
			if (recursionStack.useRecursionStack(source, distTillNow) == true) return;
		}
		ArrayList<String> oldUnexplored = new ArrayList<>(query.unexploredKeywords);
		ArrayList<String> oldExplored = new ArrayList<>(query.exploredKeywords);
		ArrayList<Integer> oldPathTillNow = new ArrayList<>(pathTillNow);
		ArrayList<Integer> oldDp = new ArrayList<>(dp);
		
		int numberOfKeywords = keywordDistances.size();
		ArrayList<Integer> indices = new ArrayList<>();
		
		PriorityQueue<Pair> pq = new PriorityQueue<>(10, new PairSortingUsingFirstAndThird());
		HashMap<Pair, Integer> bucketOfPairs = new HashMap<>();
		for (int i = 0; i < numberOfKeywords; i++) {
			indices.add(0);
			if (keywordDistances.get(i).size() != 0) {
				pq.add(keywordDistances.get(i).get(0));
				bucketOfPairs.put(keywordDistances.get(i).get(0), i);

			}
		}
		/*
		System.out.println("Source : " + source);
		for (ArrayList<Pair> pairs : keywordDistances) {
			for (Pair p : pairs) {
				System.out.print(p.second + " ");
			}
		}
		System.out.println("\n****");*/
		
		HashSet<Integer> seenIds = new HashSet<Integer>();
		while (pq.size() > 0) {
			
			Pair p = pq.poll();
			int bucket = bucketOfPairs.get(p);
			indices.set(bucket, indices.get(bucket)+1);
			if (indices.get(bucket) < keywordDistances.get(bucket).size()) {
				Pair newP = keywordDistances.get(bucket).get(indices.get(bucket));
				pq.add(newP);
				bucketOfPairs.put(newP, bucket);
			}

			if (seenIds.contains(p.second)) continue;
			seenIds.add(p.second);
			
			NextHop nx = new NextHop();
			nx.nextHop = p.second;
			nx.distanceToSource = p.first;
			nx.distanceToDest = p.third;
			
			if (graph.nodes.get(nx.nextHop).visited == true)
				continue;
			
			graph.nodes.get(nx.nextHop).visited = true;
			
			if (graph.nodes.get(nx.nextHop).keywords != null) {
				ArrayList<String> keywords = graph.nodes.get(nx.nextHop).keywords;
				for (int j = 0; j < keywords.size(); j++) {
					String keyword = keywords.get(j);
					if (query.unexploredKeywords.contains(keyword)) {
						query.unexploredKeywords.remove(keyword);
						query.exploredKeywords.add(keyword);
					}
				}
			}
			
			pathTillNow.add(nx.nextHop);
			
			storeNextHopAndDfs (nx.nextHop, distTillNow + nx.distanceToSource, pathTillNow, nx.distanceToDest);
			
			query.unexploredKeywords.clear();
			query.exploredKeywords.clear();
			pathTillNow.clear();
			
			query.unexploredKeywords.addAll(oldUnexplored);
			query.exploredKeywords.addAll(oldExplored);
			pathTillNow.addAll(oldPathTillNow);
			
			graph.nodes.get(nx.nextHop).visited = false;
			
			//see if i can prune source
			
			if (prune(distTillNow, minimumDistancesThroughKeywords)) {
				if (minimumDistancesThroughKeywords.size() > 0)	{
					query.pruneCount++;
				}
				break;
			}
		}
		
		if (Flags.RECURSION_STACK_STORAGE) {
			recursionStack.storeRecursionStack(source, distTillNow, oldDp, oldUnexplored);
		}
	}
	
/*	public void getNextHop (int source, ArrayList<NextHop> nextHops, 
			ArrayList<Pair> minimumDistancesThroughKeywords) throws Exception {
		if (source == query.destination) {
			return;
		}
		
		ArrayList<Pair> keywordDistances = new ArrayList<Pair>();
		for (String keyword : query.unexploredKeywords) {
			ArrayList<Pair> allOccurences = graph.getDistanceThroughKeyword(source, query.destination, keyword);
			int size = allOccurences.size();
			if (size > 0) {
				keywordDistances.addAll(allOccurences.subList(0, size-1));
				minimumDistancesThroughKeywords.add(allOccurences.get(size-1));
			}
		}
		
		// GOAL DIRECTED SEARCH
		if (Flags.GOAL_DIRECTED_SEARCH)
			Collections.sort(keywordDistances, new PairSortingUsingFirstAndThird());
		
		for (Pair pair : keywordDistances) {
			NextHop nx = new NextHop();
			nx.nextHop = pair.second;
			nx.distanceToSource = pair.first;
			nx.distanceToDest = pair.third;
			nextHops.add(nx);
		}
	}
	
	*/
/*	public void getNextHop (int source, ArrayList<NextHop> nextHops) throws Exception {
		if (source == query.destination) {
			return;
		}
		
		ArrayList<Pair> keywordDistances = new ArrayList<Pair>();
		for (String keyword : query.unexploredKeywords) {
			ArrayList<Pair> allOccurences = graph.getDistanceThroughKeyword(source, query.destination, keyword);
			int size = allOccurences.size();
			if (size > 0) {
				keywordDistances.addAll(allOccurences.subList(0, size-1));
			}
		}
		
		// GOAL DIRECTED SEARCH
		if (Flags.GOAL_DIRECTED_SEARCH)
			Collections.sort(keywordDistances, new PairSortingUsingFirstAndThird());
		
		for (Pair pair : keywordDistances) {
			NextHop nx = new NextHop();
			nx.nextHop = pair.second;
			nx.distanceToSource = pair.first;
			nx.distanceToDest = pair.third;
			nextHops.add(nx);
		}
	}*/
	
	
	public void getNextHop (int source, ArrayList<ArrayList<Pair>> keywordDistances) throws Exception {
		if (source == query.destination) {
			return;
		}
		for (String keyword : query.unexploredKeywords) {
			ArrayList<Pair> allOccurences = graph.getDistanceThroughKeyword(source, query.destination, keyword);
			int size = allOccurences.size();
			if (size > 0) {
				ArrayList<Pair> distances = new ArrayList<>();
				distances.addAll(allOccurences.subList(0, size-1));
				// GOAL DIRECTED SEARCH
				if (Flags.GOAL_DIRECTED_SEARCH)
					Collections.sort(distances, new PairSortingUsingFirstAndThird());
				keywordDistances.add(distances);	
				//System.out.println("Size for : " + keyword + " = " + distances.size());
			}
		}
		
		/*ArrayList<Thread> threads = new ArrayList<>();
		ArrayList<RunnableDemo> runnable = new ArrayList<>();
		for (int i = 0; i < keywordDistances.size(); i++) {
			RunnableDemo R1 = new RunnableDemo(keywordDistances.get(i));
			Thread t = new Thread(R1);
			t.start();
			threads.add(t);
			runnable.add(R1);
		}
		
		for (Thread t : threads) {
			   t.join();
		}
		for (int i = 0; i < runnable.size(); i++) {
			keywordDistances.set(i, runnable.get(i).getValue());;
		}*/
	}
	
	public void updateDp (int source, int distTillNow, ArrayList<Integer> pathTillNow, int distToDest) {
		//System.out.println("In update : " + source);
		/*Analyse an = new Analyse(graph);
		ArrayList<Integer> shortestPath = an.getPath(source, query.destination);
		HashSet<String> unexploredSet = new HashSet<String>(query.unexploredKeywords);
		
		for (int id : shortestPath) {
			ArrayList<String> keywordsOfId = graph.nodes.get(id).keywords;
			if (keywordsOfId == null) continue;
			for (String key : keywordsOfId) {
				if (unexploredSet.contains(key)) unexploredSet.remove(key);
			}
			if (unexploredSet.size() == 0) break;
		}
		
		int count = query.unexploredKeywords.size() - unexploredSet.size();
		//System.out.println("Count : " + count);		
		*/
		int exploredKeywordsCount = query.exploredKeywords.size();
		boolean flag = false;
		if (exploredKeywordsCount >= query.minKeywordCount) {
			for (int i = (exploredKeywordsCount - query.minKeywordCount); i >= 0 ; i--) {
				if (dp.get(i) >  distTillNow + distToDest ) {
					dp.set(i, distTillNow + distToDest);
					
					ArrayList<Integer> path = new ArrayList<>();
					path.addAll(pathTillNow);
					paths.set(i, path);
					
					query.updatesCount++;
					//System.out.println("Updated : " + i + " with "  + query.pathsExplored + " source : " + source);
					flag = true;
				}

				else {
					break;
				}
			}
		}
		/*if (flag == true) {
			System.out.println("Dp : " + dp);
			for (int x = 0; x < paths.size(); x++) {
				System.out.println(paths.get(x));
			}
		}*/
	
	}

	
}
