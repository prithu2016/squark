package algorithm;

import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.*;

public class Query {
	
	int source;
	int destination;
	int minKeywordCount;
	int maxKeywordCount;
	
	int pruneCount;
	int landmarkPruneCount;
	
	int pathsExplored;
	int updatesCount;
	
	
	HashSet<String> unexploredKeywords = new HashSet<String>();;
	HashSet<String> exploredKeywords = new HashSet<String>();;
	
	ArrayList<String> keywords = new ArrayList<>();
	
	Graph graph;
	
	public Query() {
		
	}
	
	public Query (Graph graph) {
		pruneCount = 0;
		landmarkPruneCount = 0;
		pathsExplored = 0;
		updatesCount = 0 ;
		this.graph = graph;
	}
	
	public Query deepCopy() {
		Query newQuery = new Query(this.graph);
		newQuery.source = source;
		newQuery.destination = destination;
		newQuery.maxKeywordCount = maxKeywordCount;
		newQuery.minKeywordCount = minKeywordCount;
		newQuery.pruneCount = 0;
		newQuery.landmarkPruneCount = 0;
		newQuery.pathsExplored = 0;
		newQuery.updatesCount = 0;
		newQuery.unexploredKeywords.addAll(unexploredKeywords);
		newQuery.exploredKeywords.addAll(exploredKeywords);
		newQuery.keywords.addAll(keywords);
		return newQuery;
	}
	
	public void clean() {
		pruneCount = 0;
		pathsExplored = 0;
		updatesCount = 0;
		landmarkPruneCount = 0;
	}
	
	
	public void start () {
		Random r = new Random();
		source = r.nextInt(graph.n);
		destination = r.nextInt(graph.n);
		while (destination == source) {
			destination = r.nextInt(graph.n);
		}
		r = new Random();
		maxKeywordCount = 4;
		minKeywordCount = 1;
		unexploredKeywords = generatePermutation(maxKeywordCount, true);
	}
	
	public void start (int start, int end, int minCount, int maxCount) {
		source = start;
		destination = end;
		minKeywordCount = minCount;
		maxKeywordCount = maxCount;
		unexploredKeywords = generatePermutation(maxKeywordCount, true);
		keywords.addAll(unexploredKeywords);
	}
	
	public void start (int start, int end, int minCount, int maxCount, int bucket) {
		source = start;
		destination = end;
		minKeywordCount = minCount;
		maxKeywordCount = maxCount;
		unexploredKeywords = getBucketedKeywords(maxKeywordCount, bucket);
		keywords.addAll(unexploredKeywords);
	}

	public HashSet<String> getBucketedKeywords (int maxKeywordCount, int bucket) {
		int total = graph.allKeywords.size();
		int bucketSize = total/10;
		int start = (bucket-1) * bucketSize;
		int end = bucket * bucketSize;
		if (bucket == 10) end = total;
		ArrayList<String> sub = new ArrayList<String>();
		sub.addAll(graph.allKeywords.subList(start, end));
		HashSet<String> ans = new HashSet<String>();
		while (ans.size() < maxKeywordCount) {
			Collections.shuffle (sub);
			ans.add(sub.get(0));
		}
		return ans;

	}

	public void start (int start, int end, int minCount, int maxCount, ArrayList<String> queryKeywords) {
		source = start;
		destination = end;
		minKeywordCount = minCount;
		maxKeywordCount = maxCount;
		unexploredKeywords.addAll(queryKeywords);
		keywords.addAll(unexploredKeywords);
	}
	
	public void generateQueriesDistanceBased() throws Exception {
		PrintWriter pow1 = new PrintWriter(new FileWriter( Parameters.ROOT_OUTPUTS + "/Input.csv"));
		HashSet<String> visited = new HashSet<String>();
		// distances = < 5, < 10, < 25, < 50, < 100, < 150, < 200, < 300, < 500 > 500
		HashMap<Integer, Integer> counts = new HashMap<Integer, Integer>();
		ArrayList<Integer> distances = new ArrayList<Integer>();
		distances.add(5);
		distances.add(10);
		distances.add(15);
		distances.add(20);
		distances.add(25);
		distances.add(30);
		distances.add(35);
		distances.add(40);
		distances.add(45);
		distances.add(50);
		/*distances.add(60);
		distances.add(70);
		distances.add(80);
		distances.add(90);
		distances.add(100);
		distances.add(120);
		distances.add(140);
		distances.add(160);
		distances.add(180);
		distances.add(200);
		/*distances.add(230);
		distances.add(260);
		distances.add(290);
		distances.add(320);
		distances.add(350);
		distances.add(380);*/

		ArrayList<String> allKeywords = new ArrayList<String>();
		for (int i = 0; i < graph.n; i++) {
			if (graph.nodes.get(i).keywords != null)
			allKeywords.addAll(graph.nodes.get(i).keywords);
		}

		for (int i : distances)
			counts.put(i, 0);
		while (true) {
			boolean flag = false;
			for (int i : distances) {
				if (counts.get(i) < 10) flag = true;
			}
			if (!flag) break;
			Random r = new Random();
			int s = r.nextInt(graph.n);
			int dest = r.nextInt(graph.n);
			String key1 = s + "_" + dest;
			String key2 = dest + "_" + s;
			int dist = (graph.getDistance(s, dest));
			while (dest == s || visited.contains(key1) || visited.contains(key2) || dist == Integer.MAX_VALUE) {
				dest = r.nextInt(graph.n);
				key1 = s + "_" + dest;
				key2 = dest + "_" + s;
				dist = graph.getDistance (s, dest);
			}
			dist = dist / 1000;
			System.out.println("Found : " + s + " " + dest + " " + dist);
			int bucket = -1;
			for (int i = 0; i < distances.size() - 2; i++) {
				if (dist > distances.get(i) && dist <= distances.get(i+1)) {
					bucket = distances.get(i+1);
					break;
				}
			}
			if (bucket == -1) {
				if (dist <= distances.get(0))
					bucket = distances.get(0);
				else
					bucket = distances.get(distances.size() - 1);
			}
			visited.add(key1);
			if (counts.get(bucket) < 10) {
				counts.put(bucket, counts.get(bucket) + 1);
				ArrayList<String> keywords1 = new ArrayList<String>();
				while (keywords1.size() < 4) {
					int i = r.nextInt(allKeywords.size());
					if (!keywords1.contains(allKeywords.get(i)))
						keywords1.add(allKeywords.get(i));
				}
				System.out.println(s + " " + dest + " " + dist + " " +  bucket);
				pow1 = new PrintWriter(new FileWriter( Parameters.ROOT_OUTPUTS + "/Input.csv", true));
				pow1.print(s + " " + dest + "," + "4" + "," + 1 + ",");
				print(pow1, keywords1);
				pow1.println("," + dist + "," + bucket);
				pow1.close();
			}
		}
		pow1.close();
	}
	
	public void generateQueries () throws Exception {
		/*PrintWriter pow1 = new PrintWriter(new FileWriter( Parameters.ROOT_DATA + Parameters.CITY + graph.size + "/finalInputMostFrequent.csv"));
		PrintWriter pow2 = new PrintWriter(new FileWriter( Parameters.ROOT_DATA + Parameters.CITY +  graph.size + "/finalInputLeastFrequent.csv"));
		PrintWriter pow3 = new PrintWriter(new FileWriter( Parameters.ROOT_DATA + Parameters.CITY +  graph.size + "/finalInputProportionalFrequent.csv"));*/
		PrintWriter pow1 = new PrintWriter(new FileWriter( Parameters.ROOT_OUTPUTS /*+ graph.size*/ + "/finalInputMostFrequent.csv"));
		PrintWriter pow2 = new PrintWriter(new FileWriter( Parameters.ROOT_OUTPUTS /*+  graph.size*/ + "/finalInputLeastFrequent.csv"));
		PrintWriter pow3 = new PrintWriter(new FileWriter( Parameters.ROOT_OUTPUTS /*+  graph.size*/ + "/finalInputProportionalFrequent.csv"));



		pow1.println("Source & Dest,MaxKeywords,MinKeywords,Keywords");
		pow2.println("Source & Dest,MaxKeywords,MinKeywords,Keywords");
		pow3.println("Source & Dest,MaxKeywords,MinKeywords,Keywords");

		int sdpairs = 0;
		ArrayList<String> allKeywords = new ArrayList<String>();
		for (int i = 0; i < graph.n; i++) {
			if (graph.nodes.get(i).keywords != null)
			allKeywords.addAll(graph.nodes.get(i).keywords);
		}
		while (sdpairs < 100) {
			int i = 1;
			Random r = new Random();
			int s = r.nextInt(graph.n);
			int dest = r.nextInt(graph.n);
			while (dest == s || graph.getDistance(s, dest) == Integer.MAX_VALUE) {
				dest = r.nextInt(graph.n);
			}
			ArrayList<String> keywords3 = new ArrayList<String>();
			while (i < 13) {
				while (true) {
					int rn = r.nextInt(allKeywords.size());
					if (!keywords3.contains(allKeywords.get(rn))) {
						keywords3.add(allKeywords.get(rn));
						break;
					}
				}
				pow1.print(s + " " + dest + "," + i + "," + 1 + ",");
				pow2.print(s + " " + dest + "," + i + "," + 1 + ",");
				pow3.print(s + " " + dest + "," + i + "," + 1 + ",");
				print(pow3, keywords3);
				ArrayList<String> keywords1 = new ArrayList<String>(generatePermutation(i, true));
				print (pow1, keywords1);
				ArrayList<String> keywords2 = new ArrayList<String>(generatePermutation(i, false));
				print (pow2, keywords2);

				i++;	
			}
			sdpairs++;
		}
		pow1.close();
		pow2.close();
		pow3.close();


	}

	public void print (PrintWriter pow, ArrayList<String> keywords) {
		for (String key : keywords) {
			pow.print(key + " ");
		}
		pow.println();
	}	

	public HashSet<String> generatePermutation(int required, boolean highToLow) {
		HashSet<String> ans  = new HashSet<String>();
		if (highToLow) {
			for (int i = 0; i < required; i++)
				ans.add(graph.allKeywords.get(i));
		}
		else {
			for (int i = 0; i < required; i++)
				ans.add(graph.allKeywords.get(graph.allKeywords.size() - i - 1));

		}
		return ans;
	}
	
	public void print() {
		System.out.println("Source : " + source);
		System.out.println("Destination : " + destination);
		System.out.println("Min : " + minKeywordCount + " Max " + maxKeywordCount);
		System.out.println(unexploredKeywords);
	}
}
