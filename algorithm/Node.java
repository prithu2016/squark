package algorithm;

import java.util.*;
import java.util.HashSet;

public class Node {
	public int id;
	public double lat;
	public double longi;
	HashMap<Integer, Integer> label = new HashMap<Integer, Integer>();
	
	boolean visited = false;
	
	//for directed
	HashMap<Integer, Integer> labelOut;
	HashMap<Integer, Integer> labelIn;
	HashSet<Integer> reverseLabelIn; //some has in from me
	HashSet<Integer> reverseLabelOut; //some has out on me
	
	ArrayList<Integer> distanceToLandmarks; // distance to closest occurrence of a keyword
	HashMap<String, Integer> distanceToDestThroughKeyword;
	HashMap<String, Integer> landmarkDistanceToDestThroughKeyword;
	HashMap<StringBuilder, HashMap<Integer, Integer>> futureInfoDist;
	int landmarkAssignment;
	public ArrayList<String> keywords;
	public HashMap<Integer, Integer> nextHop;
}
