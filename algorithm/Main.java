package algorithm;

import java.io.*;
import java.util.*;

public class Main {

	public static void main(String[] args) throws Exception {
		Main m = new Main();
		m.squark();
		//m.test();
		//m.generateQueries();
	}

	public void generateQueries() throws Exception {
		Graph graph = new Graph();
		//graph.size = 5000;
		graph.readCoordinates();
		graph.readLabels();
		graph.readKeywords();
		Query query = new Query(graph);
		query.generateQueries(); 
	}

	public void analyse() throws Exception{
		/*AnalysePaths ap = new AnalysePaths();
		ap.init();
		ap.testTime();
		ap.readPaths();*/
	}
	
	
	public void squark () throws Exception {
		
		ArrayList<Integer> sizes = new ArrayList<Integer>();
		/*sizes.add(10000);
		sizes.add(25000);
		sizes.add(50000);
		*/
		Long start, end;
		ArrayList<String> queryTypes = new ArrayList<String>();
		queryTypes.add("Proportional");
		//queryTypes.add("Most");
		//queryTypes.add("Least");
		Scanner sc = new Scanner (System.in);
		/*for (int metis = 1300; metis <= 1300; metis = metis +200)*/ {
			/*Skyline skyline = new Skyline();
			skyline.metis = metis;
			skyline.init(); 
			Graph graph = skyline.graph;
			*/
 			
			/*for (String queryType : queryTypes)*/ /*for (int size : sizes)*/ {
					int metis = -1;
					if (Parameters.CITY.equals("sydney")) metis = 1300;
					if (Parameters.CITY.equals("london") && Parameters.VERSION.equals("New")) metis = 5000;
					if(Parameters.CITY.equals("london") && Parameters.VERSION.equals("Reduced")) metis = 2500;
					if(Parameters.CITY.equals("london") && Parameters.VERSION.equals("Reduced2")) metis = 1400;
					if(Parameters.CITY.equals("london") && Parameters.VERSION.equals("Reduced3")) metis = 1400;				
					if (Parameters.CITY.equals("dublin")) metis = 150;
					Skyline skyline = new Skyline();
					skyline.metis = metis;
					//skyline.size = size;
					skyline.init(); 
					Graph graph = skyline.graph;

					for (double epsilon = 0.2; epsilon >= 0.0; epsilon = epsilon-0.2) {
					
					System.out.println(metis /*+ " " + queryType*/ + " " + epsilon);
					skyline.epsilon = epsilon;	
                                        //String outputFilename = "output_" + Flags.LANDMARK_BASED_PRUNING + "_" + Flags.GOAL_DIRECTED_SEARCH +  ".csv";
					String outputFilename = "output_" + epsilon + "_" + metis + ".csv"; 
					outputFilename = Parameters.ROOT_OUTPUTS /*+ size + "/"*/ + outputFilename;
					PrintWriter out = new PrintWriter(new FileWriter(outputFilename, true));
					BufferedReader bf = new BufferedReader(new FileReader(Parameters.ROOT_OUTPUTS /*+ size*/ + "/Input.csv"));
					
					String line = bf.readLine();
					int count = 0;	
					try {
						while (true) {
							
							line = bf.readLine();
							if (line == null /*|| count == 20*/)
								break;
							
							skyline = new SkylinePrune(graph);
							skyline.epsilon = epsilon;							
							ArrayList<String> queryKeywords = new ArrayList<String>();

							int source = Integer.parseInt(line.split(",")[0].split(" ")[0]);
							int dest = Integer.parseInt(line.split(",")[0].split(" ")[1]);
							int max = Integer.parseInt(line.split(",")[1]);
							if (max == 7) count++;
							if (max >7 ) continue; 
							//ArrayList<String> queryKeywords = new ArrayList<String>();
							
							/*System.out.println("Enter input");
							skyline.epsilon = Double.parseDouble(sc.nextLine());
							int source = sc.nextInt();
							int dest = sc.nextInt();
							int max = sc.nextInt();
							String temp = sc.nextLine();
							for (int x = 0; x < max; x++) queryKeywords.add(sc.nextLine().toString());	
							*/
							/*if (queryKeywords.size() == 0)*/ {	
							String keywords = line.split(",")[3];
							
							StringTokenizer stt = new StringTokenizer(keywords, " ");
							while (stt.hasMoreTokens()) 
								queryKeywords.add(stt.nextToken().toString());
							}
							/*source = 32076;
							dest = 181639;
							max = 5;
							queryKeywords = new ArrayList<String>();
							queryKeywords.add("playground");
							queryKeywords.add("parking");
							queryKeywords.add("establishment");
							queryKeywords.add("cafe");
							queryKeywords.add("point_of_interest");*/
							//for (int bucket = 1; bucket <= 10; bucket++) { 
								graph.clean();
								skyline.makeQuery(source, dest, 1, max, queryKeywords);

								//skyline.makeQuery (38049, 147944, 1, 5);
								out = new PrintWriter(new FileWriter(outputFilename, true));

								start = System.currentTimeMillis();
								end = skyline.start();
								//end = System.currentTimeMillis();
								print(out, skyline, (end-start)/*, line.split(",")[5]*/);
								out.close();
							//}
						}
					}
					catch (Exception e) {
						System.out.println(e);
					}
					finally {
						out.close();
					}
				}
			}
		}
	}
	
	public void print (PrintWriter out, Skyline skyline, Long time/*, String bucket*/) {
		out.print(skyline.query.source + " " + skyline.query.destination + "," + skyline.query.maxKeywordCount 
				+ "," + skyline.query.minKeywordCount + ",");
		out.print("[");
		for (String keyword : skyline.query.keywords) {
			out.print(keyword + " "); 
		}
		out.print("]");
		out.print(",");
		
		
		System.out.println("Time Prune : " + time);
		System.out.println("Prunings : " + skyline.query.pruneCount);
		System.out.println("Landmark pruning : " + skyline.query.landmarkPruneCount);
		System.out.println("Nodes touched Prune : " + skyline.query.pathsExplored);
		System.out.println("Updates count : " + skyline.query.updatesCount);
		System.out.println("--------------------------");
		
		
		out.print(time + "," + skyline.query.landmarkPruneCount + "," +  skyline.query.pruneCount + "," + skyline.query.pathsExplored + "," + skyline.query.updatesCount + ",");
		
		out.print("[");
		for (int i : skyline.dp) {
			out.print(i + " "); 
		}
		out.print("]");
		out.print(",");
		out.print(skyline.uniquePaths);
		out.print(",");
		
		out.print("[");
		for (ArrayList<Integer> path : skyline.paths) {
			out.print("[");
			for (int i : path) {
				out.print(i + " ");
			}
			out.print("]");
		}
		//out.print("]," + bucket);
		out.println("]");
		
	}

}
