package algorithm;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import data.node;

public class Partition {

	public String city = "dublin";
	public String root = "/Users/chendu/Desktop/DDP/New/Data/";
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	public void buildPartitionIndex() throws Exception {
		PrintWriter pow = new PrintWriter(new FileWriter(root + city + "/partitionIndex.txt"));
		BufferedReader bf = new BufferedReader(new FileReader(root + city + "/opticsCluster.txt"));
		Graph graph = new Graph();
		graph.readCoordinates();
		ArrayList<Integer> noise = new ArrayList<Integer>();
		HashMap<Integer, ArrayList<Integer>> reverseVisited = new HashMap<>();
		HashMap<Integer, Integer> clusterAssignment = new HashMap<>();
		
		while (true) {
			String line = bf.readLine();
			if (line == null) break;
			String[] splits = line.split("\t");
			int id = Integer.parseInt(splits[0]);
			if (splits[1].equals("noise")) { noise.add(id); continue;}
			int clusterId = Integer.parseInt(splits[1]);
			if (!reverseVisited.containsKey(clusterId)) reverseVisited.put(clusterId, new ArrayList<Integer>());
			reverseVisited.get(clusterId).add(id);
			clusterAssignment.put(id, clusterId);
		}
		for (int n : noise) {
			double minDist = Double.MAX_VALUE;
			int clusterId = 0;
			Iterator it = clusterAssignment.entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry<Integer, Integer> pair = (Entry<Integer, Integer>) it.next();
				double dist = get_dist(graph.nodes.get(n), graph.nodes.get(pair.getKey()));
				if (dist < minDist) {
					minDist = dist;
					clusterId = pair.getValue(); 
				}
			}
			clusterAssignment.put(n, clusterId);
			reverseVisited.get(clusterId).add(n);
		}
		
		Iterator it = reverseVisited.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<Integer, ArrayList<Integer>> pair = (Entry<Integer, ArrayList<Integer>>) it.next();
			pow.print(pair.getKey() + " ");
			for (int id : pair.getValue()) {
				pow.print(id + " ");
			}
			pow.println();
		}
		pow.close();
	}
	public double get_dist (Node n1, Node n2) {
		double earthRadius = 6371000; // metres
	    double dLat = Math.toRadians(n2.lat - n1.lat);
	    double dLng = Math.toRadians(n2.longi - n1.longi);
	    double sindLat = Math.sin(dLat / 2);
	    double sindLng = Math.sin(dLng / 2);
	    double a = Math.pow(sindLat, 2) + Math.pow(sindLng, 2)
	            * Math.cos(Math.toRadians(n1.lat)) * Math.cos(Math.toRadians(n2.lat));
	    double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
	    double dist = earthRadius * c;
	    return dist;
	}
}
