package algorithm;

import java.util.ArrayList;
import java.util.Collections;

public class RunnableDemo implements Runnable {
	   private Thread t;
	   ArrayList<Pair> keywordDistances;
	   
	   RunnableDemo( ArrayList<Pair> inputKeywordDistances){
		   keywordDistances = inputKeywordDistances;
	   }
	   public void run() {
	      try {
	    	  Collections.sort(keywordDistances, new PairSortingUsingFirstAndThird());
	     } catch (Exception e) {
	    	 System.out.println(e);
	     }
	   }
	   public ArrayList<Pair> getValue() {
	         return keywordDistances;
	     }

	}
