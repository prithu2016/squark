package algorithm;

import java.io.*;
import java.util.*;
import java.util.Map.Entry;

public class Graph {
	
	public ArrayList<Node> nodes = new ArrayList<Node>();
	public ArrayList<ArrayList<Edge>> edges = new ArrayList<ArrayList<Edge>>();
	HashMap<Integer, Landmark> landmarks = new HashMap<>();
	HashMap<String, Keyword> keywords = new HashMap<>();
	public int n;
	public ArrayList<String> allKeywords = new ArrayList<>();
	long totalPivotCount = 0;
	int metis = 0;
	public int size;
	//String size = "";

	public void readCoordinates () throws Exception {
		String filename = Parameters.ROOT_DATA + Parameters.CITY + "/coordinatesSerial.txt";
		//String filename = Parameters.ROOT_OUTPUTS + size + "/coordinatesSerial.txt";

		System.out.println(filename);
		BufferedReader bf1 = new BufferedReader(new FileReader(filename));
		String line = "";
		while (true) {
			line = bf1.readLine();
			if (line == null)
				break;
			String[] splits = line.split(" ");
			Node n1 = new Node();
			n1.id = Integer.parseInt(splits[0]);
			n1.lat = Double.parseDouble(splits[1]);
			n1.longi = Double.parseDouble(splits[2]);
			n1.label.put(n1.id, 0);
			if (Flags.SHORTEST_DISTANCE_THROUGH_KEYWORD_STORAGE) {
				n1.distanceToDestThroughKeyword = new HashMap<String, Integer>();
			}
			if (Flags.SHORTEST_LANDMARK_DISTANCE_THROUGH_KEYWORD_STORAGE) {
				n1.landmarkDistanceToDestThroughKeyword = new HashMap<>();
			}
			if (Flags.RECURSION_STACK_STORAGE) {
				n1.futureInfoDist = new HashMap<StringBuilder, HashMap<Integer, Integer>>();
			}
			if (Flags.LANDMARK_BASED_PRUNING) {
				n1.distanceToLandmarks = new ArrayList<Integer>();
			}
			nodes.add(n1);
		}
		n = nodes.size();
		System.out.println("Done reading coordinates");
	}
	
	public void clean() {
		for (Node node : nodes) {
			node.futureInfoDist = new HashMap<>();
			node.distanceToDestThroughKeyword = new HashMap<>();
			node.landmarkDistanceToDestThroughKeyword = new HashMap<>();
		}
	}
	
	public void readNetwork () throws Exception {
		for (int i = 0; i < n; i++) {
			edges.add(new ArrayList<Edge>());
		}
		
		BufferedReader bf1 = new BufferedReader(new FileReader(/*Parameters.ROOT_DATA + Parameters.CITY*/ Parameters.ROOT_OUTPUTS + size +  "/graphSerialIntegerWeights.txt"));
		String line = bf1.readLine();
		
		while (true) {
			line = bf1.readLine();
			if (line == null || line == "" || line == " ")
				break;
			String[] splits = line.split(" ");
			int id1 = Integer.parseInt(splits[0]);
			int id2 = Integer.parseInt(splits[1]);
			int weight = Integer.parseInt(splits[2]);
			Edge e1 = new Edge(id2, weight);
			Edge e2 = new Edge(id1, weight);
			edges.get(id1).add(e1);
			edges.get(id2).add(e2);
			/*int dist = getDistance (id1, id2);
			if (dist != weight) System.out.println("Fucked : " + id1 + " " + id2 + " " + weight + "  " + dist) ;*/
		}
	}
	
	public void readLabels () throws Exception {
		if (Flags.DIRECTED)
			readLabelsDirected();
		else
			readLabelsUndirected();
		System.out.println("Done reading labels");
	}
	
	private void readLabelsUndirected () throws Exception {
		String filename = Parameters.ROOT_DATA + Parameters.CITY + "/labelsUndirected.txt";
		//String filename = Parameters.ROOT_OUTPUTS + size;
		
		System.out.println(filename);
		BufferedReader bf1 = new BufferedReader(new FileReader(filename));
		
		//BufferedReader bf1 = new BufferedReader(new FileReader(Parameters.ROOT_DATA + Parameters.CITY + "/labelsUndirectedNextHop.txt"));
		
		String line = "";
		while (true) {
			line = bf1.readLine();
			if (line == null)
				break;
			String[] splits = line.split(" ");
			int id1 = Integer.parseInt(splits[0]);
			int count = Integer.parseInt(splits[1]);
			nodes.get(id1).label.put(id1, 0);
			for (int i = 0; i < count; i++) {
				line = bf1.readLine();
				String[] splits1 = line.split(" ");
				int id2 = Integer.parseInt(splits1[0]);
				int weight = Integer.parseInt(splits1[1]);
				if (Flags.READ_NEXTHOP) {
					int nextHop = Integer.parseInt(splits1[2]);
					if (nodes.get(id1).nextHop == null) {
						nodes.get(id1).nextHop = new HashMap<>();
					}
					nodes.get(id1).nextHop.put(id2, nextHop);
				}
				nodes.get(id1).label.put(id2, weight);
				
				totalPivotCount++;
			}
		}
		System.out.println("Total pivot count : " + totalPivotCount);
	}
	
	@Deprecated
	private void readLabelsDirected () throws Exception {
		BufferedReader bf1 = new BufferedReader(new FileReader(Parameters.ROOT_DATA + Parameters.CITY + "/labelsDirected.txt"));
		String line = "";
		int id = -1;
		while (true) {
			
			line = bf1.readLine();
			if (line == null)
				break;
			id = Integer.parseInt(line);
			line = bf1.readLine();
			
			// in labels 
			while (true) {
				line = bf1.readLine();
				if (line.contains("#"))
					break;
				String[] splits = line.split(" ");
				int id1 = Integer.parseInt(splits[0]);
				int dist = Integer.parseInt(splits[1]);
				nodes.get(id).labelIn.put(id1, dist);
			}
			//reverse in labels
			while (true) {
				line = bf1.readLine();
				if (line.contains("#"))
					break;
				int id1 = Integer.parseInt(line);
				nodes.get(id).reverseLabelIn.add(id1);
			}
			// out labels 
			while (true) {
				line = bf1.readLine();
				if (line.contains("#"))
					break;
				String[] splits = line.split(" ");
				int id1 = Integer.parseInt(splits[0]);
				int dist = Integer.parseInt(splits[1]);
				nodes.get(id).labelOut.put(id1, dist);
			}
			//reverse out labels
			while (true) {
				line = bf1.readLine();
				if (line.contains("*"))
					break;
				int id1 = Integer.parseInt(line);
				nodes.get(id).reverseLabelOut.add(id1);
			}
		}
	}
	
	public void readKeywords () throws Exception {
		String filename = Parameters.ROOT_DATA + Parameters.CITY + "/nodesWithKeywordsSerial" + Parameters.VERSION + ".txt";
		//String filename = Parameters.ROOT_OUTPUTS + size + "/nodesWithKeywordsSerial.txt";

		System.out.println(filename);
		BufferedReader bf1 = new BufferedReader(new FileReader(filename));
		
		//BufferedReader bf1 = new BufferedReader(new FileReader(Parameters.ROOT_DATA + Parameters.CITY + "/nodesWithKeywordsSerial" + Parameters.VERSION + ".txt"));
		String line = "";
		int count = 0;
		int keywordCount = 0;
		while (true) {
			line = bf1.readLine();
			if (line == null)
				break;
			count++;
			StringTokenizer stt = new StringTokenizer(line, " ");
			int id = Integer.parseInt(stt.nextToken().toString());
			if (nodes.get(id).keywords == null) {
				nodes.get(id).keywords = new ArrayList<>();
			}
			while (stt.hasMoreTokens()) {
				String token = stt.nextToken().toString();
				nodes.get(id).keywords.add(token);
				if (!keywords.containsKey(token)) {
					keywords.put(token, new Keyword());
				}
				keywordCount++;
				keywords.get(token).keynodes.add(id);
			}
		}
		allKeywords.addAll(keywords.keySet());
		Collections.sort(allKeywords, new FrequencyKeywordSorting(keywords));
		//for (String key : allKeywords)
		//	System.out.println(key + " " + keywords.get(key).keynodes.size());
		System.out.println("Done reading keywords " + count + " " + keywordCount + " " + allKeywords.size());
	}

	public void readLandmarks () throws Exception {
		readLandmarkDistanceIndex();
		readKeywordToLandmark();
		System.out.println("Done reading landmarks " + metis );
	}
	
	public void readKeywordToLandmark() throws Exception {
		String filename = Parameters.ROOT_DATA + Parameters.CITY + "/metis" + metis + "/landmarkIndex" + Parameters.VERSION  + ".txt";
		//String filename = Parameters.ROOT_OUTPUTS + size + "/metis" + metis + "/landmarkIndex.txt";
		BufferedReader bf = new BufferedReader(new FileReader(filename)); 
		System.out.println("Read : " + filename);
		String line = "";
		while (true) {
			line = bf.readLine();
			if (line == null)
				break;
			StringTokenizer stt = new StringTokenizer(line, " ");
			int clusterId = Integer.parseInt(stt.nextToken().toString());
			ArrayList<Integer> ids = new ArrayList<Integer>();
			
			landmarks.put(clusterId, new Landmark());
			
			while (stt.hasMoreTokens()) {
				int id = Integer.parseInt(stt.nextToken().toString());
				ids.add(id);
				nodes.get(id).landmarkAssignment = clusterId;
				ArrayList<String> keywords = nodes.get(id).keywords;
				landmarks.get(clusterId).uniqueKeywords.addAll(keywords);
			}
			landmarks.get(clusterId).keynodes.addAll(ids);
		}
	}
	
	public void readLandmarkDistanceIndex() throws Exception {
		String filename = Parameters.ROOT_DATA + Parameters.CITY + "/metis" + metis + "/landmarkDistanceIndex" + Parameters.VERSION  + ".txt";

		//String filename = Parameters.ROOT_OUTPUTS + size + "/metis" + metis + "/landmarkDistanceIndex.txt";
		BufferedReader bf = new BufferedReader(new FileReader(filename)); 
		System.out.println("Read : " + filename);
		
		String line = bf.readLine();
		int id = Integer.parseInt(line.split(" ")[0]);
		int clusterId = 0;
		while (true) {
			line = bf.readLine();
			if (line == null)
				break;
			if (line.contains("***")) {
				line = bf.readLine();
				if (line == null)
					break;
				id = Integer.parseInt(line.split(" ")[0]);
				if (id % 1000 == 0) System.out.println(id);
				clusterId = 0;
			}
			else {
				nodes.get(id).distanceToLandmarks.add(Integer.parseInt(line));
			}
		}
	}
	
	@Deprecated
	public void buildKeywordIndex() throws Exception {
		PrintWriter pow = new PrintWriter(new FileWriter(Parameters.ROOT_DATA + Parameters.CITY + "/keywordIndex.txt"));
		for (int i = 0; i < n; i++) {
			pow.println(nodes.get(i).id);
			for (String keyword : allKeywords) {
				ArrayList<Integer> keynodes = keywords.get(keyword).keynodes;
				for (int index : keynodes) {
					int dist = getDistanceUndirected(nodes.get(i).id, nodes.get(index).id);
					pow.println(nodes.get(index).id + " " + dist);
				}
			}
			pow.println("****");
		}
		pow.close();
	}
	
	@Deprecated
	public void generateNextHop() throws IOException {
		
		PrintWriter pow = new PrintWriter(new FileWriter(Parameters.ROOT_DATA + Parameters.CITY + "/labelsUndirectedNextHop.txt"));
		for (int i = 0; i < n; i++) {
			System.out.println(i);
			Iterator it = nodes.get(i).label.entrySet().iterator();
			pow.println(i + " " + nodes.get(i).label.size());
			while (it.hasNext()) {
				Map.Entry<Integer, Integer> pair = (Entry<Integer, Integer>) it.next();
				int nextHop = nextHop(i, pair.getKey() , pair.getValue());
				pow.println(pair.getKey() + " " + pair.getValue() + " " + nextHop);		
			}
		}
		pow.close();
		//System.out.println("Diff : " + (totalDiff/totalCount));
	}
	
	@Deprecated
	public int nextHop (int id, int pivot, int distFromIdToPivot) {
		if (distFromIdToPivot == 0)
			return id;
		int ans = -1;
		int minDist = Integer.MAX_VALUE;
		for (int i = 0; i < edges.get(id).size(); i++) {
			int weight = edges.get(id).get(i).weight;
			int neg = edges.get(id).get(i).neighbour;
			int dist = getDistanceUndirected(neg, pivot);
			if (weight + dist < minDist) {
				minDist = weight + dist;
				ans = neg;
			}
		}
		return ans;
	}
	
	public HashMap<String, Pair> LBgetDistancesThroughKeywords (int source, int destination,
			HashSet<String> keywords) {
		HashMap<String, Pair> ans = new HashMap<String, Pair>();
		Iterator it = landmarks.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<Integer, Landmark> pair = (Entry<Integer, Landmark>) it.next();
			int landmark = pair.getKey();
			for (String keyword : keywords) {
				if (pair.getValue().uniqueKeywords.contains(keyword)) {
					int dist1 = nodes.get(source).distanceToLandmarks.get(landmark);
					if (dist1 == Integer.MAX_VALUE) {
						continue;
					}
					int dist2 = nodes.get(destination).distanceToLandmarks.get(landmark);
					if (dist2 == Integer.MAX_VALUE) {
						continue;
					}
					if (!ans.containsKey(keyword)) {
						Pair p = new Pair();
						p.first = dist1 + dist2;
						ans.put(keyword, p);
					}
					else if (ans.get(keyword).first > (dist1 + dist2)) {
						ans.get(keyword).first = (dist1 + dist2);
					}
				}
			}
		}
		if (Flags.SHORTEST_LANDMARK_DISTANCE_THROUGH_KEYWORD_STORAGE) {
			it = ans.entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry<String, Pair> pair = (Entry<String, Pair>) it.next();
				nodes.get(source).landmarkDistanceToDestThroughKeyword.put(pair.getKey(), pair.getValue().first);
			}
		}
		
		return ans;
	}
	
	public Pair LBgetDistanceThroughKeyword (int source, int destination,
			String keyword) {
		Pair ans = new Pair();
		ans.first = Integer.MAX_VALUE;
		ArrayList<Integer> keynodes = keywords.get(keyword).keynodes;
		for (int keynode : keynodes){
			int landmark = nodes.get(keynode).landmarkAssignment;
			int dist1 = nodes.get(source).distanceToLandmarks.get(landmark);
			if (dist1 == Integer.MAX_VALUE) {
				continue;
			}
			int dist2 = nodes.get(destination).distanceToLandmarks.get(landmark);
			if (dist2 == Integer.MAX_VALUE) {
				continue;
			}
			if (ans.first > dist1 + dist2) {
				ans.first = dist1 + dist2;
				//ans.second = keynode;
				//ans.third = landmark;
			}
		}
		if (Flags.SHORTEST_LANDMARK_DISTANCE_THROUGH_KEYWORD_STORAGE) {
			if (ans.first != Integer.MAX_VALUE)
				nodes.get(source).landmarkDistanceToDestThroughKeyword.put(keyword, ans.first);
			else 
				nodes.get(source).landmarkDistanceToDestThroughKeyword.put(keyword, -1);
		}
		return ans;
	}
	
	public void readInter (ArrayList<ArrayList<Integer>> distances) throws Exception {
		int i = 0;
		int index = 0;
		int test = 0;
		//PrintWriter pow = new PrintWriter (new FileWriter (Parameters.ROOT_DATA + Parameters.CITY + "/inter.txt"));
		while (index < 6) {
			BufferedReader bf = new BufferedReader(new FileReader(Parameters.ROOT_DATA + Parameters.CITY + "/inter" + index + ".txt"));
                	while (true) {
                        	String line = bf.readLine();
				if (line == null) break;
				int count = 0;
				ArrayList<Integer> dists = new ArrayList<>();
				while (true) {
					line = bf.readLine();
					if (line == null || line.equals("****")) break;
					int dist = Integer.parseInt(line);
					dists.add(dist);
					count++;
				}
				int dist = getDistance (i, 209405);
				dists.add(dist);
				count++;
				//System.out.println(count);
				if (count != 87346) break;
				/*for (int d : dists) {
					pow.println(d);
				}
				pow.println("****");*/
				test++;
				System.out.println("Read : " + i++);
				distances.add(dists);
			}
			index++;
		}
		//pow.close();
		System.out.println("Test count " + test);
	}

	@Deprecated
	public void landmarkIndexHandler() throws Exception {
		
		ArrayList<ArrayList<Integer>> distances = new ArrayList<ArrayList<Integer>>();
		HashSet<Integer> keys = new HashSet<Integer>();
		HashMap<Integer, Integer> indices = new HashMap<>();
		int count = 0;
		for (Node node : nodes) {
			if (node.keywords != null) {
				keys.add(node.id);
				indices.put(node.id, count++);
			}
		}

		//readInter(distances);
			
		
		/*for (int i = size/10; i <=size/10; i=i+200) */
		{
			String destination = Parameters.ROOT_DATA /*+ size Parameters.ROOT_DATA*/ + Parameters.CITY + "/metis" + metis;
			count = 0;
			HashMap<Integer, ArrayList<Integer>> assignment = new HashMap<Integer, ArrayList<Integer>>();
			HashMap<Integer, Integer> mapping = new HashMap<>();
			int serialId = 0;
			int test = 0;
			BufferedReader bf = new BufferedReader(new FileReader(destination + "/assignment.txt"));

			while (true) {
				String line = bf.readLine();
				if (line == null) break;
				if (nodes.get(count).keywords != null /*&& reducedKeywords.contains(count)*/) {
					test++;
					int clusterId = Integer.parseInt(line);
					if (!mapping.containsKey(clusterId)) mapping.put(clusterId, serialId++);
					if (!assignment.containsKey(mapping.get(clusterId))) assignment.put(mapping.get(clusterId), new ArrayList<Integer>());
					assignment.get(mapping.get(clusterId)).add(count);
				}
				count++;
			}
			System.out.println("Done : " + test);
			PrintWriter pow = new PrintWriter(new FileWriter(destination + "/landmarkIndex.txt"));
			for (int j = 0; j < serialId; j++) {
				ArrayList<Integer> ids = assignment.get(j);
				pow.print(j + " ");
				for (int id : ids)
					pow.print(id + " ");
				pow.println();
			}
			pow.close();
		buildLandmarkIndex(assignment, serialId-1, indices, destination);
			
			System.out.println("Done : metis ");
		}
	}
	
	@Deprecated
	public void buildLandmarkIndex(HashMap<Integer, ArrayList<Integer>> reverseVisited, int maxClusterId, 
		 HashMap<Integer, Integer> indices, String destination) throws Exception {
		System.out.println("Destination : " + destination);
                ArrayList<Integer> oldKeynodes = new ArrayList<Integer>();
                for (int i = 0 ;i < n; i++) {
                        if (nodes.get(i).keywords != null) {
                                oldKeynodes.add(i);
                        }
                }
		
		size = oldKeynodes.size();
		PrintWriter pow = new PrintWriter(new FileWriter(destination + "/landmarkDistanceIndex.txt"));
		//int index = 1;
		BufferedReader bf = new BufferedReader (new FileReader(Parameters.ROOT_DATA + Parameters.CITY + "/inter" /*+ index*/ + ".txt"));
		for (int i = 0; i < n; i++) {
			boolean flag = true;
			pow = new PrintWriter(new FileWriter(destination + "/landmarkDistanceIndex.txt", true));
			int node_id = 0;
			ArrayList<Integer> distances = new ArrayList<Integer>();
			while (true) {
				String line = bf.readLine();
				if (line == null) {
					/*index++;
					bf = new BufferedReader (new FileReader(Parameters.ROOT_DATA + Parameters.CITY + "/inter_" + index + ".txt"));
					continue;*/
					break;
				}
				node_id = Integer.parseInt(line);
				for (int j = 0; j < size; j++) {
					//if (reducedKeywords.contains(oldKeynodes.get(i))) 
					distances.add(Integer.parseInt(bf.readLine()));
				}
				line = bf.readLine();
				break;
			}
			pow.println(node_id + " " + distances.size());
			if (node_id % 1000 == 0)
				System.out.println(node_id + " " + distances.size());
			for (int j = 0; j <= maxClusterId; j++) {
				ArrayList<Integer> ids = reverseVisited.get(j);
				int min = Integer.MAX_VALUE;
				for (int id : ids) {
					//if (!reducedKeywords.contains(id)) continue;
					int dist = distances.get(indices.get(id));
					if (dist < min && dist < Integer.MAX_VALUE) {
						min = dist;
					}
				}
				pow.println(min);
			}
			pow.println("****");
			pow.close();
		}
		pow.close();

		System.out.println(/*"Index : " + index +*/ " Last line : " + bf.readLine());
	}
	
	
	public ArrayList<Pair> getDistanceThroughKeyword (int source, int destination, String keyword) 
			throws Exception {
		ArrayList<Pair> ans = new ArrayList<Pair>();
		
		ArrayList<Pair> sortedFromSource;
		ArrayList<Pair> sortedFromDest;
		
		if (Flags.FIND_RANKING_OF_KEYNODE) {
			sortedFromDest = new ArrayList<>(); sortedFromSource = new ArrayList<>();
		}
		
		ArrayList<Integer> keywordIds = keywords.get(keyword).keynodes;
		
		int minimumDistance = Integer.MAX_VALUE;
		int minimumId = -1;
		
		
		for (int i = 0; i < keywordIds.size(); i++) {
			int dist1 = getDistance(source, keywordIds.get(i));
			if (dist1 == Integer.MAX_VALUE) {
				continue;
			}
			int dist2 = getDistance(keywordIds.get(i), destination);
			if (dist2 == Integer.MAX_VALUE) {
				continue;
			}
			Pair p = new Pair();
			p.first = dist1;
			p.second = keywordIds.get(i);
			p.third = dist2;
			ans.add(p);
			
			if (Flags.FIND_RANKING_OF_KEYNODE) {
				sortedFromSource.add(p); sortedFromDest.add(p);
			}
			
			if (dist1 + dist2 < minimumDistance) {
				minimumDistance = dist1 + dist2;
				minimumId = keywordIds.get(i);
			}
		}
		
		if (Flags.FIND_RANKING_OF_KEYNODE) {
			storeRanking(sortedFromSource, sortedFromDest, minimumId, ans, keyword);
		}

		if (Flags.SHORTEST_DISTANCE_THROUGH_KEYWORD_STORAGE) {
			if (minimumId == -1) {
				nodes.get(source).distanceToDestThroughKeyword.put(keyword, -1);
			}
			else {
				nodes.get(source).distanceToDestThroughKeyword.put(keyword, minimumDistance);
			}
		}
		
		Pair p = new Pair();
		p.first = minimumDistance;
		p.second = minimumId;
		p.third = 0;
		ans.add(p);
		return ans;
	}
	
	@Deprecated
	public void storeRanking (ArrayList<Pair> sortedFromSource, ArrayList<Pair> sortedFromDest,
		int minimumId, ArrayList<Pair> ans, String keyword) throws Exception {
		PrintWriter outRanking = new PrintWriter(new FileWriter(Parameters.ROOT_OUTPUTS + "ranking.csv"));
		Collections.sort(sortedFromSource, new PairSortingUsingFirst());
		Collections.sort(sortedFromDest, new PairSortingUsingThird());
		
		int sourceRank = 0;
		int destRank = 0;
		
		for (int i = 0; i < sortedFromSource.size(); i++) {
			if (sortedFromSource.get(i).second == minimumId) {
				sourceRank = i+1;
				break;
			}
		}
		
		for (int i = 0; i < sortedFromDest.size(); i++) {
			if (sortedFromDest.get(i).second == minimumId) {
				destRank = i+1;
				break;
			}
		}
		outRanking.println(keyword + "," + minimumId + "," + ans.size() + "," + sourceRank + "," + destRank);
	
	}
	
	public int getDistance (int source, int destination) {
		if (Flags.DIRECTED)
			return getDistanceDirected(source, destination);
		else
			return getDistanceUndirected(source, destination);
	}
	
	
	private int getDistanceUndirected (int source, int destination) {
		int ans = Integer.MAX_VALUE;
		Iterator it = nodes.get(source).label.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<Integer, Integer> pair = (Entry<Integer, Integer>) it.next();
			if (nodes.get(destination).label.containsKey(pair.getKey())) {
				int dist = nodes.get(destination).label.get(pair.getKey()) + pair.getValue();
				if (dist < ans)
					ans = dist;
			}
		}
		return ans;
	}
	
	@Deprecated
	public int getDistanceDirected (int source, int destination) {
		int ans = Integer.MAX_VALUE;
		Iterator it = nodes.get(source).labelOut.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<Integer, Integer> pair = (Entry<Integer, Integer>) it.next();
			if (nodes.get(destination).labelIn.containsKey(pair.getKey())) {
				int dist = nodes.get(destination).labelIn.get(pair.getKey()) + pair.getValue();
				if (dist < ans)
					ans = dist;
			}
		}
		if (ans == Integer.MAX_VALUE) {
			Iterator reverseiterator = nodes.get(source).reverseLabelIn.iterator();
			while (reverseiterator.hasNext()) {
				int id = (int) reverseiterator.next();
				if (nodes.get(id).labelOut.containsKey(destination)) {
					int dist = nodes.get(id).labelIn.get(source) + nodes.get(id).labelOut.get(destination);
					if (dist < ans)
						ans = dist;
				}
			}
		}
		return ans;
	}
}

