times=dlmread('txt/epsilonVsQuality.txt');

x1=times(:,1);
by=times(:,2);
cy=times(:,3);

plot(x1,by,'-s',x1,cy,'-o','LineWidth',3.5,'MarkerSize',24);
%plot(x1,ey,'-s',x1,fy,'->',x1,gy,'-d','LineWidth',3.5,'MarkerSize',24);

ylabel('Average loss in accuracy (%)','FontSize',40);
xlabel('\epsilon','FontSize',40);

h_legend=legend('London','Sydney');
%set(h_legend,'FontSize',30);
set(gca,'yLim',[0,100]);
%set(h_legend,'FontSize',30);
%set(h_legend,'location','northoutside');
%set(gca,'xLim',[0,8]);
%set(gca,'yScale','log');
set(gca,'FontSize',30)
%set (gca,'YTickLabel',['1000';'10000';'100000';'1000000';'10000000';'100000000'])
