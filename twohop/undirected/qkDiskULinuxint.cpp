#include "ioULinuxint.h"
#include <vector>
#include <map>
#include <cstdlib>

using namespace std;

const int kmax = 1024;

int queryCnt;

char * txtName, * degName, * outName, * nodeKeyName, * keyNodeName, * lowName, *highName, * lowPosTName, * lowPosVName, * highPosName, * testName;
long long * pos, ktCnt;

int n, tHL, fHL, kn, km;
long long m, mm;

FILE * flowPosV;
BTType * lowPosT;
long long lowPosTLen = 0;
long long * highPos;

inBufL lxPos;
inBufS lx, low;
inBuf high;

int * highCnt;
wtype * highLen;
const int P = 10007;
map<int,int> mp;

wtype * ansDis;
map<int,int>::iterator * ansLink;
int * ansV;
int ansCnt;
BCType tmpBC[BCSize];
int tmpBCLen, tmpBCCur;

vector<int> keyFreq;

void loadIndex()
{
	n = checkB(degName)/sizeof(edgeL);
	tHL = n/fHL;
	highCnt = (int*)malloc(sizeof(int) * tHL);
	highLen = (wtype*)malloc(sizeof(wtype) * tHL);	
	
	ansV = (int*)malloc(sizeof(int) * (kmax+1));
	ansLink = (map<int,int>::iterator*)malloc(sizeof(map<int,int>::iterator) * (kmax+1));
	ansDis = (wtype*)malloc(sizeof(wtype) * (kmax+1));
	
	
	
	lx.perBuf = BSize/sizeof(edgeS);
	lx.init(outName);
	lxPos.init(degName);
	
	high.perBuf = BSize/sizeof(edge);
	high.init(highName);

	low.perBuf = BSize/sizeof(edgeS);
	low.init(lowName);

	flowPosV = fopen(lowPosVName, "rb");

	lowPosTLen = checkB(lowPosTName)/sizeof(BTType);
	lowPosT = (BTType *)malloc(lowPosTLen * sizeof(BTType));
	FILE * ftmp = fopen(lowPosTName, "rb");
	fread(lowPosT, sizeof(BTType), lowPosTLen, ftmp);
	fclose(ftmp);
	
	kn = checkB(highPosName)/sizeof(long long) - 1;
	highPos = (long long *)malloc(sizeof(long long)*(kn+1LL));
	ftmp = fopen(highPosName, "rb");	
	fread(highPos, sizeof(long long), kn+1, ftmp);
	fclose(ftmp);
	
	
	FILE * fkeyNode = fopen(keyNodeName, "r");
	fscanf(fkeyNode, "%d%d", &kn, &km);
	keyFreq.resize(kn);
	for (int i = 0, x, y; i < kn; i++)
	{
		fscanf(fkeyNode, "%d%d", &x, &y);
		keyFreq[x] = y;
		for (int j = 0; j < y; j++)
			fscanf(fkeyNode, "%d", &x);
	}
}

edgeL tmpL;
edgeS tmpS, tmpS1;
edge tmp;

void ansDown(int x)
{
//	printf("%d : %d %d %d %d %d\n", x, ansCnt, ansDis[1], ansDis[2],  ansDis[3], ansDis[4]);
	ansLink[0] = ansLink[x];
	ansDis[0] = ansDis[x];
	for (int j; (j = (x<<1)) <= ansCnt; x = j)	
	{
		if (j < ansCnt && ansDis[j+1] > ansDis[j]) j++;
		if (ansDis[j] > ansDis[0])
		{
			ansDis[x] = ansDis[j];
			ansLink[x] = ansLink[j];
			ansLink[x]->second = x;
		}
		else break;
	}
	ansDis[x] = ansDis[0];
	ansLink[x] = ansLink[0];
	ansLink[x]->second = x;
//	printf("%d : %d %d %d %d %d\n", x, ansCnt, ansDis[1], ansDis[2],  ansDis[3], ansDis[4]);
	
}

void ansUp(int x)
{
//	printf("%d : %d %d %d %d %d\n", x, ansCnt, ansDis[1], ansDis[2],  ansDis[3], ansDis[4]);
	ansLink[0] = ansLink[x];
	ansDis[0] = ansDis[x];
	for (int j; (j = (x>>1)) > 0; x = j)	
	{
		if (ansDis[j] < ansDis[0])
		{
			ansDis[x] = ansDis[j];
			ansLink[x] = ansLink[j];
			ansLink[x]->second = x;
		}
		else break;
	}
	ansDis[x] = ansDis[0];
	ansLink[x] = ansLink[0];
	ansLink[x]->second = x;
//	printf("%d : %d %d %d %d %d\n", x, ansCnt, ansDis[1], ansDis[2],  ansDis[3], ansDis[4]);
	
}

int x1Cnt = 0, x2Cnt = 0, highCnt_ = 0, lowCnt = 0, lowPosCnt = 0;

double t1 = 0, t2 = 0, t3 = 0, t4 = 0, t5 = 0, t6 = 0, t11 = 0, t12 = 0, t13 = 0, t31 = 0, t41 = 0, t14 =0, t15 = 0;
double tf0 = 0, tf1 = 0, tf2 = 0, tf3 = 0, tf4 = 0, tf5 = 0;

void query(int x, int w, int k)
{

timer tf;


timer tm;
tf.restart();
	lxPos.fseek(x);
tf1 += tf.getTime();

t14 += tm.getTime();
	lxPos.nextOne(tmpL);
t15 += tm.getTime();
	
	int nx = ((tmpL.x<<32)>>32);
tf.restart();
	lx.fseek(tmpL.y);
tf2 += tf.getTime();
t11 += tm.getTime();
	lx.start();
t12 += tm.getTime();	
	lx.nextEdge(tmpS);
t13 += tm.getTime();	
t1 += tm.getTime();
	fill(highCnt, highCnt + tHL, -k-1);

tm.restart();	

	while (tmpS.x > -1 && tmpS.x < tHL)
	{	
//printf("%d %d %d\n", x, tmpS.x, tmpS.w);

x1Cnt++;
		highCnt[tmpS.x] = k+1;
		highLen[tmpS.x] = tmpS.w;
		lx.nextEdge(tmpS);		
	}
t2 += tm.getTime();	
tm.restart();	

	if (nx < tHL) 
	{
		highCnt[nx] = k+1;
		highLen[nx] = 0;
	}
		
	mp.clear();
tf.restart();
	high.fseek(highPos[w]);
tf3 += tf.getTime();
t31 += tm.getTime();

	high.start();
	int hv;
	ansCnt = 0;
	long long hh = highPos[w+1] - highPos[w];

	while (hh--)
	{

highCnt_++;

		high.nextEdge(tmp);
//printf("%d %d %d %d ", tmp.y, tmp.w, ansCnt, ansDis[1]); if (ansCnt > 0) printf("%d ", ansLink[1]->first); printf("\n");
//if (tmp.x == 716) printf("716 %d %d %d %d ", tmp.y, tmp.w, ansCnt, ansDis[1]);  if (ansCnt > 0) printf("%d ", ansLink[1]->first); printf("\n");
		
		if (highCnt[tmp.x] > 0)
		{
			
			
			highCnt[tmp.x]--;
			if (highCnt[tmp.x] == 0) break;
			if (ansCnt == k && tmp.w + highLen[tmp.x] >= ansDis[1]) continue;


			if (mp.find(tmp.y) != mp.end())
			{
				if (ansDis[hv = mp[tmp.y]] <= tmp.w + highLen[tmp.x]) continue;
				ansDis[hv] = tmp.w + highLen[tmp.x];	
				if (ansCnt == k) ansDown(hv);
			}
			else
			{
				if (ansCnt < k)
				{
					ansDis[++ansCnt] = tmp.w + highLen[tmp.x];
					mp[tmp.y] = ansCnt;
					ansLink[ansCnt] = mp.find(tmp.y);
					if (ansCnt == k)
					{
						for (int i = (k>>1); i > 0; i--)
							ansDown(i);
					}
				}
				else 
				{
					mp.erase(ansLink[1]);
					mp[tmp.y] = 1;
					ansLink[1] = mp.find(tmp.y);
					ansDis[1] = tmp.w + highLen[tmp.x];
					ansDown(1);
				}
			}
			
		}
		else 
		{
			highCnt[tmp.x]++;
			if (highCnt[tmp.x] == 0) break;		
		}
//if (tmp.x == 716) printf("716 %d %d %d %d\n", tmp.y, tmp.w, ansCnt, ansDis[1]);
		
	}
	

	if (ansCnt < k)
	{
		for (int i = (ansCnt>>1); i > 0; i--)
			ansDown(i);
	}
	

t3 += tm.getTime();
tm.restart();

	tmpBCCur = tmpBCLen = 0;
	while (1)
	{
x2Cnt++;

		if (tmpS.x == -1)
		{
			if (nx < tHL) break;
			tmpS.x = nx;
			tmpS.w = 0;
		}
		


		while (tmpBCCur < tmpBCLen && tmpBC[tmpBCCur].v < tmpS.x)
			tmpBCCur++;
		if (tmpBCCur == tmpBCLen || tmpBC[tmpBCCur].v > tmpS.x)
		{
			long long xt = 0, yt = lowPosTLen-1, zt;
			int anst = -1, answ;
			while (xt <= yt)
			{
				zt = ((xt+yt)>>1);
				if (lowPosT[zt].w < w || lowPosT[zt].w == w && lowPosT[zt].v <= tmpS.x)
				{
					anst = zt;
					xt = zt+1;		
				}	
				else yt = zt-1;
			}

		
			if (anst == -1) 
			{
				if (tmpS.x == nx) break;
				lx.nextEdge(tmpS);			
				continue;
			}
			
tf.restart();			
			fseeko64(flowPosV, sizeof(BCType) * lowPosT[anst].pos, SEEK_SET);
tf4 += tf.getTime();
			tmpBCLen = fread(tmpBC, sizeof(BCType), BCSize, flowPosV);
			tmpBCCur = 0;
			
			
lowPosCnt++;
			
			answ = lowPosT[anst].w;
			while (tmpBCCur < tmpBCLen && (answ < w || answ == w && tmpBC[tmpBCCur].v < tmpS.x))
				answ += (tmpBC[tmpBCCur++].v > n);
			if (tmpBCCur == tmpBCLen || answ != w || tmpBC[tmpBCCur].v != tmpS.x)
			{
				if (tmpS.x == nx) break;
				lx.nextEdge(tmpS);			
				continue;
			}
				
		}


tf.restart();
		low.fseek(tmpBC[tmpBCCur].pos);
tf5 += tf.getTime();
		low.start();
		low.nextEdge(tmpS1);

lowCnt++;

timer tmm;
		for (int i = 0; i < k && tmpS1.x > -1; i++)
		{
lowCnt++;

//			if (tmpS.x == 38272) printf("%d %d %d %d\n", i, tmpS.w, tmpS1.w, tmpS1.x);
			
			if (ansCnt > 0 && tmpS1.w + tmpS.w >= ansDis[1]) 
			{
				low.nextEdge(tmpS1);
				continue;
			}	

			if (mp.find(tmpS1.x) != mp.end())
			{
				if (ansDis[hv = mp[tmpS1.x]] <= tmpS.w + tmpS1.w)
				{
					low.nextEdge(tmpS1);
					continue;
				}
				ansDis[hv] = tmpS.w + tmpS1.w;	
				ansDown(hv);
			}
			else
			{
				if (ansCnt < k)
				{
					ansDis[++ansCnt] = tmpS1.w + tmpS.w;
					mp[tmpS1.x] = ansCnt;
					ansLink[ansCnt] = mp.find(tmpS1.x);
					ansUp(ansCnt);
				}
				else
				{
					mp.erase(ansLink[1]);
					mp[tmpS1.x] = 1;
					ansLink[1] = mp.find(tmpS1.x);
					ansDis[1] = tmpS.w + tmpS1.w;
					ansDown(1);

				}
			}			
			
			low.nextEdge(tmpS1);
			
		}
t5 += tmm.getTime();
		if (tmpS.x == nx) break;
		lx.nextEdge(tmpS);		

		
	}
	
t4 += tm.getTime();

	for (int i = 1; i <= ansCnt; i++)
	{
		ansV[i] = ansLink[i]->first;
		mp.erase(ansLink[i]);
	}
	
//	printf("%d %d\n", ansV[1], ansDis[1]);


}




//int queryDis(int x, int y)
//{
//	if (x == y) return 0;
//	int xx = x, yy = y;

//	x = ((deg[xx].x<<32)>>32);
//	y = ((deg[yy].x<<32)>>32);
//	
//	if (x > y)
//	{
//		labelx = labelout + deg[xx].w;
//		labely = labelout + deg[yy].y;
//	}
//	else
//	{
//		int xy = x; x = y; y = xy;
//		labelx = labelout + deg[yy].y;
//		labely = labelout + deg[xx].w;
//	}
//	
//	int ans = 10000, i = 0, j = 0;
//	
//	
//	if (labelx[i].x != -1 && labely[j].x != -1)
//	while (labelx[i].x < y)
//	{

//		if (labelx[i].x == labely[j].x) 
//		{
//			ans = ans>(labelx[i].w + labely[j].w)?(labelx[i].w + labely[j].w):ans;
//			if (labelx[++i].x == -1) break;
//			if (labely[++j].x == -1) break;
//		}
//		else if (labelx[i].x < labely[j].x)
//		{
//			if (labelx[++i].x == -1) break;
//		}
//		else if (labely[++j].x == -1) break;
//	}
//	
//	while (labelx[i].x != -1 && labelx[i].x < y) i++;
//	if (labelx[i].x == y) ans = ans>labelx[i].w?labelx[i].w:ans;
//	
//		
//	return ans;
//}



vector<vector<pair<int, wtype> > > g;
vector<vector<int> > keyNode;

void testLoad()
{
	FILE * fg = fopen(txtName, "r");
	fscanf(fg, "%d", &n);
	g.resize(n);
	for (int i = 0; i < n; i++)
		g[i].resize(0);
	wtype z;
	for (int x, y; fscanf(fg, "%d%d%d", &x, &y, &z)!=EOF;)
	{
		g[x].push_back(make_pair(y,z));
	}
	
	FILE * fkeyNode = fopen(keyNodeName, "r");
	fscanf(fkeyNode, "%d%d", &kn, &km);
	keyNode.resize(kn);
	for (int i = 0, x, y; i < kn; i++)
	{
		fscanf(fkeyNode, "%d%d", &x, &y);
		keyNode[x].resize(y);
		for (int j = 0; j < y; j++)
			fscanf(fkeyNode, "%d", &keyNode[x][j]);
		sort(keyNode[x].begin(), keyNode[x].end());
		int yy = 1;
		for (int j = 1; j < y; j++)
			if (keyNode[x][j] != keyNode[x][j-1])
				keyNode[x][yy++] = keyNode[x][j];
		keyNode[x].resize(yy);
	}
}

vector<wtype> dis;
vector<int> testH, testHpos;
int testCnt;
vector<pair<int, wtype> > testAns;

inline void testDown(int i)
{
//	printf("testDown %d\n", i);
	testH[0] = testH[i];
	for (int j; (j=(i<<1)) <= testCnt; i = j )
	{
//		printf("testDown %d ==\n", j);
		if (j < testCnt && dis[testH[j+1]] < dis[testH[j]] ) j++;
		if (dis[testH[0]] > dis[testH[j]])
		{
			testH[i] = testH[j];
			testHpos[testH[i]] = i;
		}
		else break;	
	}
	testH[i] = testH[0];
	testHpos[testH[i]] = i;
}

inline void testUp(int i)
{
	testH[0] = testH[i];
	for (int j; (j=(i>>1)) >= 1; i = j )
	{
		if (dis[testH[0]] < dis[testH[j]])
		{
			testH[i] = testH[j];
			testHpos[testH[i]] = i;
		}
		else break;	
	}
	testH[i] = testH[0];
	testHpos[testH[i]] = i;
}

vector<int> prep;
vector<bool> vv;


//vector<wtype> ansDis;

void testQuery(int x, int w, int k)
{
	vv.resize(0);
	vv.resize(n, 0);

	for (int i = 0, ii = keyNode[w].size(); i < ii; i++)
		vv[keyNode[w][i]] = 1;
			
	prep.resize(n);
	testAns.resize(0);
	dis.resize(0);
	dis.resize(n, -1);
	testHpos.resize(n);
	testH.resize(n);
	dis[x] = 0;
	testH[1] = x;
	testCnt = 1;
	testHpos[x] = 1;
	int p;
	
	while (testCnt > 0)
	{
		p = testH[1];
		testH[1] = testH[testCnt--];
		testHpos[testH[1]] = 1;
		testDown(1);

		if (vv[p])	testAns.push_back(make_pair(p, dis[p]));
				
		if (testAns.size() == k) break;

		for (int i = 0, ii = g[p].size(); i < ii; i++)
			if (dis[g[p][i].first] < 0 || dis[g[p][i].first] > dis[p] + g[p][i].second)
			{
				prep[g[p][i].first] = p;
				if (dis[g[p][i].first] < 0)
				{
					testH[++testCnt] = g[p][i].first;
					testHpos[g[p][i].first] = testCnt;
				}
				dis[g[p][i].first] = dis[p] + g[p][i].second;
				testUp(testHpos[g[p][i].first]);				
			}
	}
	
	
	sort(ansDis+1, ansDis+1+ansCnt);
	
	if (testAns.size() != ansCnt) 
	{
		FILE * ferr = fopen("ferr", "w");
		fprintf(ferr, "%d ", testAns.size());
		for (int i = 0; i < testAns.size(); i++)
			fprintf(ferr, "%d ", testAns[i].first);
		fprintf(ferr, "\n");
		fprintf(ferr, "%d ", testAns.size());
		for (int i = 0; i < testAns.size(); i++)
			fprintf(ferr, "%d ", testAns[i].second);
		fprintf(ferr, "=====\n");
		fprintf(ferr, "%d ", ansCnt);
		for (int i = 0; i < ansCnt; i++)
			fprintf(ferr, "%d ", ansV[i]);
		fprintf(ferr, "\n");
		fprintf(ferr, "%d ", ansCnt);		
		for (int i = 0; i < ansCnt; i++)
			fprintf(ferr, "%d ", ansDis[i]);
		fprintf(ferr, "\n");
		printf("size error %d %d %d \n", x, w, k);
		exit(0);
	}	
	
	for (int i = 0; i < ansCnt; i++)
		if (ansDis[i+1] != testAns[i].second)
		{
			FILE * ferr = fopen("ferr", "w");
			fprintf(ferr, "%d ", testAns.size());
			for (int i = 0; i < testAns.size(); i++)
				fprintf(ferr, "%d ", testAns[i].first);
			fprintf(ferr, "\n");
			fprintf(ferr, "%d ", testAns.size());
			for (int i = 0; i < testAns.size(); i++)
				fprintf(ferr, "%d ", testAns[i].second);
			fprintf(ferr, "=====\n");
			
			fprintf(ferr, "%d ", ansCnt);
			for (int i = 1; i <= ansCnt; i++)
				fprintf(ferr, "%d ", ansV[i]);
			fprintf(ferr, "\n");
			fprintf(ferr, "%d ", ansCnt);		
			for (int i = 1; i <= ansCnt; i++)
				fprintf(ferr, "%d ", ansDis[i]);
			fprintf(ferr, "\n");
			printf("k error %d %d %d\n", x, w, k);
			exit(0);
		}
	
}


int main(int argc, char ** argv)
{
	txtName = argv[1];

	if (argc > 2) fHL = atoi(argv[2]);
	else fHL = 100;
	
	degName = (char*)malloc(1+strlen(txtName) + 50);
	sprintf(degName, "%s.deg", txtName);	
	outName = (char*)malloc(1+strlen(txtName) + 50);
	sprintf(outName, "%s.out", txtName);
	keyNodeName = (char*) malloc(1+strlen(txtName) + 50);
	sprintf(keyNodeName, "%s.keyNode", txtName);
	nodeKeyName = (char*) malloc(1+strlen(txtName) + 50);
	sprintf(nodeKeyName, "%s.nodeKey", txtName);

	highName = (char*) malloc(1+strlen(txtName) + 50);
	sprintf(highName, "%s.high%d_B", txtName, fHL);
	highPosName = (char*) malloc(1+strlen(txtName) + 50);
	sprintf(highPosName, "%s.highpos%d_B", txtName, fHL);
	lowName = (char*) malloc(1+strlen(txtName) + 50);
	sprintf(lowName, "%s.low%d_B", txtName, fHL);
	lowPosTName = (char*) malloc(1+strlen(txtName) + 50);
	sprintf(lowPosTName, "%s.lowpost%d_B", txtName, fHL);
	lowPosVName = (char*) malloc(1+strlen(txtName) + 50);
	sprintf(lowPosVName, "%s.lowposv%d_B", txtName, fHL);

	timer tm;
	
	loadIndex();
	printf("index loaded");

	queryCnt = 1000;
	int cnt = 0;

	vector<int> qx(queryCnt), qw(queryCnt);
	
		FILE * fkeyNode = fopen(keyNodeName, "r");
		fscanf(fkeyNode, "%d%d", &kn, &km);	
//		return 0;
		int sy = 0;
		vector<int> wf(kn,0);
	
		for (int i = 0, x, y; i < kn; i++)
		{
			fscanf(fkeyNode, "%d%d", &x, &y);
			sy += y;
			wf[x] = y;
			for (int j = 0; j < y; j++)
				fscanf(fkeyNode, "%d", &x);
		}
		
		 
		fclose(fkeyNode);
		
		for (int i = 1; i < kn; i++)
			wf[i] += wf[i-1];
		n = checkB(degName)/sizeof(edgeL);
		
		for (int i = 0; i < queryCnt; i++)
		{
			qx[i] = rand()%n;
			int x = 0, y = kn-1, z = rand()%sy+1, ans = kn;
			while (x <= y)
				if (wf[(x+y)>>1] >= z)
				{
					if (ans > ((x+y)>>1) ) ans = ((x+y)>>1);
					y = ((x+y)>>1)-1;
				}
				else x = ((x+y)>>1)+1;
			qw[i] = ans;
//			while (1)
//			{
//				qw[i] = rand()%kn;
//				if (qw[i] > 0 && wf[qw[i]]-wf[qw[i]-1] > 1000) break;
//			}	
		}

		printf("High-Low querying %s \n", txtName);

		for (int k = 1; k <= 128; k *= 2)
		{
			system("sudo sh -c \"sync; echo 3 > /proc/sys/vm/drop_caches\"");
		
			double sumTime = 0;
			tm.restart();
			for (int i = 0; i < queryCnt; i++)
				query(qx[i], qw[i], k);
			printf("average top-%d query time %lf microsecond\n", k, tm.getTime()*1000000/queryCnt);

		}
		
	
	return 0;
}














