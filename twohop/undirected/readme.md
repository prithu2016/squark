The implementation is based on "Hop Doubling Label Indexing for Point to Point Distance Querying on ScaleFree Networks" paper using "Contraction Hierarchies: Faster and Simpler Hierarchical Routing in Road Networks"


1. Build a 2-hop index by HopDB algorithm with "g++ ioUint.h labelUint.cpp -O3 && ./a.out graph_file_name". Eg, flarn.graph. Follow flarn.graph for input format. Along with the graph, the node ordering in needed which is obtained from CH-ranking. Generate the ranking from Code/src/twohop/contraction-hierarchies-20090221 folder using instructions from readmePranali. Follow the ranking format in flarn.rank. For more instructions on ip/op format, refer readmeIpOp.

Example:
\flarn>g++ ioUint.h labelUint.cpp -O3 && ./a.out flarn.graph
processing flarn.graph
iteration 1 begin
iteration 2 begin
iteration 3 begin
iteration 4 begin
iteration 5 begin
iteration 6 begin
iteration 7 begin
iteration 8 begin
iteration 9 begin
iteration 10 begin
iteration 11 begin
iteration 12 begin
iteration 13 begin
iteration 14 begin
iteration 15 begin
iteration 16 begin
iteration 17 begin
iteration 18 begin
iteration 19 begin
iteration 20 begin
iteration 21 begin
index begin
2-hop index size 2464.257950 MB
2-hop indexing time 9847.507335 (sec)

2. After running the algorithm, 1 index file namely flarn.graph.labels is generated. Refer readmeIpOp for its format. 

3. To query for shortest distance between s and t, use distance(s, t) function like implementation from notNeeded/HopDBundirected.java 