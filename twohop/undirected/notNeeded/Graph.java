package twohop;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.*;
import java.util.Map.Entry;

import data.edge;

class AdsComparator implements Comparator<Graph> {
	public int compare (Graph g1, Graph g2) {
		if (g1.adsDensity < g2.adsDensity)
			return 1;
		if (g1.adsDensity > g2.adsDensity)
			return -1;
		return 0;
	}
}

public class Graph {
	int n;  //no. of nodes
	ArrayList<Node> nodes;
	ArrayList<ArrayList<Integer>> ad; //adjacency matrix
	ArrayList<ArrayList<Double>> weights; //weight matrix
	ArrayList<Graph> centerGraphs = new ArrayList<Graph>();
	double adsDensity;
	boolean touched = false;
	Graph ads;
	int centerId;
	
	public Graph () {
		ad = new ArrayList<>();
		weights = new ArrayList<>();
		adsDensity = -1;
		nodes = new ArrayList<>();
	}
	public Graph (int inputN) {
		n = inputN;
		adsDensity = -1;
		ad = new ArrayList<>();
		weights = new ArrayList<>();
		nodes = new ArrayList<Node>();
		for (int i = 0; i < n ; i++) {
			ad.add(new ArrayList<Integer>());
			weights.add(new ArrayList<Double>());
			nodes.add(new Node(i));
		}
	}
	
	public void readGraph (String file) throws Exception {
		BufferedReader bf = new BufferedReader(new FileReader(file));
		String line = bf.readLine(); //#Nodes and #Edges
		String splits[] = line.split("# ");
		n = Integer.parseInt(splits[1].split(" ")[0]);
		nodes = new ArrayList<Node>();
		
		for (int i = 0; i < n; i++) {
			Node n1 = new Node();
			n1.id =i;
			nodes.add(n1);
			ad.add(new ArrayList<Integer>());
			weights.add(new ArrayList<Double>());
		}
		
		while (true) {
			line = bf.readLine();
			if (line == null)
				break;
			String[] endPoints = line.split(" ");
			int u = Integer.parseInt(endPoints[0]);
			int v = Integer.parseInt(endPoints[1]);
			double weight = Double.parseDouble(endPoints[2]);
			nodes.get(u).degree++;
			ad.get(u).add(v);
			weights.get(u).add(weight);
			
			//nodes.get(v).degree++;
			//ad.get(v).add(u);
			//weights.get(v).add(weight);
		}
	}
	
	public void shortestPathsComputations () throws Exception {
		PrintWriter pow = new PrintWriter(new FileWriter("/Users/chendu/Desktop/DDP/New/Data/dublin/shortestPaths1.txt"));
		for (int j = 0; j < n; j++) {
			PriorityQueue<Node> pq = new PriorityQueue<>(10, new DistanceComparator());
			nodes.get(j).dist = 0;
			nodes.get(j).parent = j;
			pq.add(nodes.get(j));
			while (pq.size() > 0) {
				Node top = pq.poll();
				ArrayList<Integer> nei = ad.get(top.id);
				ArrayList<Double> nei_weights = weights.get(top.id);
				for (int i = 0; i < nei.size(); i++) {
					if (nodes.get(nei.get(i)).dist > top.dist + nei_weights.get(i)) {
						nodes.get(nei.get(i)).dist = top.dist + nei_weights.get(i);
						pq.add(nodes.get(nei.get(i)));
						nodes.get(nei.get(i)).parent = top.id;
					}
				}
			}
			Path p1 = new Path();
			p1.addNode(j);
			nodes.get(j).shortestPaths.put(j, p1);
			int count1 = 0;
			pow.println(j);
			System.out.println(j);
			for (int i = 0; i < n; i++) {
				if (nodes.get(i).dist < Double.MAX_VALUE) {
					pow.println(i + " " + nodes.get(i).dist);
				}
				/*if (i!=j && !nodes.get(j).shortestPaths.containsKey(i) && nodes.get(i).dist != Double.MAX_VALUE) {
					addPaths(j, i);
				}*/
				nodes.get(i).clean();
			}
			//nodes.get(j).clean();
			pow.println("*************");
		}
		pow.close();
	}
	
	public Path addPaths(int source, int root) {
		Path p1 = new Path();
		if (nodes.get(root).parent == -1) {
			return p1;
		}
		if (nodes.get(source).shortestPaths.containsKey(nodes.get(root).parent)) {
			p1 = nodes.get(source).shortestPaths.get(nodes.get(root).parent).shallowCopy();
		}
		else {
			p1 = addPaths(source, nodes.get(root).parent);
		}
		p1.addNode(root);
		nodes.get(source).shortestPaths.put(root, p1);
		nodes.get(root).clean();
		return p1;
	}
	
	public void print() {
		for (int i = 0; i < n; i++) {
			System.out.println(i + " : " + ad.get(i) + " " + nodes.get(i).degree);
		}
		System.out.println("**********");
	}
	
	public void printLabel() {
		for (int i = 0; i < n; i++) {
			System.out.println("In label of " + i);
			System.out.println(nodes.get(i).labelIn);
			System.out.println("Out label of " + i);
			System.out.println(nodes.get(i).labelOut);
			System.out.println("********************");
		}
	}
	
	/*public void print() {
		for (int i = 0; i < n; i++) {
			Iterator it = nodes.get(i).shortestPaths.entrySet().iterator();
			System.out.println("Source : " + i);
			while (it.hasNext()) {
				Map.Entry<Integer, Path> pair = (Map.Entry<Integer, Path>) it.next();
				System.out.println(pair.getKey() + " : " + pair.getValue().seq);
			}
		}
	}*/
	
	public void createCentreGraphs () throws Exception {
		for (int i = 0; i < n ; i++) {
			centerGraphs.add(new Graph(n));
			centerGraphs.get(i).centerId = i;
		}
		for (int i = 0; i < n ; i++) {
			Iterator it = nodes.get(i).shortestPaths.entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry<Integer, Path> pair =  (Map.Entry<Integer, Path>)it.next();
				ArrayList<Integer> intermediateNodes = pair.getValue().seq;
				for (int j = 1; j < intermediateNodes.size(); j++) {
					int node = intermediateNodes.get(j);
					centerGraphs.get(node).ad.get(i).add(pair.getKey());
					centerGraphs.get(node).nodes.get(i).degree++;
					
					centerGraphs.get(node).ad.get(pair.getKey()).add(i);
					centerGraphs.get(node).nodes.get(pair.getKey()).degree++;
				}
			}
		}
	}
	
	public void generateLabels () throws Exception {
		PriorityQueue<Graph> pq = new PriorityQueue<Graph>(10, new AdsComparator());
		for (int i = 0; i < centerGraphs.size(); i++) {
			centerGraphs.get(i).calculateADS();
			pq.add(centerGraphs.get(i));
		}
		while (pq.size() != 0) {
			Graph topCg = pq.poll();
			if (topCg.touched == true) {
				topCg.touched = false;
				topCg.calculateADS();
				pq.add(topCg);
			}
			else {
				boolean ans = removeAdsAndCheckSize(topCg);
				updateLabel(topCg.ads);
				if (ans) {
					topCg.calculateADS();
					pq.add(topCg);
				}
			}
		}
	}
	
	public void updateLabel(Graph ads) {
		for (int i = 0; i < ads.ad.size(); i++) {
			for (int j = 0; j < ads.ad.get(i).size(); j++) {
				if (nodes.get(i).shortestPaths.get(ads.ad.get(i).get(j)) != null && nodes.get(i).shortestPaths.get(ads.ad.get(i).get(j)).seq.contains(ads.centerId)) {
					nodes.get(i).labelOut.put(ads.centerId, nodes.get(i).shortestPaths.get(ads.centerId).seq.size() - 1);
					nodes.get(ads.ad.get(i).get(j)).labelIn.put(ads.centerId, nodes.get(ads.centerId).shortestPaths.get(ads.ad.get(i).get(j)).seq.size() - 1);
				}
				if (nodes.get(ads.ad.get(i).get(j)).shortestPaths.get(i) != null && nodes.get(ads.ad.get(i).get(j)).shortestPaths.get(i).seq.contains(ads.centerId)) {
					nodes.get(ads.ad.get(i).get(j)).labelOut.put(ads.centerId, nodes.get(ads.ad.get(i).get(j)).shortestPaths.get(ads.centerId).seq.size() - 1);
					nodes.get(i).labelIn.put(ads.centerId, nodes.get(ads.centerId).shortestPaths.get(i).seq.size() - 1);
				}
			}
		}
	}
	
	public boolean removeAdsAndCheckSize(Graph topCg) {
		boolean ans = false;
		for (int i = 0; i < topCg.ads.ad.size(); i++) {
			ArrayList<Integer> nei = topCg.ads.ad.get(i);
			for (int j = 0; j < nei.size(); j++) {
				for (int k = 0; k < centerGraphs.size(); k++) {
					int index = centerGraphs.get(k).ad.get(i).indexOf(nei.get(j));
					if (index != -1) {
						centerGraphs.get(k).ad.get(i).remove(index);
						centerGraphs.get(k).nodes.get(i).degree--;
						centerGraphs.get(k).touched = true;
					}
				}
				
			}
			if (topCg.ad.get(i).size() > 0) {
				ans = true;
			}
		}
		return ans;
	}
	
	public void calculateADS () throws Exception {
		Graph cg = this.shallowCopy();
		if (cg.adsDensity == -1) {
			cg.ads = new Graph(n);
		}
		
		if (cg.touched == false && cg.adsDensity != -1) {
			return ;
		}
		PriorityQueue<Node> pq = new PriorityQueue<>(10, new DegreeComparator());
		
		for (int i = 0; i < n; i++) {
			if (cg.nodes.get(i).degree > 0) {
				pq.add(cg.nodes.get(i));
			}
		}
		double max = Double.MIN_VALUE;
		Graph ads = new Graph();
		
		while (pq.size() != 0) {
			double val = density(cg);
			if (val > max) {
				max = val;
				ads = cg.shallowCopy();
			}
			Node top = pq.poll();
			for (int i = 0; i < cg.ad.get(top.id).size(); i++) {
				int index = cg.ad.get(cg.ad.get(top.id).get(i)).indexOf(top.id);
				pq.remove(cg.nodes.get(cg.ad.get(top.id).get(i)));
				cg.ad.get(cg.ad.get(top.id).get(i)).remove(index);
				cg.nodes.get(cg.ad.get(top.id).get(i)).degree--;
				pq.add(cg.nodes.get(cg.ad.get(top.id).get(i)));
			}
			cg.ad.get(top.id).clear();
			cg.nodes.get(top.id).degree = 0;
		}
		System.out.println(cg.centerId + " " + max) ;
		ads.print();
		ads.centerId = cg.centerId;
		this.ads = ads;
		adsDensity = max;
		
	}
	
	public double density (Graph g) {
		double edgeCount = 0;
		double nodeCount = 0;
		for (int i = 0; i < n ; i++) {
			if (g.nodes.get(i).degree > 0) {
				nodeCount++;
				edgeCount += g.nodes.get(i).degree;
			}
		}
		if (nodeCount == 0) 
			return -1;
		return edgeCount/(2.0*nodeCount);
	}
	
	public Graph shallowCopy() {
		Graph g1 = new Graph();
		for (int i = 0; i < n; i++) {
			ArrayList<Integer> temp = new ArrayList<>();
			temp.addAll(ad.get(i));
			g1.ad.add(temp);
		}
		for (int i = 0; i < centerGraphs.size(); i++) {
			g1.centerGraphs.add(centerGraphs.get(i).shallowCopy());
		}
		for (int i = 0; i < n; i++) {
			g1.nodes.add(nodes.get(i).shallowCopy());
		}
		g1.n = n;
		g1.centerId = centerId;
		return g1;
	}
	
	public void test() throws Exception {
		BufferedReader bf = new BufferedReader(new FileReader("/Users/chendu/Desktop/DDP/New/Data/dublin/shortestPaths.txt"));
		int s = Integer.parseInt(bf.readLine());
		while (true) {
			String line = bf.readLine();
			if (line == null) {
				break;
			}
			if (line.contains("*")) {
				s = Integer.parseInt(bf.readLine());
				if (s == 2)
					break;
			}
			else {
				String[] splits = line.split(" ");
				int d = Integer.parseInt(splits[0]);
				double dist = Double.parseDouble(splits[1]);
				int twohopdist = distance (s, d);
				if ((int)dist != twohopdist) {
					twohopdist = distance (s, d);
					System.out.println("Fucked " + s + " " + d);
					System.out.println("Stored : " + dist + " 2hop : " + twohopdist);
				}
			}
		}
	}
	
	public int distance (int source, int destination) {
		
		Double ans = Double.MAX_VALUE;
		Iterator it = nodes.get(source).labelOut.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<Integer, Double> pair = (Entry<Integer, Double>) it.next();
			if (nodes.get(destination).labelIn.containsKey(pair.getKey())) {
				double dist = nodes.get(destination).labelIn.get(pair.getKey()) + pair.getValue();
				if (dist < ans)
					ans = dist;
			}
		}
		if (ans == Double.MAX_VALUE) {
			for (int i = 0; i < nodes.get(source).reverseLabelIn.size(); i++) {
				int id = nodes.get(source).reverseLabelIn.get(i);
				if (nodes.get(id).labelOut.containsKey(destination)) {
					double dist = nodes.get(id).labelIn.get(source) + nodes.get(id).labelOut.get(destination);
					if (dist < ans)
						ans = dist;
				}
			}
		}
		return ans.intValue();
	}
}
