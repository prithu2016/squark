package twohop;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

public class HopDBundirected {

	int n;
	LinkedHashMap<Integer, Node> allNodes;
	ArrayList<ArrayList<Integer>> ad; //adjacency matrix
	ArrayList<ArrayList<Integer>> weights; //weight matrix
	ArrayList<Node> nodes;
	int total_count = 0;
	
	public HopDBundirected () {
		ad = new ArrayList<>();
		weights = new ArrayList<>();
		allNodes = new LinkedHashMap<>();
		nodes = new ArrayList<>();
	}
	
	public void readGraph (String file) throws Exception {
		BufferedReader bf = new BufferedReader(new FileReader(file));
		String line = bf.readLine(); //#Nodes and #Edges
		String splits[] = line.split(" ");
		n = Integer.parseInt(splits[0]);
		int eold = Integer.parseInt(splits[1]);
		int enew = Integer.parseInt(splits[2]);
		
		for (int i = 0; i < n; i++) {
			Node n1 = new Node();
			n1.id =i;
			n1.rank = Integer.parseInt(bf.readLine());
			nodes.add(n1);
			ad.add(new ArrayList<Integer>());
			weights.add(new ArrayList<Integer>());
			nodes.get(i).label.put(i, 0);
			nodes.get(i).reverseLabel.add(i);
		}
		line = bf.readLine();
		int ecount = 0;
		
		for (int i = 0; i < eold; i++) {
			line = bf.readLine();
			if (line == null)
				break;
			String[] endPoints = line.split(" ");
			int u = Integer.parseInt(endPoints[0]);
			int v = Integer.parseInt(endPoints[1]);
			Integer weight = Integer.parseInt(endPoints[2]);
			int flag = Integer.parseInt(endPoints[3]);
			if (flag == 1 || flag == 3) {
				ad.get(u).add(v);
				weights.get(u).add(weight);
				ecount++;
			}
			if (flag == 2 || flag == 3) {
				ad.get(v).add(u);
				weights.get(v).add(weight);
				ecount++;
			}
			if (flag != 1 && flag != 2 && flag != 3) {
				System.out.println("Check flags");
			}
		}
		
		for (int i = 0; i < enew; i++) {
			line = bf.readLine();
			if (line == null)
				break;
			String[] endPoints = line.split(" ");
			int u = Integer.parseInt(endPoints[0]);
			int v = Integer.parseInt(endPoints[1]);
			Integer weight = Integer.parseInt(endPoints[2]);
			int flag = Integer.parseInt(endPoints[3]);
			if (flag == 5 || flag == 7) {
				ad.get(u).add(v);
				weights.get(u).add(weight);
				ecount++;
			}
			if (flag == 6 || flag == 7) {
				ad.get(v).add(u);
				weights.get(v).add(weight);
				ecount++;
			}
			if (flag != 7 && flag != 5 && flag != 6) {
				System.out.println("Check flags");
			}
		}
		
		int count1 = 0;
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < ad.get(i).size(); j++) {
				if (nodes.get(i).rank < nodes.get(ad.get(i).get(j)).rank) {
					nodes.get(i).label.put(ad.get(i).get(j), weights.get(i).get(j));
					nodes.get(ad.get(i).get(j)).reverseLabel.add(i);
					count1++;
				}
			}
		}
		System.out.println(ecount + " " + count1);
	}
	
	
	public void init () {
		Collections.sort(nodes, new DecreaseRankComparator());
		for (int i = 0; i < n; i++) {
			allNodes.put(nodes.get(i).id, nodes.get(i).shallowCopy());
		}
	}
	
	public void labelGeneration () {
		LinkedHashMap<Integer, Node>  currNodes = new LinkedHashMap<Integer, Node>();
		for (int i = 0; i < n ; i++) {
			currNodes.put(nodes.get(i).id, nodes.get(i).shallowCopy());
		}

		long writebacktime = 0;
		int count = 0;
		int flag = 0;
		while (true) {
			HashMap<Integer, Node>  newCurrNodes = new HashMap<Integer, Node>();
			Iterator it_curr = currNodes.entrySet().iterator();
			while (it_curr.hasNext()) {
				Map.Entry<Integer, Node> entry = (Entry<Integer, Node>) it_curr.next();
				Node curr_node = entry.getValue();
				if (count < 10) {
					printhandler(curr_node.id);
					count++;
				}
				rule1 (curr_node, newCurrNodes);
				rule2 (curr_node, newCurrNodes);
				flag = 1 - flag;
			}
			if (newCurrNodes.size() == 0)
				break;
			
			//adding new ones to allNodes
			
			boolean ans = addToAllNodes (newCurrNodes, currNodes);
		}
	}
	
	public int rule1 (Node curr, HashMap<Integer, Node>newCurrNodes) {
		int u, v, u1;
		u1 = -1;
		int d = 0;
		int d1 = 0;
		u = curr.id;
		Iterator it = curr.label.entrySet().iterator();
		int ans = 0;
		while (it.hasNext()) {
			Map.Entry<Integer, Integer> pair = (Map.Entry<Integer, Integer>) it.next();
			v = pair.getKey();
			d = pair.getValue();
			Node curr2 = allNodes.get(u);
			Iterator it2 = curr2.label.entrySet().iterator();
			while (it2.hasNext()) {
				Map.Entry<Integer, Integer> pair2 = (Map.Entry<Integer, Integer>) it2.next();
				u1 = pair2.getKey();
				d1 = pair2.getValue();
				if ( allNodes.get(v).rank > allNodes.get(u1).rank 
					&& 
					(	newCurrNodes.get(u1) == null || newCurrNodes.get(u1).label.get(v) == null /*||
						distance(newCurrNodes.get(u1), newCurrNodes.get(v)) > (d+d1)*/
					)
					&&
					(
						allNodes.get(u1) == null || allNodes.get(u1).label.get(v) == null ||
						/*distanceFavorable (allNodes.get(u1) , allNodes.get(v), d+d1)*/
						distance(u1, v) > (d + d1)
					)
				) {
					Node newU1 = new Node();
					if (newCurrNodes.containsKey(u1)) {
						newU1 = newCurrNodes.get(u1);
					}
					Node reverseNewU1 = new Node ();
					if (newCurrNodes.containsKey(v)) {
						reverseNewU1 = newCurrNodes.get(v);
					}
					
					newU1.id = u1;
					newU1.label.put(v, d + d1);
					
					reverseNewU1.id = v;
					reverseNewU1.reverseLabel.add(u1);
					
					newCurrNodes.put(u1, newU1);
					newCurrNodes.put(v, reverseNewU1);
					ans++;
				}
			}
		}
		return ans;
	}
	
	public int rule2 (Node curr, HashMap<Integer, Node> newCurrNodes) {
		int u, v, u2;
		int d = 0;
		int d2 = 0;
		u = curr.id;
		Iterator it = curr.label.entrySet().iterator();
		int ans = 0;
		while (it.hasNext()) {
			Map.Entry<Integer, Integer> pair = (Map.Entry<Integer, Integer>) it.next();
			v = pair.getKey();
			d = pair.getValue();
			if (v == u)
				continue;
			Iterator reverseiterator = allNodes.get(u).reverseLabel.iterator();
			while (reverseiterator.hasNext()) {
				Node curr2 =  allNodes.get((int) reverseiterator.next());
				u2 = curr2.id;
				d2 = curr2.label.get(u);
				if (v == u2 || u == u2)
					continue;
				if (!curr2.label.containsKey(u) ) {
					System.out.println("Reverse label wrong");
				}
				if (
					allNodes.get(v).rank > allNodes.get(u2).rank 
					&& 
					(	newCurrNodes.get(u2) == null || newCurrNodes.get(u2).label.get(v) == null ||
						newCurrNodes.get(u2).label.get(v) > (d+d2) /*||
						distance(newCurrNodes.get(u2), newCurrNodes.get(v)) > (d+d2)*/
					)
					&&
					(
						allNodes.get(u2) == null || allNodes.get(u2).label.get(v) == null ||
						/*distanceFavorable (allNodes.get(u2) , allNodes.get(v), d+d2)*/
								distance(u2, v) > (d + d2)
					)
				) {
					Node newU1 = new Node();
					if (newCurrNodes.containsKey(u2)) {
						newU1 = newCurrNodes.get(u2);
					}
					
					Node reverseNewU1 = new Node ();
					if (newCurrNodes.containsKey(v)) {
						reverseNewU1 = newCurrNodes.get(v);
					}
					
					newU1.id = u2;
					newU1.label.put(v, d + d2);
					
					reverseNewU1.id = v;
					reverseNewU1.reverseLabel.add(u2);
					
					newCurrNodes.put(u2, newU1);
					newCurrNodes.put(v, reverseNewU1);
					ans++;
				}
			}
		}
		return ans;
	}
	
	
	public boolean addToAllNodes (HashMap<Integer, Node> newCurrNodes, HashMap<Integer, Node> currNodes) {
		boolean flag = true;
		int count_added = 0;
		Iterator itt = newCurrNodes.entrySet().iterator();
		while (itt.hasNext()) {
			Map.Entry<Integer, Node> entry = (Entry<Integer, Node>) itt.next();
			Node n1 = entry.getValue();
			int id = entry.getKey();
			Iterator it = n1.label.entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry<Integer, Integer> pair = (Entry<Integer, Integer>) it.next();
				if (allNodes.get(id).label.containsKey(pair.getKey())) {
					/*if (allNodes.get(id).label.get(pair.getKey()) > pair.getValue())*/ {
						allNodes.get(id).label.put(pair.getKey(), pair.getValue());
						allNodes.get(pair.getKey()).reverseLabel.add(id);
						flag = false;
						count_added++;
					}
					/*else {
						System.out.println("Not add : " + id + " " + pair.getKey() + " " + pair.getValue());
						System.exit(1);
					}*/
				}
				else {
					allNodes.get(id).label.put(pair.getKey(), pair.getValue());
					allNodes.get(pair.getKey()).reverseLabel.add(id);
					flag = false;
					count_added++;
				}
			}
		}
		System.out.println("Size : " + count_added);
		currNodes.clear();
		currNodes.putAll(newCurrNodes);
		return flag;
	}
	
	public void printLabel() {
		//printhandler(678);
		//printhandler(679);
		//printhandler(682);
		//printhandler(932);
		/*printhandler(32282);*/
	}
	
	public void printhandler(int i) {
		System.out.println("Id : " + i + " Rank : " + allNodes.get(i).rank);
		System.out.println("label of " + allNodes.get(i).id);
		System.out.println(allNodes.get(i).label);
		System.out.println("Reverse label of " + allNodes.get(i).id);
		System.out.println(allNodes.get(i).reverseLabel);
		System.out.println("********************");
	}
	
	public void test() throws Exception {
		loadLabels();
		BufferedReader bf = new BufferedReader(new FileReader("/Users/chendu/Desktop/DDP/New/Data/dublin/shortestPaths.txt"));
		int s = Integer.parseInt(bf.readLine());
		while (true) {
			String line = bf.readLine();
			if (line == null) {
				break;
			}
			if (line.contains("*")) {
				String temp = bf.readLine();
				if (temp == null) {
					System.out.println("Done testing");
					break;
				}
				s = Integer.parseInt(temp);
			}
			else {
				String[] splits = line.split(" ");
				int d = Integer.parseInt(splits[0]);
				double dist = Double.parseDouble(splits[1]);
				int twohopdist = distance (s, d);
				if (!(twohopdist < Integer.MAX_VALUE)) {
					twohopdist = distance (s, d);
					System.out.println("Fucked " + s + " " + d);
					System.out.println("Stored : " + dist + " 2hop : " + twohopdist);
					/*printhandler(2682);
					printhandler(2684);
					printhandler(2686);
					printhandler(2689);
					printhandler(59960);*/
					//break;
				}
			}
		}
		System.out.println("Done");
	}
	
	public int distance (int source, int destination) {
		
		int ans = Integer.MAX_VALUE;
		Iterator it = allNodes.get(source).label.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<Integer, Integer> pair = (Entry<Integer, Integer>) it.next();
			if (allNodes.get(destination).label.containsKey(pair.getKey())) {
				int dist = allNodes.get(destination).label.get(pair.getKey()) + pair.getValue();
				if (dist < ans)
					ans = dist;
			}
		}
		if (ans == Integer.MAX_VALUE) {
			Iterator reverseiterator = allNodes.get(source).reverseLabel.iterator();
			while (reverseiterator.hasNext()) {
				int id = (int) reverseiterator.next();
				if (allNodes.get(id).label.containsKey(destination)) {
					int dist = allNodes.get(id).label.get(source) + allNodes.get(id).label.get(destination);
					if (dist < ans)
						ans = dist;
				}
			}
		}
		return ans;
	}
	
	public int distance (Node source, Node destination) {
		int ans = Integer.MAX_VALUE;
		Iterator it = source.label.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<Integer, Integer> pair = (Entry<Integer, Integer>) it.next();
			if (destination.label.containsKey(pair.getKey())) {
				int dist = destination.label.get(pair.getKey()) + pair.getValue();
				if (dist < ans)
					ans = dist;
			}
		}
		if (ans == Integer.MAX_VALUE) {
			Iterator reverseiterator = source.reverseLabel.iterator();
			while (reverseiterator.hasNext()) {
				int id = (int) reverseiterator.next();
				if (allNodes.get(id).label.containsKey(destination.id)) {
					int dist = allNodes.get(id).label.get(source.id) + allNodes.get(id).label.get(destination.id);
					if (dist < ans)
						ans = dist;
				}
			}
		}
		return ans;
	}
	public boolean distanceFavorable (Node source, Node destination, Integer d) {
		Integer ans = Integer.MAX_VALUE;
		Iterator it = source.label.entrySet().iterator();
		int pivot = 0;
		while (it.hasNext()) {
			Map.Entry<Integer, Integer> pair = (Entry<Integer, Integer>) it.next();
			if (destination.label.containsKey(pair.getKey())) {
				Integer dist = destination.label.get(pair.getKey()) + pair.getValue();
				if (dist < ans) {
					ans = dist;
					pivot = pair.getKey();
				}
			}
		}
		if (ans.intValue() > d || (ans.intValue() == d && allNodes.get(pivot).rank < destination.rank))
				return true;
		return false;			
	}

	public void loadLabels () throws Exception {
		BufferedReader bf = new BufferedReader(new FileReader("/Users/chendu/Desktop/DDP/New/Data/dublin/dublin.labels.undirected"));
		int n = 62975;
		for (int i = 0; i < n; i++) {
			Node n1 = new Node();
			n1.id = i;
			n1.label.put(i, 0);
			n1.reverseLabel.add(i);
			allNodes.put(i, n1);
		}
		while (true) {
			String line = bf.readLine();
			if (line == null)
				break;
			int id = Integer.parseInt(line.split(" ")[0]);
			int count = Integer.parseInt(line.split(" ")[1]);
			for (int i = 0; i < count; i++) {
				line = bf.readLine();
				int labelId = Integer.parseInt(line.split(" ")[0]);
				int value = Integer.parseInt(line.split(" ")[1]);
				allNodes.get(id).label.put(labelId, (Integer)value);
			}
		}
		System.out.println("Done loading");
	}
}
