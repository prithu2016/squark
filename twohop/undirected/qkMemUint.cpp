#include "ioUint.h"
#include <vector>

using namespace std;

const int kmax = 1024;
typedef unsigned long long htype;
bool ftest = 0;

int queryCnt;

char * txtName, * degName, * outName, * ktName, * keyNodeName, * nodeKeyName, * kcntName, * lbName, *firstName;
vector<vector<int> > keyNode, nodeKey;

const int ranSize = 16384;
htype * kt, * hash, * ran, * nodeHash;

long long * pos, ktCnt;
int n, kn, km, bfkn = 0, bfkm = 0, tkn = 0, tkm = 0;
long long m, mm;

int * nid;
long long * outPos;
edgeL * deg;
edgeS * labelout, * lb;
edgeS * labelx, * labely;

int ansV[kmax+1], ansCnt;
wtype ansDis[kmax+1];

int hCnt;

struct ihData
{
	wtype w;
	long long lb;
	int k, nt, wh;
}tmpData;

ihData * fbs;

ihData * tmpHW, * fbsHW[2][kmax/2+1], * testHW[2][kmax/2+1];

ihData **fbsDis;
edgeS nullEdge;

long long * lbfirst1, * lbfirst2;
int * lbfirst3;

wtype * hashDis;

#define tmid(x,y) ( (x+y)>>1 )
#define HWSwap(i) {tmpHW = fbsHW[0][i]; fbsHW[0][i] = fbsHW[1][i]; fbsHW[1][i] = tmpHW; fbsHW[0][i]->wh = (i<<1)-2, fbsHW[1][i]->wh = (i<<1)-1;}

double ktTime;

int ktSearch, ktFalse, ktNode;
long long ktLen, ktAns, ktBit;

wtype queryDis(int x, int y)
{
	if (x == y) return 0;
	int xx = x, yy = y;

	x = ((deg[xx].x<<32)>>32);
	y = ((deg[yy].x<<32)>>32);
	
	if (x > y)
	{
		labelx = labelout + deg[xx].w;
		labely = labelout + deg[yy].y;
	}
	else
	{
		int xy = x; x = y; y = xy;
		labelx = labelout + deg[yy].y;
		labely = labelout + deg[xx].w;
	}
	
	wtype ans = 10000000;
	int i = 0, j = 0;
	
	
	if (labelx[i].x != -1 && labely[j].x != -1)
	while (labelx[i].x < y)
	{

		if (labelx[i].x == labely[j].x) 
		{
			ans = ans>(labelx[i].w + labely[j].w)?(labelx[i].w + labely[j].w):ans;
			if (labelx[++i].x == -1) break;
			if (labely[++j].x == -1) break;
		}
		else if (labelx[i].x < labely[j].x)
		{
			if (labelx[++i].x == -1) break;
		}
		else if (labely[++j].x == -1) break;
	}
	
	while (labelx[i].x != -1 && labelx[i].x < y) i++;
	if (labelx[i].x == y) ans = ans>labelx[i].w?labelx[i].w:ans;
	
		
	return ans;
}

inline void downAns(int i)
{
	ansDis[0] = ansDis[i];
	ansV[0] = ansV[i];
	for (int j; (j=(i<<1)) <= ansCnt; i = j)
	{
		if (j < ansCnt && ansDis[j] < ansDis[j+1]) j++;
		if (ansDis[0] < ansDis[j]) 
		{
			ansDis[i] = ansDis[j];
			ansV[i] = ansV[j];
		}
		else break;
	}
	ansDis[i] = ansDis[0];
	ansV[i] = ansV[0];
}


inline void fbsMinDown(int i)
{
//sprintf(testStr, "mindown %d", i);
	tmpHW = fbsHW[0][i];
	for (int j, hCnt_ = ((hCnt+1)>>1); (j=(i<<1)) <= hCnt_; i = j )
	{
//		printf("i j %d %d\n", i, j);
		if (j < hCnt_ && fbsHW[0][j+1]->w < fbsHW[0][j]->w ) j++;
//		printf("i j %d %d\n", i, j);
		if (tmpHW->w > fbsHW[0][j]->w)
		{
//		printf("1 i j %d %d\n", i, j);
			fbsHW[0][i] = fbsHW[0][j];
//		printf("2 i j %d %d\n", i, j);
			fbsHW[0][i]->wh = (i<<1)-2;
//		printf("3 i j %d %d\n", i, j);
			if (hCnt >= (j<<1) && tmpHW->w > fbsHW[1][j]->w)
			{
//		printf("4 i j %d %d\n", i, j);
				fbsHW[0][0] = tmpHW; tmpHW = fbsHW[1][j]; fbsHW[1][j] = fbsHW[0][0];
//		printf("5 i j %d %d\n", i, j);
				fbsHW[1][j]->wh = (j<<1)-1;
			}			
		}
		else break;	
//		printf("i j %d %d\n", i, j);
	}
	fbsHW[0][i] = tmpHW;
	fbsHW[0][i]->wh = (i<<1)-2;
	
}

inline void fbsMinUp(int i)
{
//sprintf(testStr, "minup %d", i);
	tmpHW = fbsHW[0][i];
	for (int j; j=(i>>1); i = j )
	{
		if (tmpHW->w < fbsHW[0][j]->w)
		{
			fbsHW[0][i] = fbsHW[0][j];
			fbsHW[0][i]->wh = (i<<1)-2;
			if (tmpHW->w > fbsHW[1][j]->w)
			{
				fbsHW[0][0] = tmpHW; tmpHW = fbsHW[1][j]; fbsHW[1][j] = fbsHW[0][0];
				fbsHW[1][j]->wh = (j<<1)-1;
			}			
		}
		else break;	
	}
	fbsHW[0][i] = tmpHW;
	fbsHW[0][i]->wh = (i<<1)-2;
	
}


inline void fbsMaxDown(int i)
{
//sprintf(testStr, "maxdown %d", i);
	tmpHW = fbsHW[1][i];
	for (int j, hCnt_ = (hCnt>>1); (j=(i<<1)) <= hCnt_; i = j )
	{
		if (j == hCnt_ && (hCnt & 1) && fbsHW[1][j]->w < fbsHW[0][j+1]->w && tmpHW->w < fbsHW[0][j+1]->w)
		{
			fbsHW[1][i] = fbsHW[0][j+1];
			fbsHW[1][i]->wh = (i<<1)-1;
			fbsHW[0][j+1] = tmpHW;
			fbsHW[0][j+1]->wh = hCnt-1;
			return;
		}
		
		if (j < hCnt_ && fbsHW[1][j+1]->w > fbsHW[1][j]->w ) j++;
		if (tmpHW->w < fbsHW[1][j]->w)
		{
			fbsHW[1][i] = fbsHW[1][j];
			fbsHW[1][i]->wh = (i<<1)-1;
			if (tmpHW->w < fbsHW[0][j]->w)
			{
				fbsHW[0][0] = tmpHW; tmpHW = fbsHW[0][j]; fbsHW[0][j] = fbsHW[0][0];
				fbsHW[0][j]->wh = (j<<1)-2;
			}
		}
		else break;	
	}
	
	if ((i<<2)-1 == hCnt && (hCnt & 3) == 3 && fbsHW[0][i<<1]->w > tmpHW->w)
	{
		fbsHW[1][i] = fbsHW[0][i<<1];
		fbsHW[1][i]->wh = (i<<1)-1;
		fbsHW[0][i<<1] = tmpHW;
		fbsHW[0][i<<1]->wh = (i<<2)-2;
		return;
	}
	
	fbsHW[1][i] = tmpHW;
	fbsHW[1][i]->wh = (i<<1)-1;

}


inline void fbsMaxUp(int i)
{
//sprintf(testStr, "maxup %d", i);
	tmpHW = fbsHW[1][i];
	for (int j; j=(i>>1); i = j)
	{
		if (tmpHW->w > fbsHW[1][j]->w)
		{
			fbsHW[1][i] = fbsHW[1][j];
			fbsHW[1][i]->wh = (i<<1)-1;
			if (tmpHW->w < fbsHW[0][j]->w)
			{
				fbsHW[0][0] = tmpHW; tmpHW = fbsHW[0][j]; fbsHW[0][j] = fbsHW[0][0];
				fbsHW[0][j]->wh = (j<<1)-2;
			}			
		}
		else break;	
	}
	fbsHW[1][i] = tmpHW;
	fbsHW[1][i]->wh = (i<<1)-1;
	
}


inline long long ktNext(long long x, long long pre, int w)
{
	int t = pre-1-x, s = (t&(t+1)), p, xx, yy, zz;
//	ktSearch++;
//	ktLen += pre-x;
	while (1)
	{
//		ktBit+= t-s;
		
//		ktNode++;
//		printf("%lld %d %d %d\n", x, s, t, ((kt[(x<<1)+((s+t)|(s!=t))] & hash[w]) == 0));
		if ((kt[(x<<1)+s+t] & ran[(w ^ (s+t))&(ranSize-1)]) == 0)
		{
			if (s == 0) 
			{
//				ktAns += pre-x;
				return -1;
			}
			t = s-1;
			s = (t&(t+1));			
		}
		else if (s == t)
		{
			p = lb[x+s].x;
			xx = 0;
			yy = nodeKey[p].size()-1;
			while (xx <= yy)
			{
				zz = tmid(xx,yy);
				if (nodeKey[p][zz] == w) break;
				else if (nodeKey[p][zz] > w) yy = zz-1;
				else xx = zz+1;
			}
			if (xx <= yy) 
			{
//				ktAns += pre-x-s;
				return x+s;
			}
			else if (s == 0) 
			{
//				ktAns += pre-x;
				return -1;
			}
//			ktFalse++;
			t = s-1;
			s = (t&(t+1));							
		}
		else s = tmid(s,t)+1;
	}
	
	
}

double timelen1;
inline long long ktNextOS(long long x, long long pre, int w)
{
	int wHash = 198911LL * w % 1999993, xx, yy, p, t = pre - x - 1, zz;
	for (int i = 0; i <= (t & 1); i++)
	{
		p = lb[x+t-i].x;
		if (nodeHash[p] & ran[ ((wHash + p) % 1999993) & (ranSize-1) ])
		{
			xx = 0;
			yy = nodeKey[p].size()-1;
			while (xx <= yy)
			{
				zz = tmid(xx,yy);
				if (nodeKey[p][zz] == w) break;
				else if (nodeKey[p][zz] > w) yy = zz-1;
				else xx = zz+1;
			}
//if (xx <= yy && p == 1275199) printf("%lld %lld %lld\n", x, pre, x+t-i);
			if (xx <= yy) return x+t-i;
		}
	}
	if ((t = (t | 1) - 2) < 0) return -1;

	int s = t - 1, nextT = t, nextS, len;
//	ktSearch++;
//	ktLen += pre-x;
	while (1)
	{
//if (x == pos[13]) printf("%lld %lld (%d %d) KT > 0? %d\n", x+s, x+t, s, t, (kt[x + tmid(s,t)] & ran[(w ^ (s+t))&(ranSize-1)]) > 0);

//		ktBit+= len;
//		ktNode++;
		if ((kt[x + tmid(s,t)] & ran[(w ^ (s+t))&(ranSize-1)]) == 0)
		{
			len = t - s;
			if ((t = s-1) < 0) 
			{
//				ktAns += pre - x;
				return -1;
			}
			else if (t > nextT) s = ((t+1)&t);
			else 
			{
				if (t == nextT) s = nextS;
				else s = t - len;
				if ((t & (t+1)) < s) s = 2 * s - t - 1;
			}
		}
		else if (s == t-1)
		{
			for (int i = t; i >= s; i--)
			{
				p = lb[x+i].x;
				if (nodeHash[p] & ran[ ((wHash + p) % 1999993) & (ranSize-1) ])
				{
					xx = 0;
					yy = nodeKey[p].size()-1;
					while (xx <= yy)
					{
						zz = tmid(xx,yy);
						if (nodeKey[p][zz] == w) break;
						else if (nodeKey[p][zz] > w) yy = zz-1;
						else xx = zz+1;
					}

//if (xx <= yy && p == 1275199) printf("%lld %lld %lld\n", x, pre, x+i);
					if (xx <= yy) return x+i;
				}
			}
			len = t - s;
			if ((t = s-1) < 0) return -1;
			else if (t > nextT) s = ((t+1)&t);
			else 
			{
				if (t == nextT) s = nextS;
				else s = t - len;
				if ((t & (t+1)) < s) s = 2 * s - t - 1;
			}
		}
		else 
		{
			if (nextT > s - 1) 
			{
				nextT = s - 1;
				nextS = 2 * s - t - 1;
			}
			s = tmid(s, t)+1;
		}
	}
	
	
}

long long bf(long long x, long long pre, int w)
{
//	printf("bf %lld %lld %d, %d\n", x, pre, w, lb[pre].x);
	for (long long i = pre-1; i >= x; i--)
	{
//		if (lb[i].x == 480057) printf("yues\n");
//		if (lb[pre].x == 751880) printf("%d\n", lb[i].x);
		for (int j = 0, jj = nodeKey[lb[i].x].size(); j < jj; j++)
			if (w == nodeKey[lb[i].x][j]) 
			{
//				printf("%lld !!!!\n", i);
				return i;		
			}	
	}
	return -1;
}

inline long long ktNext_(long long x, long long pre, int w)
{
	timer tm;
	long long ans = bf(x, pre, w);
//	long long ans = ktNext(x, pre, w);
	ktTime += tm.getTime();
	
//	printf("ans %lld\n", ans);
	return ans;
}

int tCnt = 0;
//void testHeap()
//{
//	return;
//	printf("%d %d  ", ansCnt, hCnt);
//	for (int i = 0; i < hCnt; i++) { if (!(i&1)) printf("("); printf("%d[%d] ", fbsHW[i&1][(i>>1)+1]->w, fbsHW[i&1][(i>>1)+1]-fbs); if ((i&1)) printf(") "); } printf("\n");
//	return;
//	bool ff = 0;
//	for (int i = 1; i <= (hCnt+1)>>1; i++)
//	{
//		if (i*2 <= hCnt && fbsHW[0][i]->w > fbsHW[1][i]->w) ff = 1;
//		if (i*4 -1 <= hCnt && (fbsHW[0][i]->w > fbsHW[0][i*2]->w || fbsHW[1][i]->w < fbsHW[0][i*2]->w )) ff = 1;
//		if (i*4    <= hCnt && (fbsHW[0][i]->w > fbsHW[1][i*2]->w || fbsHW[1][i]->w < fbsHW[1][i*2]->w )) ff = 1;
//		if (i*4 +1 <= hCnt && (fbsHW[0][i]->w > fbsHW[0][i*2+1]->w || fbsHW[1][i]->w < fbsHW[0][i*2+1]->w )) ff = 1;
//		if (i*4 +2 <= hCnt && (fbsHW[0][i]->w > fbsHW[1][i*2+1]->w || fbsHW[1][i]->w < fbsHW[1][i*2+1]->w )) ff = 1;
//	}	
//	
//	if (ff)
//	{
//		printf("%s\n", testStr);
//		for (int i = 0; i < tCnt; i++) { if (!(i&1)) printf("("); printf("%d ", testHW[i&1][(i>>1)+1]->w); if ((i&1)) printf(") "); } printf("\n");
//		for (int i = 0; i < hCnt; i++) { if (!(i&1)) printf("("); printf("%d ", fbsHW[i&1][(i>>1)+1]->w); if ((i&1)) printf(") "); } printf("\n");
//		exit(0);
//	}
//	else
//	{
//		tCnt = hCnt;
//		for (int i = 1; i <= (hCnt+1)>>1; i++)
//		{
//			testHW[0][i] = fbsHW[0][i];
//			testHW[1][i] = fbsHW[1][i];
//		}
//	}
//	
//	
//}


inline void query(int x, int w, int k)
{
//	printf("query %d %d %d %u\n", x, w, k, hash[w]);
	if (hash[w] == 0)
	{
//f search
		int wCnt = keyNode[w].size();
/*		if (k >= wCnt)
		{
			ansCnt = wCnt;
			for (int i = 0; i < ansCnt; i++)
				ansV[i] = keyNode[w][i];
			return;
		}
*/		
	
		for (labelx = labelout + outPos[x]; labelx->x != -1; labelx++)
//			if (hashDis[labelx->x] < 0 || hashDis[labelx->x] > labelx->w)
				hashDis[labelx->x] = labelx->w;
		hashDis[nid[x]] = 0;
//----!!!!! for florida
		
//		printf("1454: %d %d\n", hashDis[1454], nid[567003]);

		ansCnt = 0;
		wtype ans, ans1;
			for (int i = 0; i < wCnt; i++)
			{
				if (ansCnt == k) ans = ansDis[1];
				else ans = 100000000;
				if (hashDis[nid[keyNode[w][i]]] > -1 && ans > hashDis[nid[keyNode[w][i]]]) ans = hashDis[nid[keyNode[w][i]]];


//if (keyNode[w][i] == 567003) printf("%f\n", ans);
				
				for (labely = labelout + outPos[keyNode[w][i]]; labely->x > -1 && labely->w < ans; labely++)
				{
//if (keyNode[w][i] == 567003) printf("%d %f %f %f %d\n",  labely->x, hashDis[labely->x] + labely->w, hashDis[labely->x], labely->w, hashDis[labely->x] > -1);
					if (hashDis[labely->x] > -1)
						ans = ans < labely->w + hashDis[labely->x] ? ans : labely->w + hashDis[labely->x];
				}

				if (ans > 99999999) continue;
				
				if (ansCnt < k)
				{
					ansV[++ansCnt] = keyNode[w][i];
					ansDis[ansCnt] = ans;
					if (ansCnt == k)
					for (int j = (k>>1); j >= 1; j--)
						downAns(j);
				}
				else if (ansDis[1] > ans)
				{
					ansV[1] = keyNode[w][i];
					ansDis[1] = ans;
					downAns(1);
				}
			
			}

		for (labelx = labelout + outPos[x]; labelx->x != -1; labelx++)
			hashDis[labelx->x] = -1;
		hashDis[nid[x]] = -1;

	}
	else
	{
//fbs search
		
		labelx = labelout + outPos[x];
		
//bool finit = 0;

		int len = 0;
		while (labelx[len].x != -1) len++;
		labelx[len].x = nid[x];
		labelx[len++].w = 0;
		
		fbs = (ihData *)malloc(sizeof(ihData) * len);
		
		long long xx, yy, zz;
		hCnt = 0;
		int p, q;


		for (int i = 0, j; i < len; i++)
		{				
			xx = lbfirst1[labelx[i].x];
			yy = lbfirst1[labelx[i].x+1]-1;


			while (xx <= yy)
			{
				zz = tmid(xx,yy);
				if (lbfirst3[zz] == w) break;
				else if (lbfirst3[zz] < w) xx = zz+1;
				else yy = zz-1;
			}
				
			if (xx > yy) 
			{
				fbs[i].lb = -1;	
				continue;
			}
			
			
			fbs[i].w = lb[fbs[i].lb = lbfirst2[zz]].w + labelx[i].w;
			
//if (finit) testHeap();;

//printf("%d %d %d %f %f %f\n",i, labelx[i].x, lb[lbfirst2[zz]].x, fbs[i].w, labelx[i].w, lb[lbfirst2[zz]].w );
	
			
			if (hCnt == k && (hCnt > 1 && fbsHW[1][1]->w <= fbs[i].w || hCnt == 1 && fbsHW[0][1]->w <= fbs[i].w) )
			{
				fbs[i].lb = -1;
				continue;
			}

//if (finit) testHeap();;
	
			fbs[i].k = 1;
			if (fbsDis[j=lb[fbs[i].lb].x] == NULL)
			{
				fbsDis[j] = &fbs[i];
				fbs[i].nt = -1;
			}
			else if (fbsDis[j]->w <= fbs[i].w)
			{
				fbs[i].nt = fbsDis[j]->nt;
				fbsDis[j]->nt = i;	
				continue;
			}
			else
			{

//if (finit) testHeap();;
				fbs[i].wh = fbsDis[j]->wh;
				fbsHW[p = (fbs[i].wh & 1)][q = 1+(fbs[i].wh>>1)] = &fbs[i];
				fbs[i].nt = fbsDis[j] - fbs;
				fbsDis[j] = &fbs[i];
				if (hCnt == k)
				{
					if (p) 
					{
						if (fbsHW[0][q]->w > fbsHW[1][q]->w)
						{
							HWSwap(q);
							fbsMinUp(q);
							fbsMaxDown(q);
						}
						else fbsMaxDown(q);
					}
					else fbsMinUp(q);
				
					// key down
//if (finit) testHeap();;
				} 			
				continue;
			}
			
//if (finit) testHeap();;

			if (hCnt < k)
			{
				fbs[i].wh = hCnt;
				fbsHW[hCnt&1][1+(hCnt>>1)] = &fbs[i];

				if (++hCnt == k)
				{						

					for (int i = (hCnt>>1); i >= 1; i--)
					{
						if (fbsHW[0][i]->w > fbsHW[1][i]->w) HWSwap(i);
						fbsMinDown(i);
						fbsMaxDown(i);
					}
//finit = 1;
					
					// init
					
				}
				
//if (finit) testHeap();;
			}
			else if (hCnt > 1)
			{
				fbsDis[lb[fbsHW[1][1]->lb].x] = NULL;
				fbsHW[1][1]->lb = -1;
				fbsHW[1][1] = &fbs[i];
				fbs[i].wh = 1;
				
				if (fbsHW[0][1]->w > fbsHW[1][1]->w)
					HWSwap(1);

				fbsMaxDown(1);
//if (finit) testHeap();;
				// replace max
				
			}
			else 
			{
				fbsDis[lb[fbsHW[0][1]->lb].x] = NULL;
				fbsHW[0][1]->lb = -1;
				fbsHW[0][1] = &fbs[i];
				fbs[i].wh = 0;
//if (finit) testHeap();;
			}
			
		}
		


		if (hCnt < k)
		{
			
			for (int i = (hCnt>>1); i >= 1; i--)
			{
//				printf("%d\n", i);
				if (fbsHW[0][i]->w > fbsHW[1][i]->w) HWSwap(i);
//				printf("%d\n", i);
				fbsMinDown(i);				
//				printf("%d\n", i);
				fbsMaxDown(i);
			}
//finit = 1;
			
			// init 
			
		}
//printf("9\n");

//printf("%d\n", hCnt);

//if (finit) testHeap();;

		ansCnt = 0;
		int hCnt_ = hCnt, i, ii, j;
		
//if (finit) testHeap();;
		
				
		while(hCnt)
		{
//if (finit) testHeap();;
			ansV[++ansCnt] = lb[fbsHW[0][1]->lb].x;
			ansDis[ansCnt] = fbsHW[0][1]->w;

//printf("----%d %lf\n", ansV[ansCnt], ansDis[ansCnt]);
//printf("%d %lf %d %lf %d %lf %d %lf\n", lb[fbsHW[0][1]->lb].x, fbsHW[0][1]->w, lb[fbsHW[1][1]->lb].x, fbsHW[1][1]->w, lb[fbsHW[0][2]->lb].x, fbsHW[0][2]->w, lb[fbsHW[1][2]->lb].x, fbsHW[1][2]->w);

			if (ansCnt == k) break;
			
			hCnt--;
			
			if (hCnt > 0)
			{
				fbsHW[0][1] = fbsHW[hCnt&1][(hCnt>>1)+1];
				fbsHW[0][1]->wh = 1;
				if (fbsHW[0][1]->w > fbsHW[1][1]->w) HWSwap(1);
				fbsMinDown(1);				
			}
			
			// delete min
//if (finit) testHeap();;




			
						
			for (i = fbsDis[ansV[ansCnt]] - fbs, fbsDis[ansV[ansCnt]] = &tmpData; i > -1; i = ii)
			{
				ii = fbs[i].nt;

//if (finit) testHeap();;
//printf("try i ii %d %d\n", i, ii);

				while (fbs[i].k < k && pos[labelx[i].x] < fbs[i].lb && (fbs[i].lb = ktNextOS(pos[labelx[i].x], fbs[i].lb, w)) != -1)
				{
					fbs[i].w = labelx[i].w + lb[fbs[i].lb].w;
//if (finit) testHeap();;
//printf("i w %d %d %lld %d\n", i, fbs[i].w, fbsDis[j=lb[fbs[i].lb].x], hCnt < k-ansCnt);
					if (hCnt == k-ansCnt && (hCnt == 1 && fbsHW[0][1]->w <= fbs[i].w || hCnt > 1 && fbsHW[1][1]->w <= fbs[i].w))
					{
						fbs[i].lb = -1;
						break;
					}	
//if (finit) testHeap();;
		
					fbs[i].k++;
					if (fbsDis[j=lb[fbs[i].lb].x] == NULL)
					{
						fbsDis[j] = &fbs[i];
						fbs[i].nt = -1;
					}
					else if (fbsDis[j]->w < 0)
					{
						continue;
					}
					else if (fbsDis[j]->w <= fbs[i].w)
					{
						fbs[i].nt = fbsDis[j]->nt;
						fbsDis[j]->nt = i;
						break;
					}
					else
					{
						fbs[i].wh = fbsDis[j]->wh;
						fbsHW[p = (fbs[i].wh & 1)][q = 1+(fbs[i].wh>>1)] = &fbs[i];
						fbs[i].nt = fbsDis[j] - fbs;
						fbsDis[j] = &fbs[i];
						if (p) 
						{
							if (fbsHW[0][q]->w > fbsHW[1][q]->w)
							{
								HWSwap(q);
								fbsMinUp(q);
								fbsMaxDown(q);
							}
							else fbsMaxDown(q);
						}
						else fbsMinUp(q);
							
						// key down
						
						
						break;
					}
									
//if (finit) testHeap();;
									
					if (hCnt < k-ansCnt)
					{
						fbs[i].wh = hCnt;
						fbsHW[p = (hCnt&1)][q = 1+(hCnt>>1)] = &fbs[i];
						hCnt++;
						if (p)
						{
							if (fbsHW[0][q]->w > fbsHW[1][q]->w) 
							{
								HWSwap(q);
								fbsMinUp(q);
							}
							else fbsMaxUp(q);	
						}
						else if (q > 1)
						{
							if (fbsHW[0][q]->w < fbsHW[0][q>>1]->w)
							{
								tmpHW = fbsHW[0][q]; fbsHW[0][q] = fbsHW[0][q>>1]; fbsHW[0][q>>1] = tmpHW; 
								fbsHW[0][q]->wh = hCnt-1, fbsHW[0][q>>1]->wh = ((q>>1)<<1)-2;
								fbsMinUp(q>>1);
							}
							else if (fbsHW[0][q]->w > fbsHW[1][q>>1]->w)
							{
								tmpHW = fbsHW[0][q]; fbsHW[0][q] = fbsHW[1][q>>1]; fbsHW[1][q>>1] = tmpHW; 
								fbsHW[0][q]->wh = hCnt-1, fbsHW[1][q>>1]->wh = ((q>>1)<<1)-1;
								fbsMaxUp(q>>1);
							}
						}
						// insert in the end
						
					}
					else if (hCnt > 1)
					{
						fbsDis[lb[fbsHW[1][1]->lb].x] = NULL;
						fbsHW[1][1]->lb = -1;
						fbsHW[1][1] = &fbs[i];
						fbs[i].wh = 1;
						
						if (fbsHW[0][1]->w > fbsHW[1][1]->w)
							HWSwap(1);

						fbsMaxDown(1);
												
						// replace largest
						
					}
					else
					{
						fbsDis[lb[fbsHW[0][1]->lb].x] = NULL;
						fbsHW[0][1]->lb = -1;
						fbsHW[0][1] = &fbs[i];
						fbs[i].wh = 0;
					}
					break;

				}

//if (finit) testHeap();;





			}
		}

		for (int i = 1; i <= ansCnt; i++)
			fbsDis[ansV[i]] = NULL;
		for (int i = 0; i < len; i++)
		{
			if (fbs[i].lb != -1)
			fbsDis[lb[fbs[i].lb].x] = NULL;
		}	

		
		labelx[len-1].x = -1;

////if (finit) testHeap();;

		free(fbs);
	}

}



void loadIndex()
{
	inBufS outLabel(outName), lbBuf(lbName);
	inBufL degBuf(degName);
	
	n = checkB(degName)/sizeof(edgeL);

	nid = (int *)malloc(sizeof(int)*n);
	outPos = (long long*)malloc(sizeof(long long)*n);
	deg = (edgeL *)malloc(sizeof(edgeL)*n);
	labelout = (edgeS*)malloc(checkB(outName));
	lb = (edgeS*)malloc(checkB(lbName));

	degBuf.start();
	edgeL tmpL;
	for (int i = 0; i < n; i++)
	{
		degBuf.nextEdge(tmpL);
		nid[i] = ((tmpL.x<<32)>>32);
		outPos[i] = tmpL.y;
		deg[i] = tmpL;
	}
/*	{
		int x = 496728;
		printf("%lld ==\n", outPos[x]);
		for (long long i = outPos[x]; labelx[i].x != -1; i++)
		{
			printf("%lld\n", i);
			printf("%d %d %d\n", x, labelx[i].x, labelx[i].w);
		}
	}
*/	outLabel.start();
	for (m = 0; !outLabel.isEnd;)
		outLabel.nextEdge(labelout[m++]);	

	hashDis = (wtype *)malloc(sizeof(wtype) * n);
	for (int i = 0; i < n; i++)
		hashDis[i] = -1;

	lbBuf.start();
	for (mm = 0; !lbBuf.isEnd;)
		lbBuf.nextEdge(lb[mm++]);
	
	FILE * fkeyNode = fopen(keyNodeName, "r");
	fscanf(fkeyNode, "%d%d", &kn, &km);
	
	hash = (htype *)malloc(sizeof(htype) * (long long)kn);
	ran = (htype *)malloc(sizeof(htype) * (long long)ranSize);
	nodeHash = (htype *)malloc(sizeof(htype) * (long long)n);
	ktCnt = (checkB(ktName)- sizeof(htype)*(long long)(kn+ranSize+n) - (n+1LL) * sizeof(long long))/sizeof(htype);
	kt = (htype *)malloc( ktCnt * sizeof(htype));
	pos = (long long *)malloc((n+1LL) * sizeof(long long));
	
	FILE * ktFile = fopen(ktName, "rb"); 
	fread(ran, sizeof(htype), ranSize, ktFile);
	
//	FILE * ff = fopen("frran", "w");
//	for (int i = 0; i < ranSize; i++)
//		fprintf(ff, "%llu\n", ran[i]);
	
	fread(hash, sizeof(htype), kn, ktFile);

//	ff = fopen("frhash", "w");
//	for (int i = 0; i < kn; i++)
//		fprintf(ff, "%llu\n", hash[i]);
//	exit(0);
	
	fread(nodeHash, sizeof(htype), n, ktFile);
//	fread(kt, sizeof(htype), ktCnt, ktFile);
	{
		long long i = 0;
		while (1073741824/sizeof(htype) * (i+1) < ktCnt)
			fread(kt + 1073741824/sizeof(htype) * (i++), sizeof(htype), 1073741824/sizeof(htype), ktFile);
		fread(kt + 1073741824/sizeof(htype) * i, sizeof(htype), ktCnt - 1073741824/sizeof(htype) * i, ktFile);
	}
	fread(pos, sizeof(long long), n+1, ktFile);
	
	for (int i = 0; i < n; i++)
		if (pos[i] > pos[i+1]) 
		{
			printf("pos error %d %lld %lld\n", i, pos[i], pos[i+1]);
			exit(0);
		}
		
	lbfirst1 = (long long*)malloc((n+1LL) * sizeof(long long));
	long long fsize = checkB(firstName) - (n+1LL) * sizeof(long long);
	lbfirst2 = (long long*)malloc(fsize/3*2);
	lbfirst3 = (int*)malloc(fsize/3);
	FILE * firstFile = fopen(firstName, "rb");
	fread(lbfirst1, sizeof(long long), n+1LL, firstFile);
//	fread(lbfirst2, sizeof(long long), fsize/3/sizeof(int), firstFile);
	{
		long long i = 0;
		while (1073741824/sizeof(long long) * (i+1) < fsize/3/sizeof(int))
			fread(lbfirst2 + 1073741824/sizeof(long long) * (i++), sizeof(long long), 1073741824/sizeof(long long), firstFile);
		fread(lbfirst2 + 1073741824/sizeof(long long) * i, sizeof(long long), fsize/3/sizeof(int) - 1073741824/sizeof(long long) * i, firstFile);
	}
//	fread(lbfirst3, sizeof(int), fsize/3/sizeof(int), firstFile);
	{
		long long i = 0;
		while (1073741824/sizeof(int) * (i+1) < fsize/3/sizeof(int))
			fread(lbfirst3 + 1073741824/sizeof(int) * (i++), sizeof(int), 1073741824/sizeof(int), firstFile);
		fread(lbfirst3 + 1073741824/sizeof(int) * i, sizeof(int), fsize/3/sizeof(int) - 1073741824/sizeof(int) * i, firstFile);
	}
	
	keyNode.resize(kn);
	
	for (int i = 0, x, y; i < kn; i++)
	{
		fscanf(fkeyNode, "%d%d", &x, &y);
		keyNode[x].resize(0);
		if (hash[x] == 0) 
		{	
			bfkn++;
			bfkm += y;
			keyNode[x].resize(y);
			for (int j = 0; j < y; j++)
				fscanf(fkeyNode, "%d", &keyNode[x][j]);
			sort(keyNode[x].begin(), keyNode[x].end());
			int yy = 1;
			for (int j = 1; j < y; j++)
				if (keyNode[x][j] != keyNode[x][j-1])
					keyNode[x][yy++] = keyNode[x][j];
			keyNode[x].resize(yy);
		}
		else 
		{
			tkn++;
			tkm += y;
			keyNode[x].resize(0);
			for (int j = 0; j < y; j++)
				fscanf(fkeyNode, "%d", &x);
		}
	}
	
	FILE * fnodeKey = fopen(nodeKeyName, "r");

	fscanf(fnodeKey, "%d%d", &kn, &km);
	nodeKey.resize(n);
	int cntn0 = 0;
	
	for (int i = 0, x, y; i < n; i++)
	{
		fscanf(fnodeKey, "%d%d", &x, &y);
		nodeKey[x].resize(0);
		for (int j = 0, z; j < y; j++)
		{
			fscanf(fnodeKey, "%d", &z);
			if (hash[z] > 0) nodeKey[x].push_back(z);
			
//			if ((hash[z] > 0) != (keyNode[z].size() > 0)) printf("%d %d %d error\n", z, hash[z], keyNode[z].size() );
		}
		sort(nodeKey[x].begin(), nodeKey[x].end());
		cntn0 += (nodeKey[x].size() == 0);
	}
//	printf("%d %d %d %d %d\n", nodeKey[21129].size(), nodeKey[21129][0], nodeKey[21129][1],  nodeKey[21129][2], nodeKey[21129][3]);
	
	fbsDis = (ihData **)malloc(n * sizeof(ihData *));
	for (int i = 0; i < n; i++)
		fbsDis[i] = NULL;
	tmpData.w = -1;
	
	
	printf("index loaded\n");	
	fclose(ktFile);
	fclose(firstFile);
	fclose(fnodeKey);
	fclose(fkeyNode);

/*		FILE * ftmp2 = fopen("ftmp1", "w");
	for (int i = 0; i <= n; i++)
		fprintf(ftmp2, "%lld\n", lbfirst1[i]);
	for (int i = 0, ii = fsize/3/sizeof(int); i < ii; i++)
		fprintf(ftmp2, "%lld\n", lbfirst2[i]);
	for (int i = 0, ii = fsize/3/sizeof(int); i < ii; i++)
		fprintf(ftmp2, "%d\n", lbfirst3[i]);
	fclose(ftmp2);
	exit(0);
*/

}

vector<vector<pair<int, wtype> > > g;

void testLoad()
{
	FILE * fg = fopen(txtName, "r");
	fscanf(fg, "%d", &n);
	g.resize(n);
	for (int i = 0; i < n; i++)
		g[i].resize(0);
	wtype z;
	for (int x, y; fscanf(fg, "%d%d%d", &x, &y, &z)!=EOF;)
	{
		g[x].push_back(make_pair(y,z));
	}
}

vector<wtype> dis;
vector<int> testH, testHpos;
int testCnt;
vector<pair<int, wtype> > testAns;

inline void testDown(int i)
{
//	printf("testDown %d\n", i);
	testH[0] = testH[i];
	for (int j; (j=(i<<1)) <= testCnt; i = j )
	{
//		printf("testDown %d ==\n", j);
		if (j < testCnt && dis[testH[j+1]] < dis[testH[j]] ) j++;
		if (dis[testH[0]] > dis[testH[j]])
		{
			testH[i] = testH[j];
			testHpos[testH[i]] = i;
		}
		else break;	
	}
	testH[i] = testH[0];
	testHpos[testH[i]] = i;
}

inline void testUp(int i)
{
	testH[0] = testH[i];
	for (int j; (j=(i>>1)) >= 1; i = j )
	{
		if (dis[testH[0]] < dis[testH[j]])
		{
			testH[i] = testH[j];
			testHpos[testH[i]] = i;
		}
		else break;	
	}
	testH[i] = testH[0];
	testHpos[testH[i]] = i;
}

vector<int> prep;
vector<bool> vv;

void testQuery(int x, int w, int k)
{
/*	for (int i = 1; i <= ansCnt; i++)
		if (queryDis(x, ansV[i]) != ansDis[i]) 
		{
			printf("dis error w k x y yes no KT? %d %d %d %d %f %f %d\n", w, k, x, ansV[i], queryDis(x, ansV[i]), ansDis[i], hash[w]>0);
			exit(0);
		}
*/	
//	return;
	vv.resize(0);
	vv.resize(n, 0);

	if (hash[w] == 0)
		for (int i = 0, ii = keyNode[w].size(); i < ii; i++)
			vv[keyNode[w][i]] = 1;
			
	prep.resize(n);
	testAns.resize(0);
	dis.resize(0);
	dis.resize(n, -1);
	testHpos.resize(n);
	testH.resize(n);
	dis[x] = 0;
	testH[1] = x;
	testCnt = 1;
	testHpos[x] = 1;
	int p;
	int tmp = ansCnt;
	
	while (testCnt > 0)
	{
		p = testH[1];
		testH[1] = testH[testCnt--];
		testHpos[testH[1]] = 1;
		testDown(1);

		if (testAns.size() < k)
		if (hash[w] > 0)
		{
			for (int i = 0, ii = nodeKey[p].size(); i < ii; i++)
				if (nodeKey[p][i] == w)
				{
					testAns.push_back(make_pair(p, dis[p]));
					break;
				}
		}
		else if (vv[p])	testAns.push_back(make_pair(p, dis[p]));
		
		for (int i = 1; i <= ansCnt; i++)
			if (ansV[i] == p)
			{
				if (ansDis[i] != dis[p])
				{
					printf("distance error x w k p yes no KT? %d %d %d %d %d %d %d\n", x, w, k, p, dis[p], ansDis[i], hash[w]>0);
					exit(0);				
				}
				tmp--;
			}	
//		printf("%d %d\n", testAns.size(), tmp);
			
		if (testAns.size() == k && tmp == 0) break;

		for (int i = 0, ii = g[p].size(); i < ii; i++)
			if (dis[g[p][i].first] < 0 || dis[g[p][i].first] > dis[p] + g[p][i].second)
			{
				prep[g[p][i].first] = p;
//				printf("%d\n")
				if (dis[g[p][i].first] < 0)
				{
					testH[++testCnt] = g[p][i].first;
					testHpos[g[p][i].first] = testCnt;
				}
				dis[g[p][i].first] = dis[p] + g[p][i].second;
				testUp(testHpos[g[p][i].first]);				
			}
	}
	
	
	int pre = 0, cc;

	if (testAns.size() != ansCnt) 
	{
		printf("size error yCnt nCnt\n", testAns.size(), ansCnt);
				FILE * f1 = fopen("f1", "w");
				for (int i = 0; i < testAns.size()-1; i++)
				{
					int xx = testAns[i].first;
					fprintf(f1, "%d\t%d\t%d\n", testAns[i].first, testAns[i].second, prep[testAns[i].first] );
//					for (int j = 0; j < nodeKey[testAns[i].first].size(); j++)						fprintf(f1, "%d ", nodeKey[testAns[i].first][j]);					fprintf(f1, "\n");
				}	
				//printf("==============\n");
				FILE * f2 = fopen("f2", "w");
				for (int i = 1; i <= ansCnt; i++)
					fprintf(f2, "%d\t%d\n", ansV[i], ansDis[i]);
				fclose(f1);
				fclose(f2);
				exit(0);
	}

	vector<float> tmpDis(ansCnt+1);
	for (int i = 0; i <= ansCnt; i++)
		tmpDis[i] = ansDis[i];

	sort(ansDis+1, ansDis + ansCnt+1);
	for (int i = 0; i < testAns.size(); i++)
		if (ansDis[i+1] != testAns[i].second )
		{
				printf("k error x w k yDis nDis KT? %d %d %d %d %d %d\n", x, w, k, testAns[i].second, ansDis[i+1], (hash[w]>0));
				FILE * f1 = fopen("f1", "w");
				for (int i = 0; i < testAns.size(); i++)
				{
					int xx = testAns[i].first;
					fprintf(f1, "%d\t%d\t%d\n", testAns[i].first, testAns[i].second, prep[testAns[i].first] );
//					for (int j = 0; j < nodeKey[testAns[i].first].size(); j++)						fprintf(f1, "%d ", nodeKey[testAns[i].first][j]);					fprintf(f1, "\n");
				}	
				//printf("==============\n");
				FILE * f2 = fopen("f2", "w");
				for (int i = 1; i <= ansCnt; i++)
					fprintf(f2, "%d\t%d\n", ansV[i], tmpDis[i]);
				fclose(f1);
				fclose(f2);
				exit(0);				
		}

	
}


void testOne(int x, int w, int k)
{
	query(x, w, k);
//	testQuery();
	exit(0);
	printf("\nx w k ansCnt %d %d %d %d\n", x, w, k, ansCnt);
	for (int i = 1; i <= ansCnt; i++)
		printf("%d %d\n", ansV[i], ansDis[i]);
	exit(0);
/*	printf("%d %d\n", labelout[outPos[x]].x, labelout[outPos[x]].w);
	for (long long i = outPos[x]; labelout[i].x != -1; i++)
		printf("%d %d %d\n", x, labelout[i].x, labelout[i].w);
*/	
/*	for (int j = 1; j <= ansCnt; j++)
		for (long long i = outPos[ansV[j]]; labelout[i].x != -1; i++)
			printf("%d %d %d\n", ansV[j], labelout[i].x, labelout[i].w);
*/				
//	exit(0);

}

int main(int argc, char ** argv)
{
	txtName = argv[1];
	int tKT, qtype;

	if (argc > 2) tKT = atoi(argv[2]);
	else tKT = 1556;
	

	degName = (char*)malloc(1+strlen(txtName) + 50);
	sprintf(degName, "%s.deg", txtName);	
	outName = (char*)malloc(1+strlen(txtName) + 50);
	sprintf(outName, "%s.DisLLlf_%d", txtName, tKT);
	keyNodeName = (char*) malloc(1+strlen(txtName) + 50);
	sprintf(keyNodeName, "%s.keyNode", txtName);
	nodeKeyName = (char*) malloc(1+strlen(txtName) + 50);
	sprintf(nodeKeyName, "%s.nodeKey", txtName);

	lbName = (char*) malloc(1+strlen(txtName) + 50);
	sprintf(lbName, "%s.DisLLlb_%d", txtName, tKT);
	ktName = (char*) malloc(1+strlen(txtName) + 50);
	sprintf(ktName, "%s.DisLLkt_%d", txtName, tKT);
	firstName = (char*) malloc(1+strlen(txtName) + 50);
	sprintf(firstName, "%s.DisLLfirst_%d", txtName, tKT);

	timer tm;
	
	loadIndex();
	

	vector<int> qx(queryCnt), qw(queryCnt);
		n = checkB(degName)/sizeof(edgeL);
		
		char * testcaseName = (char*) malloc(1+strlen(txtName) + 50);
		sprintf(testcaseName, "%s.testcase", txtName);

		printf("FS-FBS querying %s with %s\n", txtName, testcaseName);

		FILE * ftest = fopen(testcaseName, "r");
		fscanf(ftest, "%d", &queryCnt);
		qx.resize(queryCnt);
		qw.resize(queryCnt);
		int s1 = 0, s2 = 0;
		for (int i = 0 ; i < queryCnt; i++)
		{
			int x, y;
			double z;
			fscanf(ftest, "%d%d%d%d", &qx[i], &qw[i], &x, &y);
			s1 += (hash[qw[i]]>0);
			while (x--)
				fscanf(ftest, "%d%f", &y, &z);
		}
		fclose(ftest);
		
		for (int k = 1; k <= 128; k *= 2)
		{
			tm.restart();
			for (int i = 0; i < queryCnt; i++)
			{	
				query(qx[i], qw[i], k);		
			}
			printf("average top-%d query time %lf microsecond\n", k, tm.getTime()*1000000/queryCnt);
		}
		
	
	free(degName);
	free(outName);
	free(keyNodeName);
	free(nodeKeyName);
	free(kcntName);
	free(lbName);
	free(ktName);
	free(pos);
	free(nid);
	free(hash);
	free(kt);
	free(lb);
	free(outPos);
	free(firstName);
	free(lbfirst1);
	free(lbfirst2);
	free(lbfirst3);
	free(ran);
	
	return 0;
}














