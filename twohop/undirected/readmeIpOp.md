1. Format of input flarn.graph : 

#no. of nodes n
one edge per line in the format "s t w". 0 1 14188 is an undirected edge between node 0 and node 1 having weight 14188. 

* only integer weights are allowed
* only integer node ids are allowed
* node ids should be between 0(inclusive) and n-1 (inclusive)

2. Format of input flarn.rank : 

n lines. each line i containing one integer denoting the rank of node[i] obtained from CH. Ranks are unique between 0(inclusive) and n-1 (inclusive)

3. Format of output flarn.graph.labels (2-hop labelling on undirected graph) : 

NodeId noOfLabels
noOfLabels lines of the format "id, w" where <id, w> pair belongs to the 2-hop label index of Node[NodeId]

Eg, 
7123 2
13355 950
22376 1065
.
.
.

<13355, 950> and <22376, 1065> belong to the 2-hop index of node 7123. 13355 is the pivot nodeId and 950 is the shortest distance between node 7123 and node 13355. 

4. While reading and storing from flarn.graph.labels, store them in the datastructure Code/src/twohop/directed/Node.java in the following way : 

7123 2
13355 950
22376 1065

Store <13355, 950> tuple in "label" field of node[7123]
Store 7123 in the "reverseLabel" field of node[13355]


5. Also add self distance for every node. For every node[id], <id, 0> should be added to the label of node[id]

6. To query for shortest distance between s and t, use distance(s, t) function like implementation from Code/src/twohop/undirected/notNeeded/HopDBundirected.java 