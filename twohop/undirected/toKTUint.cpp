#include <cstring>
#include <cstdio>
#include "ioUint.h"
#include <algorithm>
#include "math.h"
#include <vector>
#include <map>
#include <set>
using namespace std;

typedef unsigned long long htype;


char * txtName, * degName, * outName, * ktName, * keyNodeName, * nodeKeyName, * kcntName, * lbName, *firstName, * lfName;

edge * lb;
int * oid; //to oid

long long m;
int n, tKT, kn, km, bfkn = 0, bfkm = 0, tkn = 0, tkm = 0;

vector<vector<int> > keyNode, nodeKey;

const int ranSize = 16384;
htype * kt, * hash, * ran, * nodeHash;

long long * pos;
long long * lbfirst1;
vector<long long> lbfirst2;
vector<int> lbfirst3;
vector<long long> lastW;

#define tmid(x,y) ( (x+y)>>1 )

vector<int> freq;

bool cmpord(int i, int j)
{
	return freq[i] > freq[j];
}

bool cmplf(const edgeS & i, const edgeS & j)
{
	return i.w < j.w;
}

void loadIndex()
{
	inBufS outLabel(outName);
	inBufL degBuf(degName);
	
	n = checkB(degName)/sizeof(edgeL);
	oid = (int*)malloc(n*sizeof(int));
	lb = (edge*)malloc((checkB(outName)/sizeof(edgeS))*sizeof(edge));

vector<int>  freqord, frank;
{
	FILE * fkeyNode = fopen(keyNodeName, "r");
	fscanf(fkeyNode, "%d%d", &kn, &km);
	freq.resize(kn);
	freqord.resize(kn);
	frank.resize(kn);
	for (int i = 0, x, y; i < kn; i++)
	{
		fscanf(fkeyNode, "%d%d", &x, &y);
		freq[x] = y;
		for (int j = 0; j < y; j++)
			fscanf(fkeyNode, "%d", &x);
	}
	
	for (int i = 0; i < kn; i++)	
		freqord[i] = i;
	sort(freqord.begin(), freqord.end(), cmpord);
	for (int i = 0; i < kn; i++)
		frank[freqord[i]] = i;
	
	fclose(fkeyNode);	
}	


	FILE * fkeyNode = fopen(keyNodeName, "r");
	fscanf(fkeyNode, "%d%d", &kn, &km);
	keyNode.resize(kn);
	
	hash = (htype *)malloc(sizeof(htype) * (long long)kn);
	ran = (htype *)malloc(sizeof(htype) * (long long)ranSize);

	for (int i = 0, x, y, z; i < kn; i++)
	{
		fscanf(fkeyNode, "%d%d", &x, &y);
		if (frank[x] >= tKT) 
		{	
			bfkn++;
			bfkm += y;
			keyNode[x].resize(y);
			for (int j = 0; j < y; j++)
				fscanf(fkeyNode, "%d", &keyNode[x][j]);
			hash[x] = 0;
		}
		else 
		{
			tkn++;
			tkm += y;
			keyNode[x].resize(0);
			for (int j = 0; j < y; j++)
				fscanf(fkeyNode, "%d", &z);
			hash[x] = ((htype)1) << (rand()%(8*sizeof(htype)));
		}
	}
	for (int i = 0; i < ranSize; i++)
		ran[i] = (((htype)1) << (rand()%(8*sizeof(htype))));
	
//	return;

	FILE * fnodeKey = fopen(nodeKeyName, "r");

	fscanf(fnodeKey, "%d%d", &kn, &km);
	nodeKey.resize(n);
	int cntn0 = 0;
	
	nodeHash = (htype *)malloc(sizeof(htype) * (long long)n);
	for (int i = 0, x, y; i < n; i++)
	{
		fscanf(fnodeKey, "%d%d", &x, &y);
		nodeKey[x].resize(0);
		nodeHash[x] = 0;
		for (int j = 0, z; j < y; j++)
		{
			fscanf(fnodeKey, "%d", &z);
			if (hash[z] > 0) 
			{
				nodeHash[x] |= ran[((x + 198911LL * z) % 1999993) & (ranSize - 1)];
				nodeKey[x].push_back(z);
			}
		}
		cntn0 += (nodeKey[x].size() == 0);
	}

	edgeL tmpL;
	degBuf.start();
	for (int i = 0; i < n; i++)
	{
		degBuf.nextEdge(tmpL);
		oid[(tmpL.x<<32)>>32] = i;		
	}
	
	long long mm = 0;
	m = 0;
	edgeS tmp, tmpPre;
	outLabel.start();
	outBufS lf(lfName);
	vector<edgeS> tmplf;
	for (int i = 0; i < n; i++)
	{
		tmplf.resize(0);
		tmpPre.x = -1;
		while (1)
		{
			outLabel.nextEdge(tmp);
//			if (tmp.x == 18270 && i == 21129) printf("----\n");	
			
			if (tmp.x == -1) break;
/*			if (tmp.x == tmpPre.x)
			{
				if (tmplf[tmplf.size()-1].w > tmpPre.w) tmplf[tmplf.size()-1].w = tmpPre.w;
				if (m >= 0 && lb[m-1].x == tmp.x && lb[m-1].y == oid[i] && lb[m-1].w > tmp.w) lb[m-1].w = tmp.w;
				continue;
			}
*/			
			tmplf.push_back(tmp);
			mm++;
			lb[m].x = tmp.x;
			lb[m].y = oid[i];
			lb[m++].w = tmp.w;
			if (nodeKey[oid[i]].size() == 0) m--;

		}
		sort(tmplf.begin(), tmplf.end(), cmplf);
		for (int j = 0, jj = tmplf.size(); j < jj; j++)
			lf.insert(tmplf[j]);
		lf.insert(tmp);

		lb[m].x = i;
		lb[m].y = oid[i];
		lb[m++].w = 0;
		
	}
	
}

void buildTS(long long x, long long y)
{
	if (y == x) return;
	int t = y - x, s;
	if (t % 2 == 0) t--;
	s = (t & (t+1));

	long long p;
	while (1)
	{
		kt[p = x + tmid(s,t)] = 0;
		
		for (long long i = x + s; i <= x + t; i++)
			for (int xx = 0, yy = nodeKey[lb[i].y].size(); xx < yy; xx++)
			{
//				if (nodeKey[lb[i].y][xx] == 1421) printf("%d %d %d %d\n", s, t, lastW[nodeKey[lb[i].y][xx]] > x + 2*t - s, lastW[nodeKey[lb[i].y][xx]] > y);
				if (lastW[nodeKey[lb[i].y][xx]] > x + 2*t - s || lastW[nodeKey[lb[i].y][xx]] > y)
					kt[p] |= ran[(nodeKey[lb[i].y][xx] ^ (s+t))&(ranSize-1)];
			}
		
		if (s == t-1) 
		{
			for (long long i = x + s; i <= x + t; i++)
				for (int xx = 0, yy = nodeKey[lb[i].y].size(); xx < yy; xx++)
					lastW[nodeKey[lb[i].y][xx]] = i;
		
			if (s == 0) return;
			t = s-1;
			s = (t&(t+1));
		}
		else s = tmid(s,t)+1;
		
	}
	
	
}


bool cmp(const edge & i, const edge & j)
{
	return i.x < j.x || i.x == j.x && i.w > j.w;
}



void buildKT()
{
	timer tm;
	sort(lb, lb + m, cmp);

//	lastW.resize(kn, 300147288);	buildTS(299072768, 300147288-1);	exit(0);
	
	kt = (htype*)malloc(m * sizeof(htype));
	pos = (long long*)malloc((n+1LL) * sizeof(long long));
	
	lbfirst1 = (long long *)malloc((n+1) * sizeof(long long));
	lbfirst2.resize(0);
	lbfirst3.resize(0);
	
	
	map<int, long long> lbwmp;
	
	long long j = 0, delCnt = 0, delOcc = 0, totOcc = 0;
	for (int i = 0; i < n; i++)
	{
		lbwmp.clear();

		pos[i] = j;
		lbfirst1[i] = lbfirst2.size();
		
		while (j < m && lb[j].x == i)
		{
			for (int k = 0, kk = nodeKey[lb[j].y].size(); k < kk; k++)
				lbwmp[nodeKey[lb[j].y][k]] = j;
			j++;
		}		
		for (map<int, long long>::iterator it = lbwmp.begin(); it != lbwmp.end(); it++)
		{
			lbfirst2.push_back(it->second);
			lbfirst3.push_back(it->first);
		}
		
	}
		
	lbfirst1[n] = lbfirst2.size();
	pos[n] = j;
	lastW.resize(kn, j);
	for (int i = n; i > 0; i--)
		buildTS(pos[i-1], pos[i]-1);
	
}




void writeIndex()
{
	outBufS lbBuf(lbName);
	
	edgeS tmp;
	for (long long j = 0; j < m; j++)
	{
		lbBuf.insert(lb[j].y, lb[j].w);
	}
	
		
	FILE * ktFile = fopen(ktName, "wb"); 
/*	FILE * tmp1 = feopen("tmp1", "w");
	for (int i = 0; i < kn; i++)
		fprintf("%d %u\n",tmp1, i, hash[i] );
	fclose(tmp1);
*/
	fwrite(ran, sizeof(htype), ranSize, ktFile);	
	fwrite(hash, sizeof(htype), kn, ktFile);
	fwrite(nodeHash, sizeof(htype), n, ktFile);
//	fwrite(kt, sizeof(htype), m, ktFile);
	{
		long long i = 0;
		while (1073741824/sizeof(htype) * (i+1) < m)
			fwrite(kt + 1073741824/sizeof(htype) * (i++), sizeof(htype), 1073741824/sizeof(htype), ktFile);
		fwrite(kt + 1073741824/sizeof(htype) * i, sizeof(htype), m - 1073741824/sizeof(htype) * i, ktFile);
	}
	fwrite(pos, sizeof(long long), n+1, ktFile);

	
	for (int i = 0; i < n; i++)
		if (pos[i] > pos[i+1]) 
		{
			printf("pos error %d %lld %lld\n", i, pos[i], pos[i+1]);
			exit(0);
		}
	
	FILE * firstFile = fopen(firstName, "wb");
	fwrite(lbfirst1, sizeof(long long), n+1, firstFile);
//	fwrite(&lbfirst2[0], sizeof(long long), lbfirst2.size(), firstFile);
	{
		long long i = 0;
		while (1073741824/sizeof(long long) * (i+1) < lbfirst2.size())
			fwrite(&lbfirst2[1073741824/sizeof(long long) * (i++)], sizeof(long long), 1073741824/sizeof(long long), firstFile);
		fwrite(&lbfirst2[1073741824/sizeof(long long) * i], sizeof(long long), lbfirst2.size() - 1073741824/sizeof(long long) * i, firstFile);
	}

//	fwrite(&lbfirst3[0], sizeof(int), lbfirst3.size(), firstFile);
	{
		long long i = 0;
		while (1073741824/sizeof(int) * (i+1) < lbfirst3.size())
			fwrite(&lbfirst3[1073741824/sizeof(int) * (i++)], sizeof(int), 1073741824/sizeof(int), firstFile);
		fwrite(&lbfirst3[1073741824/sizeof(int) * i], sizeof(int), lbfirst3.size() - 1073741824/sizeof(int) * i, firstFile);
	}
	
	
	fclose(ktFile);
	fclose(firstFile);
}



int main(int argc, char ** argv)
{
	txtName = argv[1];
	
	if (argc > 2) tKT = atoi(argv[2]);
	else tKT = 1556;

	printf("building FS-FBS index for %s\n", argv[1]);
	
	degName = (char*)malloc(1+strlen(txtName) + 50);
	sprintf(degName, "%s.deg", txtName);	
	outName = (char*)malloc(1+strlen(txtName) + 50);
	sprintf(outName, "%s.out", txtName);
	keyNodeName = (char*) malloc(1+strlen(txtName) + 50);
	sprintf(keyNodeName, "%s.keyNode", txtName);
	nodeKeyName = (char*) malloc(1+strlen(txtName) + 50);
	sprintf(nodeKeyName, "%s.nodeKey", txtName);

	lfName = (char*) malloc(1+strlen(txtName) + 50);
	sprintf(lfName, "%s.DisLLlf_%d", txtName, tKT);
	lbName = (char*) malloc(1+strlen(txtName) + 50);
	sprintf(lbName, "%s.DisLLlb_%d", txtName, tKT);
	ktName = (char*) malloc(1+strlen(txtName) + 50);
	sprintf(ktName, "%s.DisLLkt_%d", txtName, tKT);
	firstName = (char*) malloc(1+strlen(txtName) + 50);
	sprintf(firstName, "%s.DisLLfirst_%d", txtName, tKT);
	
	timer tm;
	loadIndex();
	buildKT();
	writeIndex();
	
	printf("FS-FBS index size %lf MB\n", (checkB(lbName) + checkB(ktName) + checkB(lfName) + checkB(firstName))/(1024*1024.0) );
	printf("FS-FBS indexing time %lf sec\n", tm.getTime());

	free(degName);
	free(outName);
	free(keyNodeName);
	free(nodeKeyName);
	free(kcntName);
	free(lbName);
	free(ktName);
	free(pos);
	free(oid);
	free(hash);
	free(kt);
	free(lb);
	free(lbfirst1);
	free(firstName);
	free(ran);

}





