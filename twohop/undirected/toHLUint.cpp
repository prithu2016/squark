#include <cstring>
#include <cstdio>
#include "ioUint.h"
#include <algorithm>
#include "math.h"
#include <vector>
#include <map>
using namespace std;

char * txtName, * degName, * outName, * nodeKeyName, * keyNodeName, * lowName, *highName, * lowPosTName, * lowPosVName, * highPosName, * testName;

edge * lb;
int * oid; //to oid

long long m, mL, mH;
int n, tHL, kn, km, fHL;

vector<vector<int> > nodeKey;

vector<vector<int> > keyNode;
long long * posH;
long long * posL3, *posL1;
int * posL2;

vector<edge> high, low;
vector<vector<edgeS> > lf;
vector<long long> highPos;
vector< vector<edge>::iterator > highNext;

vector<BTType> lowPosT;
BCType tmpBC[BCSize];
int tmpBCLen;

bool cmpHigh(const edge & i, const edge & j)
{
	return (i.x < j.x || i.x == j.x && i.w < j.w);
}

FILE * flowPosV;


void buildHL()
{
	
	outBuf highBuf(highName);
	outBufS lowBuf(lowName);
	flowPosV = fopen(lowPosVName, "wb");
	tmpBCLen = 0;
	
	
	long long highLen = 0, lowLen = 0;
	
	highPos.resize(kn+1);
	lowPosT.resize(0);

	edge tmp;
	highNext.resize(tHL);
	
	FILE * flowPosV = fopen(lowPosVName, "wb");
	long long lowPosVLen = 0;
	
	for (int i = 0; i < kn; i++)
//	for (int i = 362209; i < 362209+1; i++)
	{
		highPos[i] = highLen;
		high.resize(0);
		low.resize(0);
		for (vector<int>::iterator it = keyNode[i].begin(); it != keyNode[i].end(); it++)
		{
			for (vector<edgeS>::iterator jt = lf[*it].begin(); jt != lf[*it].end(); jt++)
			{
				tmp.y = *it;
				tmp.x = jt->x;
				tmp.w = jt->w;
				if (tmp.x < tHL) high.push_back(tmp);
				else low.push_back(tmp);
			}
		}
//		if (i == 362209) printf("--- %d %lld\n", high.size(), highPos[i]);
//exit(0);		
		highLen += high.size();
		sort(high.begin(), high.end(), cmpHigh);
		tmp.x = -1;
		high.push_back(tmp);
		vector<edge>::iterator j = high.begin();
		
		int highNextCnt = 0;
		
		for (int ii = 0; ii < tHL; ii++)
		{
			if (j->x != ii) continue;
			highNext[highNextCnt++] = j;
			while (j->x == ii) j++;
		}
		
		while (highNextCnt > 0)			
			for (int ii = 0; ii < highNextCnt; ii++)
			{
				highBuf.insert(*highNext[ii]);
				if ( highNext[ii]->x != (++highNext[ii])->x ) 
				{
					highNext[ii] = highNext[--highNextCnt];
					ii--;
					continue;
				}
			}
		
		sort(low.begin(), low.end(), cmpHigh);
		
		int prex = -1;
		for (vector<edge>::iterator it = low.begin(); it != low.end(); lowLen++)		
		{
			if (prex != it->x) 
			{
				if (tmpBCLen == 0)
				{
					lowPosT.push_back(BTType(i, it->x, lowPosVLen));
				}
				tmpBC[tmpBCLen].v = prex =it->x;
				tmpBC[tmpBCLen++].pos = lowLen;
				lowPosVLen++;
				if (tmpBCLen == BCSize)
				{
//					fwrite(tmpBC, sizeof(BCType), tmpBCLen, flowPosV);
					{
						long long i = 0;
						while (1073741824/sizeof(BCType) * (i+1) < tmpBCLen)
							fwrite(tmpBC + 1073741824/sizeof(BCType) * (i++), sizeof(BCType), 1073741824/sizeof(BCType), flowPosV);
						fwrite(tmpBC + 1073741824/sizeof(BCType) * i, sizeof(BCType), tmpBCLen - 1073741824/sizeof(BCType) * i, flowPosV);
					}
					tmpBCLen = 0;
				}
			}
			lowBuf.insert(it->y, it->w);	
			if ((++it) == low.end() || it->x != prex)
			{
				lowBuf.insert(-1, 0);
				lowLen++;
			}
		}
		
		if (tmpBCLen > 0)
		{
			tmpBC[tmpBCLen++].v = n+1;
			lowPosVLen++;
			if (tmpBCLen == BCSize)
			{
//				fwrite(tmpBC, sizeof(BCType), tmpBCLen, flowPosV);
				{
					long long i = 0;
					while (1073741824/sizeof(BCType) * (i+1) < tmpBCLen)
						fwrite(tmpBC + 1073741824/sizeof(BCType) * (i++), sizeof(BCType), 1073741824/sizeof(BCType), flowPosV);
					fwrite(tmpBC + 1073741824/sizeof(BCType) * i, sizeof(BCType), tmpBCLen - 1073741824/sizeof(BCType) * i, flowPosV);
				}
				tmpBCLen = 0;
			}			
		}
		
	}
	
	if (tmpBCLen > 0) 
	{
//		fwrite(tmpBC, sizeof(BCType), tmpBCLen, flowPosV);
		{
			long long i = 0;
			while (1073741824/sizeof(BCType) * (i+1) < tmpBCLen)
				fwrite(tmpBC + 1073741824/sizeof(BCType) * (i++), sizeof(BCType), 1073741824/sizeof(BCType), flowPosV);
			fwrite(tmpBC + 1073741824/sizeof(BCType) * i, sizeof(BCType), tmpBCLen - 1073741824/sizeof(BCType) * i, flowPosV);
		}
	}
	fclose(flowPosV);
	
	FILE * flowPosT = fopen(lowPosTName, "wb");
//	fwrite(&lowPosT[0], sizeof(BTType), lowPosT.size(), flowPosT);
	{
		long long i = 0;
		while (1073741824/sizeof(BTType) * (i+1) < lowPosT.size())
			fwrite(&lowPosT[1073741824/sizeof(BTType) * (i++)], sizeof(BTType), 1073741824/sizeof(BTType), flowPosT);
		fwrite(&lowPosT[1073741824/sizeof(BTType) * i], sizeof(BTType), lowPosT.size() - 1073741824/sizeof(BTType) * i, flowPosT);
	}
	highPos[kn] = highLen;	
	FILE * fhighPos = fopen(highPosName, "wb");
	fwrite(&highPos[0], sizeof(long long), kn+1, fhighPos);
	fclose(fhighPos);
	

	
}





void loadIndex()
{
	int c1 = 0, c2 = 0;
	FILE * fkeyNode = fopen(keyNodeName, "r");
	fscanf(fkeyNode, "%d%d", &kn, &km);
	keyNode.resize(kn);
	for (int i = 0, x, y, yy; i < kn; i++)
	{
		fscanf(fkeyNode, "%d%d", &x, &y);
		keyNode[x].resize(y);
		if (y == 0) continue;
		for (int j = 0; j < y; j++)
			fscanf(fkeyNode, "%d", &keyNode[x][j]);
		sort(keyNode[x].begin(), keyNode[x].end());
		yy = 1;
		for (int j = 1; j < y; j++)
			if (keyNode[x][j] != keyNode[x][j-1]) keyNode[x][yy++] = keyNode[x][j];
		keyNode[x].resize(yy);
		c1 += keyNode[x].size();		
	}
	
	n = checkB(degName)/sizeof(edgeL);
	tHL = n/fHL;
	

	FILE * fnodeKey = fopen(nodeKeyName, "r");
	fscanf(fnodeKey, "%d%d", &kn, &km);
	nodeKey.resize(n);
	for (int i = 0, x, y, yy; i < n; i++)
	{
		fscanf(fnodeKey, "%d%d", &x, &y);
		nodeKey[x].resize(y);
		if (y == 0) continue;
		for (int j = 0, z; j < y; j++)
			fscanf(fnodeKey, "%d", &nodeKey[x][j]);
		sort(nodeKey[x].begin(), nodeKey[x].end());
		yy = 1;
		for (int j = 1; j < y; j++)
			if (nodeKey[x][j] != nodeKey[x][j-1]) nodeKey[x][yy++] = nodeKey[x][j];
		nodeKey[x].resize(yy);
		c2 += nodeKey[x].size();
	}
	fclose(fnodeKey);


	vector<int> oid(n);

	inBufL degBuf(degName);
	degBuf.start();
	edgeL tmpL;
	for (int i = 0; i < n; i++)
	{
		degBuf.nextEdge(tmpL);
		oid[(tmpL.x<<32)>>32] = i;		
	}
	
	long long docL = 0, docHigh = 0, docLow = 0;
	
	inBufS outLabel(outName);
	outLabel.start();
	edgeS tmpS;
	lf.resize(n);
	for (int i = 0, oi; i < n; i++)
	{
		oi = oid[i];
		lf[oi].resize(1);
		lf[oi][0].x = i;
		lf[oi][0].w = 0;
		while (1)
		{
			outLabel.nextEdge(tmpS);
			if (tmpS.x == -1) break;
			lf[oi].push_back(tmpS);	
			docL += nodeKey[oi].size();
			if (tmpS.x < tHL) docHigh += nodeKey[oi].size();
			else docLow += nodeKey[oi].size();
		}
	}
	
//	int oi = 996633;
//	for (int i = 0; i < lf[oi].size(); i++)
//		printf("%d %d %d\n", oi, lf[oi][i].x, lf[oi][i].w);
//	exit(0);
	
	
	
}



int main(int argc, char ** argv)
{
	txtName = argv[1];
	if (argc > 2) fHL = atoi(argv[2]);
	else fHL = 100;

	printf("building High-Low index for %s\n", argv[1]);
	
	degName = (char*)malloc(1+strlen(txtName) + 50);
	sprintf(degName, "%s.deg", txtName);	
	outName = (char*)malloc(1+strlen(txtName) + 50);
	sprintf(outName, "%s.out", txtName);
	keyNodeName = (char*) malloc(1+strlen(txtName) + 50);
	sprintf(keyNodeName, "%s.keyNode", txtName);
	nodeKeyName = (char*) malloc(1+strlen(txtName) + 50);
	sprintf(nodeKeyName, "%s.nodeKey", txtName);

	highName = (char*) malloc(1+strlen(txtName) + 50);
	sprintf(highName, "%s.high%d_B", txtName, fHL);
	highPosName = (char*) malloc(1+strlen(txtName) + 50);
	sprintf(highPosName, "%s.highpos%d_B", txtName, fHL);
	lowName = (char*) malloc(1+strlen(txtName) + 50);
	sprintf(lowName, "%s.low%d_B", txtName, fHL);
	lowPosTName = (char*) malloc(1+strlen(txtName) + 50);
	sprintf(lowPosTName, "%s.lowpost%d_B", txtName, fHL);
	lowPosVName = (char*) malloc(1+strlen(txtName) + 50);
	sprintf(lowPosVName, "%s.lowposv%d_B", txtName, fHL);
	
	timer tm;
	loadIndex();
	
	buildHL();

	printf("High-Low index size %lf MB\n", (checkB(highName) + checkB(highPosName) + checkB(lowName) + checkB(lowPosTName) + checkB(lowPosVName))/(1024*1024.0) );
	printf("High-Low indexing time %lf sec\n", tm.getTime());

	return 0;

}




