To generate CH ranking of nodes : 

1. Boost library for c++ is needed to run CH

2. For input graph format, refer docu/ddsgDocu.html. graphSerialUndirected.txt is one example. 

3. Generate graphSerialUndirected.txt with the above format. Use Code/data/ReadData.java/addDirection() function to generate the same

4. Run make
5. Run ./main -s -p -f graphSerialUndirected.txt -o chOrderingUndirected.txt -C chContractionUndirected.txt -x 190 -y 1 -e 600 -p 1000 -k 1,3.3,2,10,3,10,5

6. Keep the numeric parameters in the above command unchanged. 

7. Format of the output chOrderingUndirected.txt and chContractionUndirected.txt can be found in docu/chDocu.html. For building the 2-hop index on undirected graph, we require chOrderingUndirected.txt which contains n lines, each line i specifying the rank of node[i]. Use the file chOrderingUndirected.txt as flarn.graph.rank Code/src/twohop/undirected folder. 

8. For directed graph, generate the graphSerialDirected.txt accordingly by following docu/ddsgDocu.html

9. Run same commands

10. For building the 2-hop index on directed graph, we require chContractionDirected.txt. Use the file chContractionDirected.txt in Code/src/twohop/directed folder. 

