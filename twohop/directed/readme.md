The implementation is based on "Hop Doubling Label Indexing for Point to Point Distance Querying on ScaleFree Networks" paper using "Contraction Hierarchies: Faster and Simpler Hierarchical Routing in Road Networks"

Steps to run : 
1. Follow instructions in Code/src/twohop/contraction-hierarchies-20090221/readmePranali to generate chOrderingDirected.txt and chContractionDirected.txt

2. Run Main.java by input-ing appropriate CH file.

3. city.labels.directed will be generated in Data/city folder. Format : 

Node current_id
### IN LABELS ###
<id, w> pairs where each pair tells that there is a shortest path from id to current_id of length w. Store them in labelIn of node[curr_id] from Node.java

### REVERSE IN LABELS ###
single integer id, representing that curr_id is in the labelIn of node[id]. Store id in reverseLabelIn of node[curr_id]

### OUT LABELS ###
<id, w> pairs where each pair tells that there is a shortest path from current_id and id of length w. Store them in labelOut of node[curr_id] from Node.java

### REVERSE OUT LABELS ###
single integer id, representing that curr_id is in the labelOut of node[id]. Store id in reverseLabelOut of node[curr_id]

*******************
.
.
.

Eg, 

0
### IN LABELS ###
0 0
### REVERSE IN LABELS ###
0
### OUT LABELS ###
58840 500
0 0
1 106
2 15
58783 98
3 126
163029 503
4 81
163031 517
163030 513
180398 534
58839 442
395 534
164981 267
164977 273
164976 233
164979 292
164978 297
390 316
389 281
388 333
206398 582
163237 233
371 238
356 267
357 141
358 195
838 476
57708 201
59338 334
365 196
58731 347
58728 382
362 81
58732 333
58785 163
### REVERSE OUT LABELS ###
0

4. To query for shortest distance between s and t, use distance(s, t) function like implementation from HopDB.java 