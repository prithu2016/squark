package twohop;

import java.util.ArrayList;

public class Path {
	ArrayList<Integer> seq;
	
	public Path () {
		seq = new ArrayList<>();
	}
	
	public Path (ArrayList<Integer> inputSeq) {
		seq = inputSeq;
	}
	
	public void addNode (int n1) {
		seq.add(n1);
	}
	
	public Path shallowCopy() {
		Path p1 = new Path();
		p1.seq.addAll(seq);
		return p1;
	}
	
}
