package twohop;

import java.util.*;

class DistanceComparator implements Comparator<Node> {
	public int compare(Node n1, Node n2) {
		if ( n1.dist > n2.dist)
			return 1;
		else if (n1.dist < n2.dist)
			return 1;
		return 0;
	}
}

/*class DegreeComparator implements Comparator<Node> {
	public int compare(Node n1, Node n2) {
		return n1.degree - n2.degree;
	}
}*/

class DecreaseRankComparator implements Comparator<Node> {
	public int compare(Node n1, Node n2) {
		return n2.rank - n1.rank;
	}
}

public class Node {
	int id;
	HashMap<Integer, Path> shortestPaths; //from me as the source
	double dist;
	int parent;
	int rank;
	int degree;
	
	
	//for directed
	HashMap<Integer, Integer> labelOut = new HashMap<Integer, Integer>();
	HashMap<Integer, Integer> labelIn = new HashMap<Integer, Integer>();
	HashSet<Integer> reverseLabelIn = new HashSet<>(); //some has in from me
	HashSet<Integer> reverseLabelOut = new HashSet<>(); //some has out on me
	
	//for undirected
	HashMap<Integer, Integer> label = new HashMap<Integer, Integer>();
	HashSet<Integer> reverseLabel = new HashSet<>(); //some one has
	
	ArrayList<Integer> removeIn = new ArrayList<>();
	ArrayList<Integer> removeOut = new ArrayList<>();
	
	public Node shallowCopy() {
		Node n1 = new Node();
		n1.id = id;
		n1.shortestPaths.putAll(shortestPaths);
		n1.dist = dist;
		n1.rank = rank;
		n1.parent = parent;
		n1.labelIn.putAll(labelIn);
		n1.labelOut.putAll(labelOut);
		n1.reverseLabelIn.addAll(reverseLabelIn);
		n1.reverseLabelOut.addAll(reverseLabelOut);
		n1.degree = degree;
		n1.label.putAll(label);
		n1.reverseLabel.addAll(reverseLabel);
		return n1;
	}
	
	public Node() {
		shortestPaths = new HashMap<Integer, Path>();
		this.clean();
	}
	
	public Node(int inputid) {
		id = inputid;
		shortestPaths = new HashMap<Integer, Path>();
		this.clean();
	}
	
	public void clean() {
		dist = Double.MAX_VALUE;
		parent = -1;
	}
}
