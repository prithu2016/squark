package twohop;

public class Main {

	public static void main(String[] args) throws Exception {
		long startTime = System.currentTimeMillis();
		HopDB hdb = new HopDB();
		hdb.readGraph("/Users/chendu/Desktop/DDP/New/Data/london/chContractionDirected.txt");
		System.out.println("Done reading");
		hdb.init();
		hdb.labelGeneration();
		System.out.println("Done generation");
		hdb.printLabels("/Users/chendu/Desktop/DDP/New/Data/london/london.labels.directed");
		System.out.println("Done printing labels to file");
		//hdb.test();  //only for dublin
		//System.out.println("Done testing");
		long endTime = System.currentTimeMillis();
		System.out.println((endTime - startTime) + " ms");
	}

}
