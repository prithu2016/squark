package twohop;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

public class HopDB {

	int n;
	LinkedHashMap<Integer, Node> allNodes;
	ArrayList<ArrayList<Integer>> ad; //adjacency matrix
	ArrayList<ArrayList<Integer>> weights; //weight matrix
	ArrayList<Node> nodes;
	int total_count = 0;
	
	public HopDB () {
		ad = new ArrayList<>();
		weights = new ArrayList<>();
		allNodes = new LinkedHashMap<>();
		nodes = new ArrayList<>();
	}
	
	public void readGraph (String file) throws Exception {
		BufferedReader bf = new BufferedReader(new FileReader(file));
		String line = bf.readLine(); //#Nodes and #Edges
		String splits[] = line.split(" ");
		n = Integer.parseInt(splits[0]);
		int eold = Integer.parseInt(splits[1]);
		int enew = Integer.parseInt(splits[2]);
		
		
		for (int i = 0; i < n; i++) {
			Node n1 = new Node();
			n1.id =i;
			n1.rank = Integer.parseInt(bf.readLine());
			nodes.add(n1);
			ad.add(new ArrayList<Integer>());
			weights.add(new ArrayList<Integer>());
			nodes.get(i).labelIn.put(i, 0);
			nodes.get(i).labelOut.put(i, 0);
			nodes.get(i).reverseLabelIn.add(i);
			nodes.get(i).reverseLabelOut.add(i);
		}
		line = bf.readLine();
		int ecount = 0;
		for (int i = 0; i < eold; i++) {
			line = bf.readLine();
			if (line == null)
				break;
			String[] endPoints = line.split(" ");
			int u = Integer.parseInt(endPoints[0]);
			int v = Integer.parseInt(endPoints[1]);
			int weight = Integer.parseInt(endPoints[2]);
			int flag = Integer.parseInt(endPoints[3]);
			if (flag == 1 || flag == 3) {
				ad.get(u).add(v);
				weights.get(u).add(weight);
				ecount++;
			}
			if (flag == 2 || flag == 3) {
				ad.get(v).add(u);
				weights.get(v).add(weight);
				ecount++;
			}
			if (flag != 1 && flag != 2 && flag != 3) {
				System.out.println("Check flags");
			}
		}
		
		for (int i = 0; i < enew; i++) {
			line = bf.readLine();
			if (line == null)
				break;
			String[] endPoints = line.split(" ");
			int u = Integer.parseInt(endPoints[0]);
			int v = Integer.parseInt(endPoints[1]);
			int weight = Integer.parseInt(endPoints[2]);
			int flag = Integer.parseInt(endPoints[3]);
			if (flag == 7 || flag == 5) {
				ad.get(u).add(v);
				weights.get(u).add(weight);
				ecount++;
			}
			if (flag == 7 || flag == 6) {
				ad.get(v).add(u);
				weights.get(v).add(weight);
				ecount++;
			}
			if (flag != 7 && flag != 5 && flag != 6) {
				System.out.println("Check flags");
			}
		}
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < ad.get(i).size(); j++) {
				if (nodes.get(i).rank < nodes.get(ad.get(i).get(j)).rank) {
					nodes.get(i).labelOut.put(ad.get(i).get(j), weights.get(i).get(j));
					if (!nodes.get(ad.get(i).get(j)).reverseLabelOut.contains(i))
						nodes.get(ad.get(i).get(j)).reverseLabelOut.add(i);

				}
				else if (nodes.get(i).rank > nodes.get(ad.get(i).get(j)).rank) {
					nodes.get(ad.get(i).get(j)).labelIn.put(i, weights.get(i).get(j));
					if (!nodes.get(i).reverseLabelIn.contains(ad.get(i).get(j)))
						nodes.get(i).reverseLabelIn.add(ad.get(i).get(j));
				}
			}
		}
	}
	
	
	public void init () {
		Collections.sort(nodes, new DecreaseRankComparator());
		for (int i = 0; i < n; i++) {
			allNodes.put(nodes.get(i).id, nodes.get(i).shallowCopy());
		}
	}
	
	public void labelGeneration () {
		LinkedHashMap<Integer, Node>  currNodes = new LinkedHashMap<Integer, Node>();
		for (int i = 0; i < n ; i++) {
			currNodes.put(nodes.get(i).id, nodes.get(i).shallowCopy());
		}

		while (true) {
			HashMap<Integer, Node>  newCurrNodes = new HashMap<Integer, Node>();
			Iterator it_curr = currNodes.entrySet().iterator();
			while (it_curr.hasNext()) {
				Map.Entry<Integer, Node> entry = (Entry<Integer, Node>) it_curr.next();
				Node curr_node = entry.getValue();
				rule1 (curr_node, newCurrNodes);
				rule2 (curr_node, newCurrNodes);
				rule4 (curr_node, newCurrNodes);
				rule5 (curr_node, newCurrNodes);
			}
			if (newCurrNodes.size() == 0)
				break;
			
			//adding new ones to allNodes
			boolean ans = addToAllNodes (newCurrNodes, currNodes);
		}
	}
	
	public boolean rule1 (Node curr, HashMap<Integer, Node>newCurrNodes) {
		int u, v, u1;
		u1 = -1;
		int d = 0;
		int d1 = 0;
		u = curr.id;
		Iterator it = curr.labelOut.entrySet().iterator();
		boolean ans = false;
		while (it.hasNext()) {
			Map.Entry<Integer, Integer> pair = (Map.Entry<Integer, Integer>) it.next();
			v = pair.getKey();
			d = pair.getValue();
			Node curr2 = allNodes.get(u);
			Iterator it2 = curr2.labelIn.entrySet().iterator();
			while (it2.hasNext()) {
				Map.Entry<Integer, Integer> pair2 = (Map.Entry<Integer, Integer>) it2.next();
				u1 = pair2.getKey();
				d1 = pair2.getValue();
				if ( allNodes.get(v).rank > allNodes.get(u1).rank 
						&& allNodes.get(u1).rank > allNodes.get(u).rank
						&& 
						(	newCurrNodes.get(u1) == null || newCurrNodes.get(u1).labelOut.get(v) == null ||
							newCurrNodes.get(u1).labelOut.get(v) > (d+d1)
						)
						&&
						(
							allNodes.get(u1) == null || allNodes.get(u1).labelOut.get(v) == null ||
							distance(u1, v) > (d+d1)
						)
						
				) {
					Node newU1 = new Node();
					if (newCurrNodes.containsKey(u1)) {
						newU1 = newCurrNodes.get(u1);
					}
					Node reverseNewU1 = new Node ();
					if (newCurrNodes.containsKey(v)) {
						reverseNewU1 = newCurrNodes.get(v);
					}
					
					newU1.id = u1;
					newU1.labelOut.put(v, d + d1);
					
					reverseNewU1.id = v;
					if (!reverseNewU1.reverseLabelOut.contains(u1))
						reverseNewU1.reverseLabelOut.add(u1);
					
					newCurrNodes.put(u1, newU1);
					newCurrNodes.put(v, reverseNewU1);
					//return true;
					ans = true;
					//System.out.println("Added by rule 1. Size : " + newCurrNodes.size());
				}
			}
		}
		//return false;
		return ans;
	}
	
	public boolean rule2 (Node curr, HashMap<Integer, Node> newCurrNodes) {
		int u, v, u2;
		int d = 0;
		int d2 = 0;
		u = curr.id;
		Iterator it = curr.labelOut.entrySet().iterator();
		boolean ans = false;
		while (it.hasNext()) {
			Map.Entry<Integer, Integer> pair = (Map.Entry<Integer, Integer>) it.next();
			v = pair.getKey();
			d = pair.getValue();
			Iterator reverseiterator = allNodes.get(u).reverseLabelOut.iterator();
			while (reverseiterator.hasNext()) {
				Node curr2 =  allNodes.get((int) reverseiterator.next());
				u2 = curr2.id;
				if (!curr2.labelOut.containsKey(u) ) {
					System.out.println("Reverse label wrong");
				}
				if (
					(
						newCurrNodes.get(u2) == null || newCurrNodes.get(u2).labelOut.get(v) == null ||
						newCurrNodes.get(u2).labelOut.get(v) > (d+d2)
					)
					&&
					(
						allNodes.get(u2) == null || allNodes.get(u2).labelOut.get(v) == null ||
						distance(u2, v) > (d+d2)
					)
				) {
					d2 = curr2.labelOut.get(u);
					Node newU1 = new Node();
					if (newCurrNodes.containsKey(u2)) {
						newU1 = newCurrNodes.get(u2);
					}
					
					Node reverseNewU1 = new Node ();
					if (newCurrNodes.containsKey(v)) {
						reverseNewU1 = newCurrNodes.get(v);
					}
					
					newU1.id = u2;
					newU1.labelOut.put(v, d + d2);
					
					reverseNewU1.id = v;
					if (!reverseNewU1.reverseLabelOut.contains(u2))
						reverseNewU1.reverseLabelOut.add(u2);
					
					newCurrNodes.put(u2, newU1);
					newCurrNodes.put(v, reverseNewU1);
					ans = true;
				}
			}
		}
		return ans;
	}
	
	public boolean rule4 (Node curr, HashMap<Integer, Node> newCurrNodes) {
		int u, v, u4;
		int d = 0;
		int d4 = 0;
		v = curr.id;
		Iterator it = curr.labelIn.entrySet().iterator();
		boolean ans = false;
		while (it.hasNext()) {
			Map.Entry<Integer, Integer> pair = (Map.Entry<Integer, Integer>) it.next();
			u = pair.getKey();
			d = pair.getValue();
			Node curr2 = allNodes.get(v);
			Iterator it2 = curr2.labelOut.entrySet().iterator();
			while (it2.hasNext()) {
				Map.Entry<Integer, Integer> pair2 = (Map.Entry<Integer, Integer>) it2.next();
				u4 = pair2.getKey();
				d4 = pair2.getValue();
				if ( allNodes.get(u).rank > allNodes.get(u4).rank 
						&& allNodes.get(u4).rank > allNodes.get(v).rank
						&& 
						(
							newCurrNodes.get(u4) == null || newCurrNodes.get(u4).labelIn.get(u) == null ||
							newCurrNodes.get(u4).labelIn.get(u) > (d+d4)
						)
						&&
						(
							allNodes.get(u4) == null || allNodes.get(u4).labelIn.get(u) == null ||
							distance(u, u4) > (d+d4)
						)
				) {
					{
						Node newU1 = new Node();
						if (newCurrNodes.containsKey(u4)) {
							newU1 = newCurrNodes.get(u4);
						}
						
						Node reverseNewU1 = new Node ();
						if (newCurrNodes.containsKey(u)) {
							reverseNewU1 = newCurrNodes.get(u);
						}
						
						newU1.id = u4;
						newU1.labelIn.put(u, d + d4);
						
						reverseNewU1.id = u;
						if (!reverseNewU1.reverseLabelIn.contains(u4))
							reverseNewU1.reverseLabelIn.add(u4);
						newCurrNodes.put(u4, newU1);
						newCurrNodes.put(u, reverseNewU1);
						ans = true;
					}
				}
			}
		}
		return ans;
	}
	
	public boolean rule5 (Node curr, HashMap<Integer, Node> newCurrNodes) {
		int u, v, u5;
		int d = 0;
		int d5 = 0;
		v = curr.id;
		Iterator it = curr.labelIn.entrySet().iterator();
		boolean ans = false;
		while (it.hasNext()) {
			Map.Entry<Integer, Integer> pair = (Map.Entry<Integer, Integer>) it.next();
			u = pair.getKey();
			d = pair.getValue();
			Iterator reverseiterator = allNodes.get(v).reverseLabelIn.iterator();
			while (reverseiterator.hasNext()) {
				int curr_it = (int) reverseiterator.next();
				Node curr2 = allNodes.get(curr_it);
				u5 = curr2.id;
				d5 = curr2.labelIn.get(v);
				if (!curr2.labelIn.containsKey(v) ) {
					System.out.println("Reverse label wrong");
				}
				
				if (
						(	
							newCurrNodes.get(u5) == null || newCurrNodes.get(u5).labelIn.get(u) == null ||
							newCurrNodes.get(u5).labelIn.get(u) > (d+d5)
						)
						&& 
						(
							allNodes.get(u5) == null || allNodes.get(u5).labelIn.get(u) == null ||
							distance(u, u5) > (d + d5)
						)
					) {
					
					Node newU1 = new Node();
					if (newCurrNodes.containsKey(u5)) {
						newU1 = newCurrNodes.get(u5);
					}
					
					Node reverseNewU1 = new Node ();
					if (newCurrNodes.containsKey(u)) {
						reverseNewU1 = newCurrNodes.get(u);
					}
					
					newU1.id = u5;
					newU1.labelIn.put(u, d + d5);
					
					reverseNewU1.id = u;
					if (!reverseNewU1.reverseLabelIn.contains(u5))
						reverseNewU1.reverseLabelIn.add(u5);
					
					newCurrNodes.put(u5, newU1);
					newCurrNodes.put(u, reverseNewU1);
					ans = true;
				}
			}
		}
		return ans;
	}

	public boolean addToAllNodes (HashMap<Integer, Node> newCurrNodes, HashMap<Integer, Node> currNodes) {
		boolean flag = true;
		Iterator itt = newCurrNodes.entrySet().iterator();
		while (itt.hasNext()) {
			Map.Entry<Integer, Node> entry = (Entry<Integer, Node>) itt.next();
			Node n1 = entry.getValue();
			int id = entry.getKey();
			Iterator it = n1.labelIn.entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry<Integer, Integer> pair = (Entry<Integer, Integer>) it.next();
				if (allNodes.get(id).labelIn.containsKey(pair.getKey())) {
					if (allNodes.get(id).labelIn.get(pair.getKey()) > pair.getValue()) {
						allNodes.get(id).labelIn.put(pair.getKey(), pair.getValue());
						allNodes.get(pair.getKey()).reverseLabelIn.add(id);
						flag = false;
					}
				}
				else {
					allNodes.get(id).labelIn.put(pair.getKey(), pair.getValue());
					allNodes.get(pair.getKey()).reverseLabelIn.add(id);
					flag = false;
				}
			}
			it = n1.labelOut.entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry<Integer, Integer> pair = (Entry<Integer, Integer>) it.next();
				
				if (allNodes.get(id).labelOut.containsKey(pair.getKey())) {
					if (allNodes.get(id).labelOut.get(pair.getKey()) > pair.getValue()) {
						allNodes.get(id).labelOut.put(pair.getKey(), pair.getValue());
						allNodes.get(pair.getKey()).reverseLabelOut.add(id);
						flag = false;
					}
				}
				else {
					allNodes.get(id).labelOut.put(pair.getKey(), pair.getValue());
					allNodes.get(pair.getKey()).reverseLabelOut.add(id);
					flag = false;
				}
			}
		}
		currNodes.clear();
		currNodes.putAll(newCurrNodes);
		return flag;
	}
	
	public void printNode() {
		//printhandler(678);
		//printhandler(679);
		//printhandler(682);
		//printhandler(932);
		/*printhandler(32282);*/
	}
	
	public void printhandler(int i) {
		System.out.println("Id : " + i + " Rank : " + allNodes.get(i).rank);
		System.out.println("Neighbors : " + ad.get(i));
		System.out.println("In label of " + allNodes.get(i).id);
		System.out.println(allNodes.get(i).labelIn);
		System.out.println("Out label of " + allNodes.get(i).id);
		System.out.println(allNodes.get(i).labelOut);
		System.out.println("ReverseIn label of " + allNodes.get(i).id);
		System.out.println(allNodes.get(i).reverseLabelIn);
		System.out.println("ReverseOut label of " + allNodes.get(i).id);
		System.out.println(allNodes.get(i).reverseLabelOut);
		System.out.println("********************");
	}
	
	public void test() throws Exception {
		BufferedReader bf = new BufferedReader(new FileReader("/Users/chendu/Desktop/DDP/New/Data/dublin/shortestPaths.txt"));
		int s = Integer.parseInt(bf.readLine());
		while (true) {
			String line = bf.readLine();
			if (line == null) {
				break;
			}
			if (line.contains("*")) {
				String temp = bf.readLine();
				if (temp == null) {
					break;
				}
				s = Integer.parseInt(temp);
			}
			else {
				String[] splits = line.split(" ");
				int d = Integer.parseInt(splits[0]);
				double dist = Double.parseDouble(splits[1]);
				int twohopdist = distance (s, d);
				if (!(twohopdist < Integer.MAX_VALUE)) {
					twohopdist = distance (s, d);
					System.out.println("Stored : " + dist + " 2hop : " + twohopdist);
					break;
				}
			}
		}
	}
	
	public int distance (int source, int destination) {
		
		int ans = Integer.MAX_VALUE;
		Iterator it = allNodes.get(source).labelOut.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<Integer, Integer> pair = (Entry<Integer, Integer>) it.next();
			if (allNodes.get(destination).labelIn.containsKey(pair.getKey())) {
				int dist = allNodes.get(destination).labelIn.get(pair.getKey()) + pair.getValue();
				if (dist < ans)
					ans = dist;
			}
		}
		if (ans == Integer.MAX_VALUE) {
			Iterator reverseiterator = allNodes.get(source).reverseLabelIn.iterator();
			while (reverseiterator.hasNext()) {
				int id = (int) reverseiterator.next();
				if (allNodes.get(id).labelOut.containsKey(destination)) {
					int dist = allNodes.get(id).labelIn.get(source) + allNodes.get(id).labelOut.get(destination);
					if (dist < ans)
						ans = dist;
				}
			}
		}
		return ans;
	}

	public void printLabels(String file) throws Exception {
		PrintWriter pow = new PrintWriter(new FileWriter(file));
		for (int i = 0; i < n; i++) {
			pow.println(allNodes.get(i).id);
			pow.println("### IN LABELS ###");
			Iterator it = allNodes.get(i).labelIn.entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry<Integer, Integer> pair = (Entry<Integer, Integer>) it.next();
				pow.println(pair.getKey() + " " + pair.getValue());
			}
			pow.println("### REVERSE IN LABELS ###");
			it = allNodes.get(i).reverseLabelIn.iterator();
			while (it.hasNext()) {
				int pair =  (int) it.next();
				pow.println(pair);
			}
			pow.println("### OUT LABELS ###");
			it = allNodes.get(i).labelOut.entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry<Integer, Integer> pair = (Entry<Integer, Integer>) it.next();
				pow.println(pair.getKey() + " " + pair.getValue());
			}
			pow.println("### REVERSE OUT LABELS ###");
			it = allNodes.get(i).reverseLabelOut.iterator();
			while (it.hasNext()) {
				int pair =  (int) it.next();
				pow.println(pair);
			}
			pow.println("************************");
		}
		pow.close();
	}
}
