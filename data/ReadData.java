package data;
import java.io.*;
import java.util.*;
import java.util.Map.Entry;

import algorithm.Edge;
import algorithm.Graph;
import algorithm.Node;
import algorithm.Pair;
import algorithm.Parameters;

import javax.print.attribute.HashAttributeSet;

class closest {
	double dist;
	int id;
}

class closest_comparator implements Comparator<closest> {
	public int compare (closest c1, closest c2) {
		if (c1.dist < c2.dist)
			return -1;
		if (c1.dist > c2.dist)
			return 1;
		return 0;
	}
}

public class ReadData {
	int count_nodes;
	
	public static void main(String[] args)throws Exception {
		ReadData r1 = new ReadData();
		//r1.ReadNetwork();
		//r1.ReadObject();
		//r1.find_closest();
		//r1.Serialize();
		//r1.assignKeyword();
		//r1.cleanNodes();
		r1.addDirection();
		//r1.generateGraph();
		//r1.printCoordinatesSerial();
		//r1.serialiseKeywords();
		//r1.readMetisPartition();
	}

	public void printCoordinatesSerial() throws Exception {
		ArrayList<Integer> sizes = new ArrayList<Integer>();
		//String size = "";
		sizes.add(5000);
		sizes.add(10000);
		sizes.add(15000);
		Graph graph = new Graph();
		graph.readCoordinates();
		for (int size : sizes) {
			BufferedReader bf2 = new BufferedReader(new FileReader(Parameters.ROOT_OUTPUTS + size + "/serial_node_mapping.txt"));
			PrintWriter pow = new PrintWriter(new File(Parameters.ROOT_OUTPUTS + size + "/coordinatesSerial.txt"));
			HashMap<Integer, Integer> newToOldId = new HashMap<>();
			String line = bf2.readLine();
			int max = 0;
			while (true) {
				line = bf2.readLine();
				if (line == null)
					break;
				int idnew = Integer.parseInt(line.split(" -> ")[0]);
				int idold = Integer.parseInt(line.split(" -> ")[1]);
				if (idnew > max)
					max = idnew;
				newToOldId.put(idnew, idold);
			}
			System.out.println("Max : " + max);
			for (int i = 0; i <= max; i++) {
				pow.println(i + " " + graph.nodes.get(newToOldId.get(i)).lat + " " + graph.nodes.get(newToOldId.get(i)).longi);
			}
			pow.close();
		}
	}
	
	public void serialiseKeywords () throws Exception {
		ArrayList<Integer> sizes = new ArrayList<Integer>();
		sizes.add(5000);
		sizes.add(10000);
		sizes.add(15000);
		for (int size : sizes) {
			BufferedReader bf1 = new BufferedReader(new FileReader(Parameters.ROOT_OUTPUTS + size + "/serial_node_mapping.txt"));
			BufferedReader bf2 = new BufferedReader(new FileReader(Parameters.ROOT_OUTPUTS + size + "/nodesWithKeywords.txt"));
			PrintWriter pow = new PrintWriter(new File(Parameters.ROOT_OUTPUTS + size + "/nodesWithKeywordsSerial.txt"));
			HashMap<Integer, Integer> oldToNew = new HashMap<>();
			ArrayList<Integer> newOnes = new ArrayList<>();
			String line = bf1.readLine();
			
			while (true) {
				line = bf1.readLine();
				if (line == null)
					break;
				int idnew = Integer.parseInt(line.split(" -> ")[0]);
				int idold = Integer.parseInt(line.split(" -> ")[1]);
				oldToNew.put(idold, idnew);
			}
			HashMap<Integer, ArrayList<String>> keywords = new HashMap<>();
			while (true) {
				line = bf2.readLine();
				if (line == null)
					break;
				StringTokenizer stt = new StringTokenizer(line, " ");
				int id = Integer.parseInt(stt.nextToken().toString());
				newOnes.add(oldToNew.get(id));
				keywords.put(oldToNew.get(id), new ArrayList<String>());
				while (stt.hasMoreTokens()) {
					keywords.get(oldToNew.get(id)).add(stt.nextToken().toString());
				}
			}
			System.out.println("Unique keywords : " + newOnes.size());
			Collections.sort(newOnes);
			for (int i = 0; i < newOnes.size(); i++) {
				pow.print(newOnes.get(i) + " ");
				ArrayList<String> keys = keywords.get(newOnes.get(i));
				for (int j = 0; j < keys.size(); j++) {
					pow.print(keys.get(j) + " ");
				}
				pow.println();
			}
			pow.close();
		}
	}

	public void generateGraph () throws Exception {
		ArrayList<Integer> sizes = new ArrayList<Integer>();
		sizes.add(5000);
		sizes.add(10000);
		sizes.add(15000);
		
		Graph graph = new Graph();
		graph.readCoordinates();
		graph.readNetwork();
		graph.readKeywords();
		
		int keynodeCount = 0;
		
		ArrayList<PrintWriter> pows = new ArrayList<>();
		for (int size : sizes) 
			pows.add(new PrintWriter(new FileWriter(Parameters.ROOT_OUTPUTS + size + "/graphIntegerWeights" + ".txt")));
		
		ArrayList<PrintWriter> powsKeywords = new ArrayList<>();
		for (int size : sizes) 
			powsKeywords.add(new PrintWriter(new FileWriter(Parameters.ROOT_OUTPUTS + size + "/nodesWithKeywords" + ".txt")));
		
		ArrayList<PrintWriter> powsCoordinates = new ArrayList<>();
		for (int size : sizes) 
			powsCoordinates.add(new PrintWriter(new FileWriter(Parameters.ROOT_OUTPUTS + size + "/coordinates" + ".txt")));
		
		Random r = new Random();
		int start = r.nextInt(graph.n);
		Queue<Pair> q = new LinkedList<Pair>();
		// first  = soruce second = weight third = destination
		
		for (Edge e : graph.edges.get(start)) {
			Pair p = new Pair();
			p.first = start;
			p.second = e.neighbour;
			p.third = e.weight;
			q.add(p);
		}
		
		HashSet<Integer> visited = new HashSet<Integer>();
		HashSet<String> visitedEdges = new HashSet<>();

		
		while (true) {
			Pair p = q.poll();
			if (p.first < p.second) {
				if (visitedEdges.contains(p.first + "_" + p.second))
					continue;
			}
			else {
				if (visitedEdges.contains(p.second + "_" + p.first))
					continue;
			}
			if (p.first < p.second)
				visitedEdges.add(p.first + "_" + p.second);
			else
				visitedEdges.add(p.second + "_" + p.first);
			
			for (int i = 0; i < sizes.size(); i++) {
				if (keynodeCount < sizes.get(i)) {
					pows.get(i).println(p.first + " " + p.second + " " + p.third);
					if (!visited.contains(p.first)) {
						powsCoordinates.get(i).println(p.first + " " + graph.nodes.get(p.first).lat + " " + graph.nodes.get(p.first).longi);
						if (graph.nodes.get(p.first).keywords != null) {
							powsKeywords.get(i).print(p.first + " ");
							for (String key : graph.nodes.get(p.first).keywords)
								powsKeywords.get(i).print(key + " ");
							powsKeywords.get(i).println();
						}
						if (i == sizes.size() - 1) {
							visited.add(p.first);
							if (graph.nodes.get(p.first).keywords != null)
								keynodeCount++;
						}
					}
				}
			}
			for (int size : sizes) {
				if (size == keynodeCount) {
					System.out.println("Nodes : " + visited.size() + " Edges : " + visitedEdges.size() + " Keynodes : " + keynodeCount);
				}

			}
			for (Edge e : graph.edges.get(p.second)) {
				boolean okay = true;
				if (e.neighbour < p.second) {
					if (visitedEdges.contains(e.neighbour + "_" + p.second))
						okay = false;
				}
				else {
					if (visitedEdges.contains(p.second + "_" + e.neighbour))
						okay = false;
				}
				if (okay) {
					Pair pNew = new Pair();
					pNew.first = p.second;
					pNew.second = e.neighbour;
					pNew.third = e.weight;
					q.add(pNew);
				}
			}
			if (keynodeCount == sizes.get(sizes.size() - 1)) break;
			
		}
		for (PrintWriter pow : pows)
			pow.close();
		for (PrintWriter pow : powsKeywords)
			pow.close();
		for (PrintWriter pow : powsCoordinates)
			pow.close();
		
	}
	public void ReadNetwork() throws Exception {
		BufferedReader bf = new BufferedReader(new FileReader("/Users/chendu/Desktop/DDP/New/Data/london/network.txt"));
		//PrintWriter pow1 = new PrintWriter(new FileWriter("/Users/chendu/Desktop/DDP/git/Datasets/london/nodes.txt"));
		PrintWriter pow = new PrintWriter(new FileWriter("/Users/chendu/Desktop/DDP/New/Data/london/coordinates.txt"));
		HashMap <Integer, String> coordinates = new HashMap<>();
		String s = "p";
		HashSet<Integer> nodes = new HashSet<Integer>();
		//pow.println( "# " + 209406 + " " + 564534);
		//pow1.println("# " + 209406);
		int count = 0;
		while (s != null) {
			s = bf.readLine();
			if (s == null)
				break;
			StringTokenizer stt = new StringTokenizer(s, " ");
			int edge_id = Integer.parseInt(stt.nextToken().toString());
			int n1 = Integer.parseInt(stt.nextToken().toString());
			int n2 = Integer.parseInt(stt.nextToken().toString());
			double dist = 0.0;
			node node1 = new node();
			node node2 = new node();
			node1.lat = Double.parseDouble(stt.nextToken().toString());
			node1.longi = Double.parseDouble(stt.nextToken().toString());
			
			if (!coordinates.containsKey(n1)) {
				String coordinate = node1.lat + "_" + node1.longi;
				coordinates.put(n1, coordinate);
			}
			else {
				String coordinate = node1.lat + "_" + node1.longi;
				String storedCoordinate = coordinates.get(n1);
				if (!coordinate.equals(storedCoordinate)) {
					System.out.println("Fucked " + coordinate + " " + storedCoordinate);
				}
			}
			while (stt.hasMoreTokens()) {
				node2.lat = Double.parseDouble(stt.nextToken().toString());
				node2.longi = Double.parseDouble(stt.nextToken().toString());
				if (!stt.hasMoreTokens()) {
					if (!coordinates.containsKey(n2)) {
						String coordinate = node2.lat + "_" + node2.longi;
						coordinates.put(n2, coordinate);
					}
					else {
						String coordinate = node2.lat + "_" + node2.longi;
						String storedCoordinate = coordinates.get(n2);
						if (!coordinate.equals(storedCoordinate)) {
							System.out.println("Fucked " + coordinate + " " + storedCoordinate);
						}
					}
				}
				dist += get_dist(node1, node2);
				node1.lat = node2.lat;
				node1.longi = node2.longi;
			}
			
			//pow.println((n1-1) + " " + (n2-1) + " " + dist);
			if (!nodes.contains(n1-1)) {
				//pow1.println(n1-1);
				nodes.add(n1-1); 
			}
			if (!nodes.contains(n2-1)) {
				//pow1.println(n2-1);
				nodes.add(n2-1);
			}
		}
		Iterator it = coordinates.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<Integer, String> coordiate = (Entry<Integer, String>) it.next();
			pow.println(coordiate.getKey() + " " + coordiate.getValue().split("_")[0] + " " + coordiate.getValue().split("_")[1]);
		}
		pow.close();
		//System.out.println(count);
		//System.out.println(nodes.size());
	}
	
	public void Serialize() throws Exception {
		ArrayList<Integer> sizes = new ArrayList<Integer>();
		sizes.add(5000);		
		sizes.add(10000);
		sizes.add(15000);
		for (int size : sizes) {
			BufferedReader bf = new BufferedReader(new FileReader(Parameters.ROOT_OUTPUTS + size + "/graphIntegerWeights.txt"));
			PrintWriter pow = new PrintWriter(new FileWriter(Parameters.ROOT_OUTPUTS + size + "/graphSerialIntegerWeights.txt"));
			PrintWriter pow1 = new PrintWriter(new FileWriter(Parameters.ROOT_OUTPUTS + size + "/serial_node_mapping.txt"));
			String s = "p";
			HashMap<Integer, Integer> mapping = new HashMap<>();
			int init = 0;
			int ncount = 0;
			int ecount = 0;
			while (s != null) {
				s = bf.readLine();
				if (s == null)
					break;
				StringTokenizer stt = new StringTokenizer(s, " ");
				int n1 = Integer.parseInt(stt.nextToken().toString());
				int n2 = Integer.parseInt(stt.nextToken().toString());
				int node1 = 0;
				int node2 = 0;
				ecount++;
				if (mapping.containsKey(n1)) {
					node1 = mapping.get(n1);
				}
				else {
					node1 = init++;
					mapping.put(n1, node1);
				}
				if (mapping.containsKey(n2)) {
					node2 = mapping.get(n2);
				}
				else {
					node2 = init++;
					mapping.put(n2, node2);
				}
				pow.println(node1 + " " + node2 + " " + stt.nextToken().toString());
			}
			Iterator it = mapping.entrySet().iterator();
			pow1.println("Serialized -> Old");
			while (it.hasNext()) {
				Map.Entry<Integer, Integer> pair = (Entry<Integer, Integer>) it.next();
				pow1.println(pair.getValue() + " -> " + pair.getKey());
			}
			System.out.println("Nodes : " + mapping.size() + " Edges : " + ecount);
			pow.close();
			pow1.close();
		}
	}
	
	public void ReadObject() throws Exception {
		ArrayList<String> stopwords = new ArrayList<>();
		BufferedReader bf1 = new BufferedReader(new FileReader("src/stopwords"));
		while (true) {
			String s = bf1.readLine();
			if (s == null)
				break;
			stopwords.add(s.toLowerCase());
		}
		BufferedReader bf = new BufferedReader(new FileReader("/Users/chendu/Desktop/DDP/New/Data/london/object.txt"));
		BufferedReader bfkeywords = new BufferedReader(new FileReader("/Users/chendu/Desktop/DDP/New/Data/london/validkeywords.txt"));
		PrintWriter pow = new PrintWriter(new FileWriter("/Users/chendu/Desktop/DDP/New/Data/london/objectWithKeywords.txt"));

		
		ArrayList<String> validKeywords = new ArrayList<>();
		while (true) {
			String s = bfkeywords.readLine();
			if (s == null)
				break;
			validKeywords.add(s);
		}
		//HashMap <String, ArrayList<Integer>> keyword_to_nodes_ids = new HashMap<>();
		//ArrayList<node> nodes = new ArrayList<>();
		while (true) {
			String s = bf.readLine();
			if (s == null)
				break;
			StringTokenizer stt = new StringTokenizer(s, " ,");
			int id = Integer.parseInt(stt.nextToken().toString());
			/*node n1 = new node();
			n1.id = id -1 ;
			String temp = stt.nextToken().toString();
			n1.lat = Double.parseDouble(stt.nextToken().toString());
			n1.longi = Double.parseDouble(stt.nextToken().toString());
			nodes.add(n1);*/
			ArrayList<String> taken = new ArrayList<>();
			int bigid = Integer.parseInt(stt.nextToken().toString());
			double lat = Double.parseDouble(stt.nextToken().toString());
			double longi = Double.parseDouble(stt.nextToken().toString());
			while (stt.hasMoreTokens()) {
				String token = stt.nextToken().toString().toLowerCase();
				if (validKeywords.contains(token) && !taken.contains(token)) {
					taken.add(token);
				}
			}
			if (taken.size() > 0) {
				pow.print(id + " " + bigid + " " + lat + " " + longi + " ");
				for (String token : taken) {
					pow.print(token + " ");
				}
				pow.println();
			}
		}
		pow.close();
		
			/*while (stt.hasMoreTokens()) {
				String token = stt.nextToken().toString().toLowerCase();
				if (stopwords.contains(token))
					continue;
				if (token.contains("wiki"))
					continue;
				ArrayList<Integer> nodes_with_keyword;
				if (keyword_to_nodes_ids.containsKey(token.toLowerCase())) {
					nodes_with_keyword = keyword_to_nodes_ids.get(token.toLowerCase());
				}
				else {
					nodes_with_keyword = new ArrayList<>();
				}
				nodes_with_keyword.add(id - 1);
				keyword_to_nodes_ids.put(token.toLowerCase(), nodes_with_keyword);
			}
		}
		Iterator it = keyword_to_nodes_ids.entrySet().iterator();
		HashMap <String, ArrayList<Integer>> pruned_keyword_to_nodes_ids = new HashMap<>();
		while(it.hasNext()) {
			Map.Entry pair = (Map.Entry)it.next();
			ArrayList<Integer> ids = (ArrayList<Integer>) pair.getValue();
			if (ids.size() > 20)
				pruned_keyword_to_nodes_ids.put((String) pair.getKey(), ids);
		}
		System.out.println(pruned_keyword_to_nodes_ids.size());
		Set<String> keys = pruned_keyword_to_nodes_ids.keySet();
		System.out.println(keys);
		for (String key : keys) {
			pow.println(key);
		}*/
		//get_distances(pruned_keyword_to_nodes_ids, nodes);
	}
	
	public void get_distances(HashMap <String, ArrayList<Integer>> pruned_keyword_to_nodes_ids, 
			ArrayList<node> nodes) throws Exception {
		int number_of_keywords = pruned_keyword_to_nodes_ids.keySet().size();
		double [][] avg_dist = new double[number_of_keywords][number_of_keywords];
		Iterator it = pruned_keyword_to_nodes_ids.entrySet().iterator();
		HashMap <String, Integer> keyword_to_id = new HashMap<>();
		HashMap<Integer, String> id_to_keyword = new HashMap<>();
		int keyword_id = 0;
		while (it.hasNext()) {
			Map.Entry pair = (Map.Entry)it.next();
			Iterator it1 = pruned_keyword_to_nodes_ids.entrySet().iterator();
			while (it1.hasNext()) {
				Map.Entry pair1 = (Map.Entry)it1.next();
				int id1 = 0;
				int id2 = 0;
				if (keyword_to_id.get(pair.getKey()) != null) {
					id1 = keyword_to_id.get(pair.getKey());
				}
				else {
					keyword_to_id.put((String) pair.getKey(), keyword_id);
					id1 = keyword_id;
					id_to_keyword.put(keyword_id, (String) pair.getKey());
					keyword_id++;
				}
				if (keyword_to_id.get(pair1.getKey()) != null) {
					id2 = keyword_to_id.get(pair1.getKey());
				}
				else {
					keyword_to_id.put((String) pair1.getKey(), keyword_id);
					id2 = keyword_id;
					id_to_keyword.put(keyword_id, (String) pair1.getKey());
					keyword_id++;
				}
				if (avg_dist[id1][id2] > 0)
					continue;
				ArrayList<Integer> ids = (ArrayList<Integer>) pair.getValue();
				ArrayList<Integer> ids1 = (ArrayList<Integer>) pair1.getValue();
				double[]  closest = new double[ids.size()];
				double[]  closest1 = new double[ids1.size()];
				for (int i = 0; i < ids.size(); i++) 
					closest[i] = Double.MAX_VALUE;
				for (int i = 0; i < ids1.size(); i++)
					closest1[i] = Double.MAX_VALUE;
				for (int i = 0; i < ids.size(); i++) {
					for (int j = 0; j < ids1.size(); j++) {
						node n1 = nodes.get(ids.get(i));
						node n2 = nodes.get(ids1.get(j));
						double dist = get_dist(n1, n2);
						if (dist < closest[i])
							closest[i] = dist;
						if (dist < closest1[j])
							closest1[j] = dist;
					}
				}
				if (ids.size() == 0 || ids1.size() == 0) {
					System.out.println("Fucked");
				}
				
				double dist = 0;
				for (int i = 0; i < ids.size(); i++)
					dist += closest[i];
				
				for (int j = 0; j < ids1.size(); j++)
					dist += closest1[j];
				
				double avg;
				if ((ids.size() + ids1.size()) > 0)
					avg = dist / ((ids.size() + ids1.size()));
				else
					avg = Double.MAX_VALUE;
				avg_dist[id1][id2] = avg;
				avg_dist[id2][id1] = avg;
			}
		}
		
		PrintWriter out1 = new PrintWriter(new FileWriter("src/matrix.txt"));
		out1.println(number_of_keywords);
		for (int i = 0; i < number_of_keywords; i++) {
			for (int j = 0; j < number_of_keywords; j++) {
				out1.print(avg_dist[i][j] + " ");
			}
			out1.println();
			System.out.println(i);
		}
		
		out1.close();
		
		out1 = new PrintWriter(new FileWriter("src/keywords.txt"));
		out1.println(number_of_keywords);
		for (int i = 0; i < number_of_keywords; i++)
			out1.println(id_to_keyword.get(i) + " " + pruned_keyword_to_nodes_ids.get(id_to_keyword.get(i)).size());
		
		out1.close();
	}
	
	public void find_closest() throws IOException {
		BufferedReader bf = new BufferedReader(new FileReader("src/matrix.txt"));
		int number_of_keywords = Integer.parseInt(bf.readLine());
		double avg_dist[][] = new double[number_of_keywords][number_of_keywords];
		for(int i = 0; i < number_of_keywords; i++) {
			String s = bf.readLine();
			StringTokenizer stt = new StringTokenizer(s, " ");
			for(int j = 0; j < number_of_keywords; j++) {
				avg_dist[i][j] = Double.parseDouble(stt.nextToken().toString());
			}
		}
		bf = new BufferedReader(new FileReader("src/keywords.txt"));
		number_of_keywords = Integer.parseInt(bf.readLine());
		HashMap <String, Integer> keyword_to_id = new HashMap<>();
		HashMap<Integer, String> id_to_keyword = new HashMap<>();
		
		int count = 0;
		for (int i = 0; i < number_of_keywords; i++) {
			String s = bf.readLine();
			keyword_to_id.put(s, count);
			id_to_keyword.put(count, s);
			count++;
		}
		
		PrintWriter out = new PrintWriter(new FileWriter("src/top10closest.txt"));
		for (int i = 0; i < number_of_keywords; i++) {
			ArrayList<closest> close_ones = new ArrayList<>();
			for (int j = 0; j < number_of_keywords ; j++) {
				if (i != j) {
					closest c1 = new closest();
					c1.dist = avg_dist[i][j];
					c1.id = j;
					close_ones.add(c1);
				}
			}
			Collections.sort(close_ones, new closest_comparator());
			out.println(id_to_keyword.get(i));
			for (int j = 0; j < 10; j++) {
				if (j != 9)
					out.print(id_to_keyword.get(close_ones.get(j).id) + " $ ");
				else
					out.println(id_to_keyword.get(close_ones.get(j).id));
			}
			System.out.println(i);
		}
		out.close();
		System.out.println("Done");
		
	}
	
	public double get_dist (node n1, node n2) {
		double earthRadius = 6371000; // metres
	    double dLat = Math.toRadians(n2.lat - n1.lat);
	    double dLng = Math.toRadians(n2.longi - n1.longi);
	    double sindLat = Math.sin(dLat / 2);
	    double sindLng = Math.sin(dLng / 2);
	    double a = Math.pow(sindLat, 2) + Math.pow(sindLng, 2)
	            * Math.cos(Math.toRadians(n1.lat)) * Math.cos(Math.toRadians(n2.lat));
	    double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
	    double dist = earthRadius * c;
	    return dist;
	}

	public void assignKeyword() throws Exception {
		BufferedReader bf1 = new BufferedReader(new FileReader("/Users/chendu/Desktop/DDP/New/Data/london/coordinates.txt"));
		BufferedReader bf2 = new BufferedReader(new FileReader("/Users/chendu/Desktop/DDP/New/Data/london/objectWithKeywords.txt"));
		PrintWriter pow = new PrintWriter(new FileWriter("/Users/chendu/Desktop/DDP/New/Data/london/nodesWithKeywords.txt"));
		
		HashMap<Integer, ArrayList<String>> assignment = new HashMap<>();
		HashMap<Integer, node> coordinates = new HashMap<>();
		while (true) {
			String s = bf1.readLine();
			if (s == null)
				break;
			String[] splits = s.split(" ");
			int id = Integer.parseInt(splits[0]);
			node n1 = new node();
			n1.lat = Double.parseDouble(splits[1]);
			n1.longi = Double.parseDouble(splits[2]);
			n1.id = id;
			coordinates.put(id, n1);
		}
		
		while (true) {
			String s = bf2.readLine();
			if (s == null)
				break;
			StringTokenizer stt = new StringTokenizer(s, " ");
			String temp = stt.nextToken();
			temp = stt.nextToken();
			node n1 = new node();
			n1.lat = Double.parseDouble(stt.nextToken());
			n1.longi = Double.parseDouble(stt.nextToken());
			Iterator it = coordinates.entrySet().iterator();
			double min = Integer.MAX_VALUE;
			node chosen = new node();
			while (it.hasNext()) {
				Map.Entry<Integer, node> pair = (Entry<Integer, node>) it.next();
				double dist = get_dist(n1, pair.getValue());
				if (dist < min) {
					min = dist;
					chosen = pair.getValue();
				}
			}
			if (!assignment.containsKey(chosen.id)) {
				assignment.put(chosen.id, new ArrayList<String>());
			}
			while (stt.hasMoreTokens()) {
				assignment.get(chosen.id).add(stt.nextToken().toString());
			}
		}
		Iterator it = assignment.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<Integer, ArrayList<String>> pair = (Entry<Integer, ArrayList<String>>) it.next();
			pow.print(pair.getKey() + " ");
			for (int i = 0; i < pair.getValue().size(); i++) {
				pow.print(pair.getValue().get(i) + " ");
			}
			pow.println();
		}
		pow.close();
	}
	public void cleanNodes () throws Exception {
		BufferedReader bf1 = new BufferedReader(new FileReader("/Users/chendu/Desktop/DDP/New/Data/dublin/nodesWithKeywords.txt"));
		PrintWriter pow = new PrintWriter(new FileWriter("/Users/chendu/Desktop/DDP/New/Data/dublin/nodesWithKeywords1.txt"));
		while (true) {
			String s = bf1.readLine();
			if (s == null)
				break;
			StringTokenizer stt = new StringTokenizer(s, " ");
			pow.print(stt.nextToken() + " ");
			HashSet<String> taken = new HashSet<>();
			while (stt.hasMoreTokens()) {
				String token = stt.nextToken();
				if (!taken.contains(token)) {
					pow.print(token + " ");
					taken.add(token);
				}
			}
			pow.println();
		}
		pow.close();
	}

	public void addDirection () throws Exception {
		ArrayList<Integer> sizes = new ArrayList<Integer>();
		sizes.add(5000);
		sizes.add(10000);
		sizes.add(15000);
		//String size = "";*/
		for (int size : sizes) {
			BufferedReader bf = new BufferedReader(new FileReader(Parameters.ROOT_OUTPUTS + size + "/graphSerialIntegerWeights.txt"));
			PrintWriter pow = new PrintWriter(new FileWriter(Parameters.ROOT_OUTPUTS + size + "/graphSerialIntegerWeightsDirected.txt"));
			//String line = bf.readLine();
			//pow.println(line.split(" ")[0]);
			//pow.println(line);
			int count = 0;
			while (true) {
				String line = bf.readLine();
				if (line == null)
					break;
				count++;
				StringTokenizer stt = new StringTokenizer(line, " ");
				//pow.println(stt.nextToken() + " " + stt.nextToken() + " " + ((int)Double.parseDouble(stt.nextToken())+1));
				pow.println(line + " " + 0);
			}
			pow.println();
			pow.close();
			System.out.println("Count : " + count);
		}
	}

	public void readMetisPartition() throws Exception {
		ArrayList<Integer> sizes = new ArrayList<Integer>();
		sizes.add(10000);
		sizes.add(25000);
		sizes.add(50000);
		for (int size : sizes) {
			Graph graph = new Graph();
			BufferedReader bf = new BufferedReader(new FileReader(Parameters.ROOT_DATA + Parameters.CITY + size + "/metis.txt.part." + (size/10)));
			HashMap<Integer, Integer> clusterCounts = new HashMap<>();
			HashMap<Integer, Integer> clusterAssignment = new HashMap<>();
			int i = 0;
			while (true) {
				String line = bf.readLine();
				if (line == null) break;
				int id = Integer.parseInt(line);
				if (!clusterCounts.containsKey(id)) clusterCounts.put(id, 0);
				clusterCounts.put(id, clusterCounts.get(id)+1);
				clusterAssignment.put(i, id);
			i++;
			}
			//HashMap<Integer, Integer> sorted = (HashMap<Integer, Integer>) sortByValue(clusterCounts);
			//System.out.println(sorted);
			graph.size = size;		
			graph.readCoordinates();
			graph.readKeywords();
			graph.readLabels();
			
			HashMap<Integer, Integer> keywordCounts = new HashMap<>();
			HashMap<Integer, ArrayList<Integer>> reverseVisited = new HashMap<>();
			
			//Set<Integer> keys = graph.keynodeToKeywords.keySet();
			HashSet<Integer> keys = new HashSet<>();
			for (Node node : graph.nodes) {
				if (node.keywords != null)
					keys.add(node.id);
			}
			
			int index = 0;
			
			HashMap<Integer, Integer> idMapping = new HashMap<>();
			
			for (int key : keys) {
				int clusterdId = clusterAssignment.get(key);
				if (!keywordCounts.containsKey(clusterdId)) {
					keywordCounts.put(clusterdId, 0);
				}
				keywordCounts.put(clusterdId, keywordCounts.get(clusterdId) + 1);
				if (!idMapping.containsKey(clusterdId)){
					idMapping.put(clusterdId, index++);
					reverseVisited.put(idMapping.get(clusterdId), new ArrayList<Integer>());
				}
				reverseVisited.get(idMapping.get(clusterdId)).add(key);
			}
			
			
			PrintWriter pow = new PrintWriter(new FileWriter(Parameters.ROOT_DATA + Parameters.CITY + size + "/landmarkIndexMetis.txt"));
			
			for (int j = 0; j < index; j++) {
				ArrayList<Integer> ids = reverseVisited.get(j);
				pow.print(j + " ");
				for (int id : ids) pow.print(id + " ");
				pow.println();
			}
			pow.close();
			//System.out.println(reverseVisited);
		}
	}

}
